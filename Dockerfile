FROM docker.io/library/eclipse-temurin:latest
WORKDIR /usr/src/app/
COPY entrypoint.sh /usr/src/app/entrypoint.sh
COPY build/libs/openbudget-0.0.1-SNAPSHOT.jar /usr/src/app/app.jar
ENTRYPOINT /usr/src/app/entrypoint.sh -jar /usr/src/app/app.jar
