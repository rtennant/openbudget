SCRIPT_HOME=`dirname $0 | while read a; do cd $a && pwd && break; done`
echo "$SCRIPT_HOME"
podman run -it --rm \
    --name openbudget-postgres \
    -p 5432:5432 \
    -e POSTGRES_PASSWORD=openbudget \
    -e POSTGRES_USER=openbudget \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v "$SCRIPT_HOME/data:/var/lib/postgresql/data:z" \
    postgres:14
