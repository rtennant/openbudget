#!/usr/bin/env sh

BASE_JAVA_OPTS="-server"

exec java ${BASE_JAVA_OPTS} ${JAVA_OPTS} $@
