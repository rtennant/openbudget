package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.BaseIntegrationTest
import eu.vnagy.openbudget.OpenBudgetFakerService
import eu.vnagy.openbudget.assertEquals
import eu.vnagy.openbudget.assertNotNull
import eu.vnagy.openbudget.model.entites.Category
import eu.vnagy.openbudget.model.entites.Transaction
import eu.vnagy.openbudget.model.entites.TransactionStatus
import eu.vnagy.openbudget.model.pojo.EqualsFilterExpression
import eu.vnagy.openbudget.model.pojo.NotExpression
import io.ebean.Database
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Month


internal class AccountDaoITest : BaseIntegrationTest() {

    @Autowired
    private lateinit var database: Database

    @Autowired
    private lateinit var reconciliationDao: ReconciliationDao

    @Autowired
    private lateinit var fakerService: OpenBudgetFakerService

    @Autowired
    private lateinit var transactionDao: TransactionDao

    @Autowired
    private lateinit var accountDao: AccountDao

    @Test
    fun reconciliation_with_account_tab_search_should_work() {
        // Create a transaction with child-transactions -> even if the child transactions are uncleared, the _clearness_ status should be inherited
        // Reconcile the parent transaction
        // The filtering from the accounts tab should not show the reconciled transactions

        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val payee = fakerService.fakePayee()
        val rentCategory = Category(budget = budget).apply {
            name = "Rent & stuff"
        }
        val rentSubCategory = Category(budget = budget).apply {
            name = "Rent"
            parentCategory = rentCategory
        }
        val utilitiesSubCategory = Category(budget = budget).apply {
            name = "Utilities"
            parentCategory = rentCategory
        }
        database.saveAll(rentCategory, rentSubCategory, utilitiesSubCategory)

        val clearedParentTransaction = Transaction(account = account).apply {
            this@apply.payee = payee
            this@apply.date = LocalDate.of(2022, Month.NOVEMBER, 13)
            this@apply.amount = BigDecimal(23450)
            this@apply.status = TransactionStatus.CLEARED
        }
        val clearedRentTransaction = Transaction(account = account, parentTransaction = clearedParentTransaction).apply {
            this@apply.category = rentSubCategory
            this@apply.amount = BigDecimal(17500)
            this@apply.status = TransactionStatus.UNCLEARED
        }
        val clearedUtilitiesTransaction = Transaction(account = account, parentTransaction = clearedParentTransaction).apply {
            this@apply.category = utilitiesSubCategory
            this@apply.amount = BigDecimal(5950)
            this@apply.status = TransactionStatus.UNCLEARED
        }

        val unclearedParentTransaction = Transaction(account = account).apply {
            this@apply.payee = payee
            this@apply.date = LocalDate.of(2022, Month.DECEMBER, 13)
            this@apply.amount = BigDecimal(45123)
            this@apply.status = TransactionStatus.UNCLEARED
        }
        val unclearedRentTransaction = Transaction(account = account, parentTransaction = unclearedParentTransaction).apply {
            this@apply.category = rentSubCategory
            this@apply.amount = BigDecimal(16233 * 2)
            this@apply.status = TransactionStatus.UNCLEARED
        }
        val unclearedUtilitiesTransaction = Transaction(account = account, parentTransaction = unclearedParentTransaction).apply {
            this@apply.category = utilitiesSubCategory
            this@apply.amount = BigDecimal(12657)
            this@apply.status = TransactionStatus.UNCLEARED
        }
        database.saveAll(
            clearedParentTransaction, clearedRentTransaction, clearedUtilitiesTransaction,
            unclearedParentTransaction, unclearedRentTransaction, unclearedUtilitiesTransaction
        )

        accountDao.reconcileAccount(account, BigDecimal.ZERO)

        val nonReconciledTransactions = transactionDao.findTransactionStream(
            listOf(account),
            null,
            0,
            Int.MAX_VALUE,
            filters = listOfNotNull(
                NotExpression(
                    EqualsFilterExpression(
                        Transaction::status.name, TransactionStatus.RECONCILED
                    )
                )
            )
        ).toList().map { it.id }.toSet()

        val allTransactions = transactionDao.findTransactionStream(
            listOf(account),
            null,
            0,
            Int.MAX_VALUE,
        ).toList().map { it.id }.toSet()

        assertEquals(setOf(unclearedParentTransaction.id), nonReconciledTransactions)
        assertEquals(setOf(unclearedParentTransaction.id, clearedParentTransaction.id), allTransactions)
    }
    
    @Test
    fun account_zero_balance_should_only_be_added_to_reconciled_transactions() {
        // Create a transaction with child-transactions -> even if the child transactions are uncleared, the _clearness_ status should be inherited
        // Reconcile the parent transaction
        // The filtering from the accounts tab should not show the reconciled transactions

        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val payee = fakerService.fakePayee()
        
        account.relativeZeroBalance = 10000.toBigDecimal()
        database.saveAll(account)
        
        val rentCategory = Category(budget = budget).apply {
            name = "Rent & stuff"
        }
        val rentSubCategory = Category(budget = budget).apply {
            name = "Rent"
            parentCategory = rentCategory
        }
        val utilitiesSubCategory = Category(budget = budget).apply {
            name = "Utilities"
            parentCategory = rentCategory
        }
        database.saveAll(rentCategory, rentSubCategory, utilitiesSubCategory)

        val clearedParentTransaction = Transaction(account = account).apply {
            this@apply.payee = payee
            this@apply.date = LocalDate.of(2022, Month.NOVEMBER, 13)
            this@apply.amount = BigDecimal(-23450)
            this@apply.status = TransactionStatus.CLEARED
        }
        val clearedRentTransaction = Transaction(account = account, parentTransaction = clearedParentTransaction).apply {
            this@apply.category = rentSubCategory
            this@apply.amount = BigDecimal(-17500)
            this@apply.status = TransactionStatus.UNCLEARED
        }
        val clearedUtilitiesTransaction = Transaction(account = account, parentTransaction = clearedParentTransaction).apply {
            this@apply.category = utilitiesSubCategory
            this@apply.amount = BigDecimal(-5950)
            this@apply.status = TransactionStatus.UNCLEARED
        }
        database.saveAll(clearedParentTransaction, clearedRentTransaction, clearedUtilitiesTransaction)

        assertEquals(BigDecimal(-13450), accountDao.getBalanceForAccount(account, TransactionStatus.values().toList()))
        assertEquals(BigDecimal.ZERO, accountDao.getBalanceForAccount(account, listOf(TransactionStatus.UNCLEARED)))
        assertEquals(BigDecimal(-23450), accountDao.getBalanceForAccount(account, listOf(TransactionStatus.CLEARED)))
        assertEquals(BigDecimal(10000), accountDao.getBalanceForAccount(account, listOf(TransactionStatus.RECONCILED)))
    }
        
    @Test
    fun should_save_reconciliation_entity_with_null_transaction_when_the_reconciled_amount_is_null() {
        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val payee = fakerService.fakePayee()

        val rentCategory = Category(budget = budget).apply {
            name = "Rent & stuff"
        }
        
        val rentTransaction = Transaction(account = account).apply { 
            this.amount = BigDecimal(-100_000)
            this.date = LocalDate.of(2022, Month.DECEMBER, 10)
            this.category = rentCategory
            this.payee = payee
            this.status = TransactionStatus.CLEARED
        }
        
        val reconcileDate = LocalDateTime.now()
        database.saveAll(rentCategory, rentTransaction)
        accountDao.reconcileAccount(account, BigDecimal.ZERO, reconcileDate)
        
        val reconciliationsForAccount = reconciliationDao.findReconciliationsForAccount(account)
        assertEquals(1, reconciliationsForAccount.size)
        
        val reconciliation = reconciliationsForAccount.first()
        assertEquals(reconcileDate, reconciliation.dateTime)
        assertEquals(account, reconciliation.account)
        assertNull(reconciliation.transaction)
    }
        
    @Test
    fun should_save_reconciliation_entity_with_a_transaction_when_the_reconciled_amount_is_not_null() {
        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val payee = fakerService.fakePayee()

        val rentCategory = Category(budget = budget).apply {
            name = "Rent & stuff"
        }
        
        val rentTransaction = Transaction(account = account).apply { 
            this.amount = BigDecimal(-100_000)
            this.date = LocalDate.of(2022, Month.DECEMBER, 10)
            this.category = rentCategory
            this.payee = payee
            this.status = TransactionStatus.CLEARED
        }
        
        val reconcileDate = LocalDateTime.now()
        database.saveAll(rentCategory, rentTransaction)
        accountDao.reconcileAccount(account, BigDecimal(-500), reconcileDate)
        
        val reconciliationsForAccount = reconciliationDao.findReconciliationsForAccount(account)
        assertEquals(1, reconciliationsForAccount.size)
        
        val reconciliation = reconciliationsForAccount.first()
        assertEquals(reconcileDate, reconciliation.dateTime)
        assertEquals(account, reconciliation.account)
        val transaction = reconciliation.transaction
        
        assertNotNull(transaction)
        assertEquals(BigDecimal(-500), transaction.amount)
    }

}