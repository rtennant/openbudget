package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.BaseIntegrationTest
import eu.vnagy.openbudget.OpenBudgetFakerService
import eu.vnagy.openbudget.assertEquals
import eu.vnagy.openbudget.model.entites.BudgetForMonth
import eu.vnagy.openbudget.model.entites.Category
import eu.vnagy.openbudget.model.entites.Transaction
import io.ebean.Database
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.math.BigDecimal
import java.time.Month
import java.time.YearMonth

class BudgetForMonthDaoITest  : BaseIntegrationTest() {

    @Autowired
    private lateinit var database: Database

    @Autowired
    private lateinit var fakerService: OpenBudgetFakerService

    @Autowired
    private lateinit var transactionDao: TransactionDao

    @Autowired
    private lateinit var budgetForMonthDao: BudgetForMonthDao

    @Test
    fun last_day_spending_should_be_calculated_into_sum_by_category() {
        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val payee = fakerService.fakePayee()
        val carCategory = Category(budget = budget).apply {
            name = "Car"
        }
        database.saveAll(budget, account, payee, carCategory)

        val yearMonth = YearMonth.of(2022, Month.NOVEMBER)
        val carSpending = Transaction(account = account).apply {
            date = yearMonth.atEndOfMonth()
            amount = BigDecimal(-10_000)
            category = carCategory
        }
        val carBudgetForMonth = BudgetForMonth(
            budget = budget,
            firstDayOfMonth = yearMonth.atDay(1),
            category = carCategory
        ).apply { 
            budgetedMoney = BigDecimal(0)
        }
        database.saveAll(carBudgetForMonth, carSpending)

        assertEquals(
            BigDecimal(-10_000),
            budgetForMonthDao.calcAvailableMoneyUntilMonthInCategory(carCategory, yearMonth)
        )
    }

    @Test
    fun should_return_correct_available_money_if_there_is_no_budget_for_month() {
        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val payee = fakerService.fakePayee()
        val carCategory = Category(budget = budget).apply {
            name = "Car"
        }
        database.saveAll(budget, account, payee, carCategory)

        val yearMonth = YearMonth.of(2022, Month.NOVEMBER)
        val carSpending = Transaction(account = account).apply {
            date = yearMonth.atEndOfMonth()
            amount = BigDecimal(-10_000)
            category = carCategory
        }
        database.saveAll(carSpending)

        assertEquals(
            BigDecimal(-10_000),
            budgetForMonthDao.calcAvailableMoneyUntilMonthInCategory(carCategory, yearMonth)
        )
    }
    
}