package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.BaseIntegrationTest
import eu.vnagy.openbudget.OpenBudgetFakerService
import eu.vnagy.openbudget.assertEquals
import eu.vnagy.openbudget.model.entites.Category
import eu.vnagy.openbudget.model.entites.Transaction
import eu.vnagy.openbudget.model.pojo.EqualsFilterExpression
import io.ebean.Database
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.math.BigDecimal
import java.time.LocalDate
import java.time.Month
import java.time.YearMonth

internal class TransactionDaoITest : BaseIntegrationTest() {

    @Autowired
    private lateinit var database: Database

    @Autowired
    private lateinit var fakerService: OpenBudgetFakerService

    @Autowired
    private lateinit var transactionDao: TransactionDao

    @Test
    fun should_find_transactions_based_on_hierarchy_correctly() {
        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val payee = fakerService.fakePayee()
        val rentCategory = Category(budget = budget).apply {
            name = "Rent & stuff"
        }
        val rentSubCategory = Category(budget = budget).apply {
            name = "Rent"
            parentCategory = rentCategory
        }
        val utilitiesSubCategory = Category(budget = budget).apply {
            name = "Utilities"
            parentCategory = rentCategory
        }
        database.saveAll(rentCategory, rentSubCategory, utilitiesSubCategory)

        val parentTransaction = Transaction(account = account).apply {
            this@apply.payee = payee
            this@apply.date = LocalDate.of(2022, Month.NOVEMBER, 13)
            this@apply.amount = BigDecimal(23450)
        }
        val rentTransaction = Transaction(account = account, parentTransaction = parentTransaction).apply {
            this@apply.category = rentSubCategory
            this@apply.amount = BigDecimal(17500)
        }
        val utilitiesTransaction = Transaction(account = account, parentTransaction = parentTransaction).apply {
            this@apply.category = utilitiesSubCategory
            this@apply.amount = BigDecimal(5950)
        }
        database.saveAll(parentTransaction, rentTransaction, utilitiesTransaction)

        val parentTransactionIdsFromDatabase = transactionDao.findTransactionStream(listOf(account)).map { it.id }.toList().toSet()
        assertEquals(setOf(parentTransaction.id), parentTransactionIdsFromDatabase)

        val subTransactionIdsFromDatabase = transactionDao.findTransactionStream(
            account = listOf(account),
            parent = parentTransaction
        ).map { it.id }.toList().toSet()
        assertEquals(setOf(rentTransaction.id, utilitiesTransaction.id), subTransactionIdsFromDatabase)
    }

    @Test
    fun should_find_parent_transactions_even_if_the_filter_applies_only_for_child_transaction() {
        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val payee = fakerService.fakePayee()
        val rentCategory = Category(budget = budget).apply {
            name = "Rent & stuff"
        }
        val rentSubCategory = Category(budget = budget).apply {
            name = "Rent"
            parentCategory = rentCategory
        }
        val utilitiesSubCategory = Category(budget = budget).apply {
            name = "Utilities"
            parentCategory = rentCategory
        }
        database.saveAll(rentCategory, rentSubCategory, utilitiesSubCategory)

        val parentTransaction = Transaction(account = account).apply {
            this@apply.payee = payee
            this@apply.date = LocalDate.of(2022, Month.NOVEMBER, 13)
            this@apply.amount = BigDecimal(23450)
        }
        val rentTransaction = Transaction(account = account, parentTransaction = parentTransaction).apply {
            this@apply.category = rentSubCategory
            this@apply.amount = BigDecimal(17500)
        }
        val utilitiesTransaction = Transaction(account = account, parentTransaction = parentTransaction).apply {
            this@apply.category = utilitiesSubCategory
            this@apply.amount = BigDecimal(5950)
        }
        database.saveAll(parentTransaction, rentTransaction, utilitiesTransaction)

        val parentTransactionIdsFromDatabase = transactionDao.findTransactionStream(
            account = listOf(account),
            parent = null,
            filters = listOf(
                EqualsFilterExpression(
                    Transaction::category.name, rentSubCategory
                )
            )
        ).map { it.id }.toList().toSet()
        assertEquals(setOf(parentTransaction.id), parentTransactionIdsFromDatabase)

        val subTransactionIdsFromDatabase = transactionDao.findTransactionStream(
            account = listOf(account),
            parent = parentTransaction,
            filters = listOf(
                EqualsFilterExpression(
                    Transaction::category.name, rentSubCategory
                )
            )
        ).map { it.id }.toList().toSet()
        assertEquals(setOf(rentTransaction.id), subTransactionIdsFromDatabase)
    }

    @Test
    fun should_find_first_transaction_date_for_accounts() {
        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val payee = fakerService.fakePayee()
        val incomeCategory = Category(budget = budget).apply {
            name = "Income"
        }

        database.saveAll(budget, account, payee, incomeCategory)

        val incomeTransaction = Transaction(account = account).apply {
            date = LocalDate.of(2022, Month.NOVEMBER, 20)
        }

        database.saveAll(incomeTransaction)

        assertEquals(incomeTransaction.date, transactionDao.findMinTransactionDateForAccounts(listOf(account)))
    }
    
    @Test
    fun last_day_spending_should_be_calculated_into_sum_by_category() {
        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val payee = fakerService.fakePayee()
        val incomeCategory = Category(budget = budget).apply {
            name = "Income"
        }
        database.saveAll(budget, account, payee, incomeCategory)
        
        val yearMonth = YearMonth.of(2022, Month.NOVEMBER)
        val incomeTransaction = Transaction(account = account).apply {
            date = yearMonth.atEndOfMonth()
            amount = BigDecimal(10_000)
            category = incomeCategory
        }
        database.saveAll(incomeTransaction)

        assertEquals(
            BigDecimal(10_000),
            transactionDao.sumSpentAndIncomeByCategoryInMonth(incomeCategory, yearMonth)
        )
    }
}
