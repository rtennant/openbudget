package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.BaseIntegrationTest
import eu.vnagy.openbudget.OpenBudgetFakerService
import eu.vnagy.openbudget.model.entites.Reconciliation
import io.ebean.Database
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalDateTime

class ReconciliationDaoITest : BaseIntegrationTest() {

    @Autowired
    private lateinit var database: Database
    
    @Autowired
    private lateinit var fakerService: OpenBudgetFakerService
    
    @Autowired
    private lateinit var testObj: ReconciliationDao
    
    @Test
    fun should_return_latest_reconciliation_when_multiple_entities_are_present_for_account() {
        val account = fakerService.fakeAccount()
        
        assertNull(testObj.findLatestReconciliationForAccount(account))

        val first = Reconciliation(
            account = account,
            dateTime = LocalDateTime.now(),
            transaction = null
        )
        database.save(first)
        assertEquals(first, testObj.findLatestReconciliationForAccount(account))
        
        val second = Reconciliation(
            account = account,
            dateTime = LocalDateTime.now(),
            transaction = null
        )
        database.save(second)
        assertEquals(second, testObj.findLatestReconciliationForAccount(account))
    }
}