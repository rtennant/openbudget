package eu.vnagy.openbudget

import eu.vnagy.openbudget.model.dao.CurrencyDao
import eu.vnagy.openbudget.model.entites.*
import io.ebean.Database
import net.datafaker.Faker
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class OpenBudgetFakerService {

    @Autowired
    private lateinit var database: Database

    @Autowired
    private lateinit var currencyDao: CurrencyDao

    private val faker = Faker()

    @Transactional
    fun fakeUser(requireNewOne: Boolean = false): User {
        if (!requireNewOne) {
            val existingUser = database.find(User::class.java).findOne()
            if (existingUser != null) {
                return existingUser
            }
        }
        val user = faker.name().let { User(
            givenName = it.firstName(),
            familyName = it.lastName(),
            email = "${it.username()}@${faker.domain().firstLevelDomain(faker.company().url())}"
        ) }
        database.save(user)
        return user
    }

    fun fakeBudget(requireNewOne: Boolean = false): Budget {
        if (!requireNewOne) {
            val existingBudget = database.find(Budget::class.java).findOne()
            if (existingBudget != null) {
                return existingBudget
            }
        }
        val mainCurrency = currencyDao.findById("HUF")!!
        val budget = Budget(user = fakeUser(), name = faker.funnyName().name(), mainCurrency = mainCurrency)
        val incomeCategory = Category(budget = budget).apply { 
            name = "Inflow: Ready to Assign"
        }
        database.saveAll(budget)
        database.saveAll(incomeCategory)
        return budget
    }

    fun fakeAccount(requireNewOne: Boolean = false): Account {
        if (!requireNewOne) {
            val existingAccount = database.find(Account::class.java).findOne()
            if (existingAccount != null) {
                return existingAccount
            }
        }

        val account = Account(budget = fakeBudget(), currency = Currency("HUF")).apply {
            name = faker.funnyName().name()
        }
        database.save(account)
        return database.find(Account::class.java, account.id)!!
    }

    fun fakePayee(requireNewOne: Boolean = false): Payee {
        if (!requireNewOne) {
            val existingPayee = database.find(Payee::class.java).findOne()
            if (existingPayee != null) {
                return existingPayee
            }
        }
        val payee = Payee(budget = fakeBudget()).apply {
            name = "Example Inc."
        }
        database.save(payee)
        return payee
    }
}
