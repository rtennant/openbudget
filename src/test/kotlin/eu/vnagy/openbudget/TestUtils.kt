package eu.vnagy.openbudget

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import java.math.BigDecimal
import java.time.Duration
import java.time.temporal.ChronoUnit
import java.time.temporal.Temporal
import kotlin.contracts.contract
import kotlin.math.max

fun assertEquals(expected: BigDecimal?, actual: BigDecimal?) {
    if (actual == null || expected == null) {
        assertEquals(expected, actual)
    } else {
        val largerScale = max(expected.scale(), actual.scale())
        assertEquals(
            expected.setScale(largerScale),
            actual.setScale(largerScale)
        )
    }
}

fun <T: Temporal> assertEquals(
    expected: T?, 
    actual: T?,
    delta: Duration = Duration.ofMillis(100)
) {
    if (actual == null || expected == null) {
        Assertions.assertEquals(expected, actual)
    } else {
        if (actual.isSupported(ChronoUnit.SECONDS) && expected.isSupported(ChronoUnit.SECONDS)) {
            val timeBetween = Duration.between(expected, actual)
            assertTrue(timeBetween < delta)
        } else {
            Assertions.assertEquals(expected, actual)
        }
    }
}

fun assertNotNull(any: Any?) {
    contract {
        returns() implies (any != null)
    }
    Assertions.assertNotNull(any)
}