package eu.vnagy.openbudget.services.budget.networth

import eu.vnagy.openbudget.BaseIntegrationTest
import eu.vnagy.openbudget.OpenBudgetFakerService
import eu.vnagy.openbudget.assertEquals
import eu.vnagy.openbudget.model.entites.Category
import eu.vnagy.openbudget.model.entites.Transaction
import io.ebean.Database
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.math.BigDecimal
import java.time.LocalDate
import java.time.Month

internal class BudgetNetWorthServiceITest : BaseIntegrationTest(){

    @Autowired
    private lateinit var fakerService: OpenBudgetFakerService

    @Autowired
    private lateinit var database: Database

    @Autowired
    private lateinit var netWorthService: BudgetNetWorthService

    @Test
    fun calculateNetWorthValueWithFewIncomes() {
        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val incomeCategory = Category(budget = budget).apply {
            name = "Income"
        }
        val incomeTransaction01 = Transaction(account = account).apply {
            category = incomeCategory
            amount = BigDecimal(15000)
            date = LocalDate.of(2020, Month.JANUARY, 2)
        }
        val incomeTransaction02 = Transaction(account = account).apply {
            category = incomeCategory
            amount = BigDecimal(30000)
            date = LocalDate.of(2020, Month.JANUARY, 5)
        }

        database.saveAll(incomeCategory, incomeTransaction01, incomeTransaction02)

        val netWorthDescriptor = netWorthService.calculateNetWorthBetweenDates(
            accounts = listOf(account),
            fromDate = LocalDate.of(2020, Month.JANUARY, 2),
            toDate = LocalDate.of(2020, Month.JANUARY, 10),
        )

        assertEquals(
            setOf(
                LocalDate.of(2020, Month.JANUARY, 2),
                LocalDate.of(2020, Month.JANUARY, 3),
                LocalDate.of(2020, Month.JANUARY, 4),
                LocalDate.of(2020, Month.JANUARY, 5),
                LocalDate.of(2020, Month.JANUARY, 6),
                LocalDate.of(2020, Month.JANUARY, 7),
                LocalDate.of(2020, Month.JANUARY, 8),
                LocalDate.of(2020, Month.JANUARY, 9),
                LocalDate.of(2020, Month.JANUARY, 10),
            ),
            netWorthDescriptor.netWorthByDate.keys
        )

        assertEquals(BigDecimal(15_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 2)])
        assertEquals(BigDecimal(15_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 3)])
        assertEquals(BigDecimal(15_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 4)])
        assertEquals(BigDecimal(45_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 5)])
        assertEquals(BigDecimal(45_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 6)])
        assertEquals(BigDecimal(45_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 7)])
        assertEquals(BigDecimal(45_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 8)])
        assertEquals(BigDecimal(45_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 9)])
        assertEquals(BigDecimal(45_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 10)])
    }

    @Test
    fun calculateNetWorthValueWithSplitIncomeTransactions() {
        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val incomeCategory = Category(budget = budget).apply {
            name = "Income"
        }
        val taxesCategory = Category(budget = budget).apply {
            name = "Taxes"
        }
        val incomeTransaction01 = Transaction(account = account).apply {
            category = incomeCategory
            amount = BigDecimal(15000)
            date = LocalDate.of(2020, Month.JANUARY, 2)
        }
        val splitIncomeTransaction = Transaction(account = account).apply {
            amount = BigDecimal(30000)
            date = LocalDate.of(2020, Month.JANUARY, 5)
        }
        val splitIncome01 = Transaction(account = account, parentTransaction = splitIncomeTransaction).apply {
            category = incomeCategory
            amount = BigDecimal(33000)
            date = LocalDate.of(2020, Month.JANUARY, 5)
        }
        val splitIncome02 = Transaction(account = account, parentTransaction = splitIncomeTransaction).apply {
            category = taxesCategory
            amount = BigDecimal(-3000)
            date = LocalDate.of(2020, Month.JANUARY, 5)
        }

        database.saveAll(incomeCategory, taxesCategory, incomeTransaction01, splitIncomeTransaction, splitIncome01, splitIncome02)

        val netWorthDescriptor = netWorthService.calculateNetWorthBetweenDates(
            accounts = listOf(account),
            fromDate = LocalDate.of(2020, Month.JANUARY, 2),
            toDate = LocalDate.of(2020, Month.JANUARY, 10),
        )

        assertEquals(
            setOf(
                LocalDate.of(2020, Month.JANUARY, 2),
                LocalDate.of(2020, Month.JANUARY, 3),
                LocalDate.of(2020, Month.JANUARY, 4),
                LocalDate.of(2020, Month.JANUARY, 5),
                LocalDate.of(2020, Month.JANUARY, 6),
                LocalDate.of(2020, Month.JANUARY, 7),
                LocalDate.of(2020, Month.JANUARY, 8),
                LocalDate.of(2020, Month.JANUARY, 9),
                LocalDate.of(2020, Month.JANUARY, 10),
            ),
            netWorthDescriptor.netWorthByDate.keys
        )

        assertEquals(BigDecimal(15_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 2)])
        assertEquals(BigDecimal(15_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 3)])
        assertEquals(BigDecimal(15_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 4)])
        assertEquals(BigDecimal(45_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 5)])
        assertEquals(BigDecimal(45_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 6)])
        assertEquals(BigDecimal(45_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 7)])
        assertEquals(BigDecimal(45_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 8)])
        assertEquals(BigDecimal(45_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 9)])
        assertEquals(BigDecimal(45_000), netWorthDescriptor.netWorthByDate[LocalDate.of(2020, Month.JANUARY, 10)])
    }

    // TODO write a test where we try to calculate the sum of net worth from before the first transaction...

}