package eu.vnagy.openbudget.services.budget.montecarlo

import eu.vnagy.openbudget.BaseIntegrationTest
import eu.vnagy.openbudget.OpenBudgetFakerService
import eu.vnagy.openbudget.assertEquals
import eu.vnagy.openbudget.model.entites.Category
import eu.vnagy.openbudget.model.entites.Transaction
import io.ebean.Database
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.math.BigDecimal
import java.time.LocalDate
import java.time.Month
import java.time.temporal.ChronoUnit

class BudgetMonteCarloSimulationServiceITest : BaseIntegrationTest() {

    @Autowired
    private lateinit var fakerService: OpenBudgetFakerService

    @Autowired
    private lateinit var database: Database

    @Autowired
    private lateinit var monteCarloSimulationService: BudgetMonteCarloSimulationService

    @Test
    fun simulations_first_data_point_should_be_equal_to_net_worth_last_data_point() {
        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val incomeCategory = Category(budget = budget).apply {
            name = "Income"
        }
        val outcomeCategory = Category(budget = budget).apply {
            name = "Outcome"
        }
        database.saveAll(incomeCategory, outcomeCategory)
        val startDate = LocalDate.of(2020, Month.JANUARY, 1)
        var loopDate = startDate
        val endDate = LocalDate.of(2022, Month.JUNE, 19)

        while (loopDate <= endDate) {
            val weeksSince = ChronoUnit.WEEKS.between(startDate, loopDate)
            if ((weeksSince % 4).toInt() == 0) {
                val incomeTransaction = Transaction(account = account).apply {
                    category = incomeCategory
                    amount = BigDecimal(1000 * (weeksSince + 1))
                    date = loopDate
                }
                database.saveAll(incomeTransaction)
            }
            val outcomeTransaction = Transaction(account = account).apply {
                category = incomeCategory
                amount = BigDecimal(-150 * (weeksSince + 1))
                date = loopDate
            }
            database.saveAll(outcomeTransaction)
            loopDate = loopDate.plusWeeks(1)
        }
        
        val monteCarloSimulation = monteCarloSimulationService.createMonteCarloSimulations(
            1,
            listOf(account),
            startDate,
            endDate,
            LocalDate.of(2030, Month.DECEMBER, 31)
        )

        monteCarloSimulation.orderedMonteCarloResults.forEach {
            assertEquals(
                monteCarloSimulation.factDataFromPast[endDate],
                it.predictedValueByDate[endDate]
            )
        }
    }

    // TODO write more tests with mocked random & stuff
}