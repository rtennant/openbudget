package eu.vnagy.openbudget.services.goal

import eu.vnagy.openbudget.BaseIntegrationTest
import eu.vnagy.openbudget.OpenBudgetFakerService
import eu.vnagy.openbudget.assertEquals
import eu.vnagy.openbudget.model.dao.GoalDao
import eu.vnagy.openbudget.model.entites.*
import io.ebean.Database
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.math.BigDecimal
import java.math.BigInteger
import java.math.RoundingMode
import java.time.LocalDate
import java.time.Month
import java.time.YearMonth

internal class GoalServiceITest : BaseIntegrationTest(){

    @Autowired
    private lateinit var database: Database

    @Autowired
    private lateinit var fakerService: OpenBudgetFakerService

    @Autowired
    private lateinit var goalDao: GoalDao

    @Autowired
    private lateinit var testObj: GoalService

    @Test
    fun should_calculate_goal_statistics_correctly_for_total_goal() {
        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val carCategory = Category(budget = budget).apply {
            name = "Car related stuff"
        }
        val amortizationCategory = Category(budget = budget).apply {
            name = "New car's cost"
            parentCategory = carCategory
        }
        val goal = Goal(category = amortizationCategory).apply {
            startMonthFirstDay = LocalDate.of(2020, Month.JANUARY, 1)
            endMonthLastDay = LocalDate.of(2024, Month.DECEMBER, 31)
            goalAmount = BigDecimal(10_000_000)
            type = GoalType.TOTAL
        }
        listOf(carCategory, amortizationCategory, goal).forEach { database.save(it) }
        var allBudgetedUntilNov2022: BigDecimal = BigDecimal.ZERO
        var allSpentUntilNov2022: BigDecimal = BigDecimal.ZERO
        val yearMonthOfTest = YearMonth.of(2022, Month.NOVEMBER)
        for (monthsSince in 0 until 60) {
            val firstDayOfMonth = goal.startMonthFirstDay.plusMonths(monthsSince.toLong())
            val budgetForMonth = BudgetForMonth(
                budget = budget,
                category = amortizationCategory,
                firstDayOfMonth = firstDayOfMonth
            ).apply {
                budgetedMoney = BigDecimal(6_000) * monthsSince.toBigDecimal()
            }
            val transaction = Transaction(account = account).apply {
                category = amortizationCategory
                amount = BigDecimal(-3_456)
                date = firstDayOfMonth
            }
            database.save(budgetForMonth)
            database.save(transaction)

            if (firstDayOfMonth <= yearMonthOfTest.atDay(1)) {
                allBudgetedUntilNov2022 += budgetForMonth.budgetedMoney
                allSpentUntilNov2022 += transaction.amount.negate()
            }
        }

        Assertions.assertEquals(
            BigDecimal(35 / 2.0 * 34 * 6_000),
            allBudgetedUntilNov2022
        ) // sum of an arithmetic series (n/2) * ((n-1)*d), n=35 (months), d=10_000 (amount)
        Assertions.assertEquals(BigDecimal(35 * 3_456), allSpentUntilNov2022) // 35 months * 3456

        val goalStatistics = testObj.calculateGoalStatistics(goal, yearMonthOfTest)

        Assertions.assertEquals(BigDecimal(10_000_000), goalStatistics.goalTotalAmount)
        Assertions.assertEquals(BigDecimal(10_000_000).divide(BigDecimal(60), 10, RoundingMode.HALF_EVEN), goalStatistics.goalMonthlyAmount)
        Assertions.assertEquals(allBudgetedUntilNov2022.setScale(3), goalStatistics.budgetedForGoalIncludingCurrentMonth.setScale(3))
        assertEquals(BigInteger.valueOf(60), goalStatistics.durationInMonthsIncludingFirstAndLast)
        assertEquals(BigInteger.valueOf(35), goalStatistics.alreadyPassedMonthsIncludingCurrentOne)
        assertEquals(BigInteger.valueOf(25), goalStatistics.monthsLeftOverExcludingCurrentOne)
        Assertions.assertEquals(allSpentUntilNov2022.setScale(3), goalStatistics.spentForGoalIncludingCurrentMonth.setScale(3))

        Assertions.assertEquals(
            BigDecimal(10_000_000 * 35)
                .setScale(10)
                .divide(BigDecimal(60), RoundingMode.HALF_UP)
                .minus(allBudgetedUntilNov2022)
                .plus(BigDecimal(6_000 * 34))
                .setScale(3, RoundingMode.HALF_UP),
            goalStatistics.shouldBudgetOnceTheFollowingToBeOnTrackThisMonth.setScale(3, RoundingMode.HALF_UP)
        )

        Assertions.assertEquals(
            BigDecimal(10_000_000)
                .minus(allBudgetedUntilNov2022)
                .plus(BigDecimal(6_000 * 34))
                .divide(BigDecimal(26), 10, RoundingMode.HALF_UP)
                .setScale(3, RoundingMode.HALF_UP),
            goalStatistics.shouldBudgetAlwaysTheFollowingToBeOnTrackFromNowOn.setScale(3, RoundingMode.HALF_UP)
        )
    }

    @Test
    fun should_calculate_budget_each_month_and_budget_this_month_to_be_on_track_correctly_with_one_year_goal() {
        val budget = fakerService.fakeBudget()
        val account = fakerService.fakeAccount()
        val savingsParentCategory = Category(budget = budget).apply {
            name = "Savings"
        }
        val generalSavingsCategory = Category(budget = budget).apply {
            name = "General savings"
            parentCategory = savingsParentCategory
        }
        val goal = Goal(category = generalSavingsCategory).apply {
            startMonthFirstDay = LocalDate.of(2020, Month.JANUARY, 1)
            endMonthLastDay = LocalDate.of(2020, Month.DECEMBER, 31)
            goalAmount = BigDecimal(6_000_000) // 500_000 each month
            type = GoalType.TOTAL
        }
        listOf(savingsParentCategory, generalSavingsCategory, goal).forEach { database.save(it) }
        val yearMonthOfTest = YearMonth.of(2020, Month.NOVEMBER)
        for (monthsSince in 0 until 11) {
            val firstDayOfMonth = goal.startMonthFirstDay.plusMonths(monthsSince.toLong())
            val budgetForMonth = BudgetForMonth(
                budget = budget,
                category = generalSavingsCategory,
                firstDayOfMonth = firstDayOfMonth
            ).apply {
                budgetedMoney = BigDecimal(450_000)
            }
            database.save(budgetForMonth)
        }

        val goalStatistics = testObj.calculateGoalStatistics(goal, yearMonthOfTest)

        assertEquals(BigDecimal(6_000_000), goalStatistics.goalTotalAmount)
        assertEquals(BigDecimal(500_000), goalStatistics.goalMonthlyAmount)
        assertEquals(BigDecimal(11 * 450_000), goalStatistics.budgetedForGoalIncludingCurrentMonth)
        assertEquals(BigInteger.valueOf(12), goalStatistics.durationInMonthsIncludingFirstAndLast)
        assertEquals(BigInteger.valueOf(11), goalStatistics.alreadyPassedMonthsIncludingCurrentOne)
        assertEquals(BigInteger.valueOf(1), goalStatistics.monthsLeftOverExcludingCurrentOne)
        assertEquals(BigDecimal.ZERO, goalStatistics.spentForGoalIncludingCurrentMonth)

        assertEquals(BigDecimal(1_000_000), goalStatistics.shouldBudgetOnceTheFollowingToBeOnTrackThisMonth)

        assertEquals(BigDecimal(750_000), goalStatistics.shouldBudgetAlwaysTheFollowingToBeOnTrackFromNowOn)
    }

}
