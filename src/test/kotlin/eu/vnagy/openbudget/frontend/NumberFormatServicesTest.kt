package eu.vnagy.openbudget.frontend

import eu.vnagy.openbudget.assertEquals
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.model.entites.Budget
import org.junit.jupiter.api.Test
import org.mockito.kotlin.mock
import java.math.BigDecimal

class NumberFormatServicesTest {
    @Test
    fun basic_parsing_examples() {
        val testBudget: Budget = mock()
        val testObj = NumberFormatServices()

        assertEquals(BigDecimal.ZERO, testObj.parseBigDecimalFromInput(null, testBudget))
        assertEquals(BigDecimal.ZERO, testObj.parseBigDecimalFromInput("", testBudget))
        assertEquals(BigDecimal(1), testObj.parseBigDecimalFromInput("1", testBudget))
        assertEquals(BigDecimal(0.5), testObj.parseBigDecimalFromInput("1/2", testBudget))
        assertEquals(BigDecimal(30), testObj.parseBigDecimalFromInput("2*(5+10)", testBudget))
        assertEquals(BigDecimal(1234), testObj.parseBigDecimalFromInput(testObj.formatBigDecimalForInput(BigDecimal(1234), testBudget), testBudget))
    }

    @Test
    fun should_parse_expressions_if_a_formula_was_wrote_after_the_already_formatted_string() {
        val testBudget: Budget = mock()
        val testObj = NumberFormatServices()
        val formattedValue = testObj.formatBigDecimalForInput(BigDecimal(1000), testBudget)
        assertEquals(
            BigDecimal(1234),
            testObj.parseBigDecimalFromInput(
                "$formattedValue + 234",
                testBudget
            )
        )
    }
}