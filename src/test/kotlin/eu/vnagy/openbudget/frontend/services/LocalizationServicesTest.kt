package eu.vnagy.openbudget.frontend.services

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.Month

class LocalizationServicesTest {

    private val testObj = LocalizationServices()

    @Nested
    inner class PrettyPrintDurationTests {
        
        @Test
        fun should_pretty_print_durations_if_the_delta_is_small() {
            val start = LocalDateTime.of(2022, Month.DECEMBER, 23, 23, 4)
            val end01 = start.plusSeconds(10)
            assertEquals("just now", testObj.prettyPrintDurationBetween(start, end01))
            
            val end02 = start.plusSeconds(35)
            assertEquals("35 seconds", testObj.prettyPrintDurationBetween(start, end02))

            val end03 = start.plusMinutes(1)
            assertEquals("1 minute", testObj.prettyPrintDurationBetween(start, end03))

            val end04 = start.plusMinutes(1).plusSeconds(13)
            assertEquals("1 minute and 13 seconds", testObj.prettyPrintDurationBetween(start, end04))

            val end05 = start.plusHours(5).plusSeconds(13)
            assertEquals("5 hours", testObj.prettyPrintDurationBetween(start, end05))

            val end06 = start.plusHours(5).plusMinutes(10).plusSeconds(13)
            assertEquals("5 hours and 10 minutes", testObj.prettyPrintDurationBetween(start, end06))
        }
    }
}