package eu.vnagy.openbudget

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Transactional
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers

@SpringBootTest("spring.profiles.active=test")
@ExtendWith(value = [SpringExtension::class])
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
@Rollback
@Testcontainers
@ContextConfiguration(initializers = [BaseIntegrationTest.Initializer::class])
abstract class BaseIntegrationTest {
    companion object {

        @Container
        @JvmStatic
        var postgresql: PostgreSQLContainer<*> = PostgreSQLContainer(
            "postgres:14"
            // DockerImageName.parse("docker.io/postgres:14").asCompatibleSubstituteFor("postgres:14")
        )
            .withDatabaseName("openbudget")
            .withUsername("openbudget")
            .withPassword("openbudget")

        @JvmStatic
        @BeforeAll
        fun beforeAll() {
            println("Postgresql starting...")
            postgresql.start()
            println("Postgresql started")
        }

        @JvmStatic
        @AfterAll
        fun afterAll() {
            postgresql.stop()
        }
    }

    class Initializer : ApplicationContextInitializer<ConfigurableApplicationContext> {
        override fun initialize(applicationContext: ConfigurableApplicationContext) {
            TestPropertyValues.of(
                "spring.datasource.url=" + postgresql.jdbcUrl,
                "spring.datasource.username=" + postgresql.username,
                "spring.datasource.password=" + postgresql.password
            ).applyTo(applicationContext.environment);
        }

    }
}