package com.vaadin.starter.business.ui.components.navigation.drawer;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.RouteParameters;

import java.util.List;
import java.util.stream.Collectors;

@CssImport("./styles/components/navi-menu.css")
public class NaviMenu extends Div {

	private String CLASS_NAME = "navi-menu";

	public NaviMenu() {
		setClassName(CLASS_NAME);
	}

	protected void addNaviItem(NaviItem item) {
		add(item);
	}

	protected void addNaviItem(NaviItem parent, NaviItem item) {
		parent.addSubItem(item);
		addNaviItem(item);
	}

	public NaviItem addNaviItem(String text,
	                            Class<? extends Component> navigationTarget) {
		NaviItem item = new NaviItem(text, navigationTarget);
		addNaviItem(item);
		return item;
	}

	public NaviItem addNaviItem(VaadinIcon icon, String text,
	                            Class<? extends Component> navigationTarget) {
		NaviItem item = new NaviItem(icon, text, navigationTarget);
		addNaviItem(item);
		return item;
	}

	public NaviItem addNaviItem(Image image, String text,
	                            Class<? extends Component> navigationTarget) {
		NaviItem item = new NaviItem(image, text, navigationTarget);
		addNaviItem(item);
		return item;
	}

	public NaviItem addNaviItem(NaviItem parent, String text,
	                            Class<? extends Component> navigationTarget) {
		NaviItem item = new NaviItem(text, navigationTarget);
		addNaviItem(parent, item);
		return item;
	}

	public List<NaviItem> getNaviItems() {
		List<NaviItem> items = (List) getChildren().collect(Collectors.toList());
		return items;
	}

	public NaviItem addNaviItem(
		NaviItem parent, 
		Component component, 
		Class<? extends Component> navigationTarget, 
		RouteParameters routeParameters
	) {
		NaviItem item = new NaviItem(component, navigationTarget, routeParameters);
		addNaviItem(parent, item);
		return item;
	}

	public NaviItem addNaviItem(
		Component component, 
		Class<? extends Component> navigationTarget, 
		RouteParameters routeParameters
	) {
		NaviItem item = new NaviItem(component, navigationTarget, routeParameters);
		addNaviItem(item);
		return item;
	}

	public NaviItem addNaviItem(
		String text, 
		Class<? extends Component> navigationTarget, 
		RouteParameters routeParameters
	) {
		NaviItem item = new NaviItem(text, navigationTarget, routeParameters);
		addNaviItem(item);
		return item;
	}

	public NaviItem addNaviItem(Component component) {
		NaviItem item = new NaviItem(component);
		addNaviItem(item);
		return item;
	}
}
