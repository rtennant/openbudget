package kotlin.internal

// https://youtrack.jetbrains.com/issue/KT-13198/Expose-OnlyInputTypes-annotation-to-allow-proper-checks-of-parameters-of-generic-methods-same-as-those-used-Collectioncontains

@Target(AnnotationTarget.TYPE_PARAMETER)
@Retention(AnnotationRetention.BINARY)
annotation class OnlyInputTypes
