package eu.vnagy.openbudget.model.entites

import java.time.Instant

interface BaseEntity<T> {

    val id: T

    val version: Instant
}
