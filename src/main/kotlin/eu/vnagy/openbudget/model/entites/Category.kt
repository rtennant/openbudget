package eu.vnagy.openbudget.model.entites

import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "categories")
data class Category (

    @Id
    @Column(name = "id")
    override val id: UUID = UUID.randomUUID(),

    @Version
    @Column(name = "version")
    override val version: Instant = Instant.now(),

    @ManyToOne
    @JoinColumn(name = "budget_id", referencedColumnName = "id")
    val budget: Budget,
): BaseEntity<UUID> {

    @Column(name = "name")
    var name: String = ""

    @ManyToOne
    @JoinColumn(name = "parent_category_id", referencedColumnName = "id")
    var parentCategory: Category? = null

    @OrderBy("order")
    @OneToMany(mappedBy = "parentCategory", fetch = FetchType.LAZY)
    var childCategories: List<Category> = emptyList()

    @Column(name = "hidden")
    var hidden: Boolean = false

    @Column(name = "deleted")
    var deleted: Boolean = false

    @Column(name = "order_by")
    var order: Int? = null

}
