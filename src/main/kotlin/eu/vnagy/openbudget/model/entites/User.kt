package eu.vnagy.openbudget.model.entites

import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "users")
data class User(

    @Id
    @Column(name = "id")
    override val id: UUID = UUID.randomUUID(),

    @Version
    @Column(name = "version")
    override val version: Instant = Instant.now(),

    @Column(name = "email")
    var email: String,

    @Column(name = "given_name")
    var givenName: String,

    @Column(name = "family_name")
    var familyName: String,

) : BaseEntity<UUID> {


    @OneToMany(mappedBy = "user")
    val budgets: MutableSet<Budget> = mutableSetOf()
}
