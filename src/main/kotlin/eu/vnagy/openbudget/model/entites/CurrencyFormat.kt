package eu.vnagy.openbudget.model.entites

import io.ebean.annotation.EnumValue
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "currency_formats")
data class CurrencyFormat (
    
    @Id
    @Column(name = "id")
    override val id: UUID = UUID.randomUUID(),

    @Version
    @Column(name = "version")
    override val version: Instant = Instant.now(),
    
    @Column(name = "symbol_pos")
    val symbolPosition: SymbolPosition,
    
    @Column(name = "grouping_separator")
    val groupingSeparator: Char,

    @Column(name = "decimal_separator")
    val decimalSeparator: Char,
    
): BaseEntity<UUID>

enum class SymbolPosition {

    @EnumValue("+")
    POST,
    
    @EnumValue("-")
    PRE

}
