package eu.vnagy.openbudget.model.entites

import java.time.Instant
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "reconciliations")
data class Reconciliation(

    @Id
    @Column(name = "id")
    override val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    val account: Account,

    @Column(name = "date_time")
    var dateTime: LocalDateTime,

    @OneToOne(cascade = [CascadeType.PERSIST, CascadeType.MERGE])
    @JoinColumn(name = "transaction_id", referencedColumnName = "id")
    val transaction: Transaction?,

    ) : BaseEntity<UUID> {
    
    @Version
    @Column(name = "version")
    override val version: Instant = Instant.now()

    init {
        this.dateTime = this.dateTime.truncatedTo(ChronoUnit.SECONDS)
    }
}