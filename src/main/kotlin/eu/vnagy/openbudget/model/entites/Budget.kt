package eu.vnagy.openbudget.model.entites

import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "budgets")
data class Budget(

    @Id
    @Column(name = "id")
    override val id: UUID = UUID.randomUUID(),

    @Version
    @Column(name = "version")
    override val version: Instant = Instant.now(),

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    val user: User,

    @Column(name = "name")
    var name: String,

    @ManyToOne(cascade = [CascadeType.PERSIST])
    @JoinColumn(name = "main_currency", referencedColumnName = "currency")
    var mainCurrency: Currency

) : BaseEntity<UUID> {

    @OneToMany(mappedBy = "budget")
    val accounts: MutableList<Account> = mutableListOf()

    @ManyToOne(cascade = [])
    @JoinColumn(name = "currency_format", referencedColumnName = "id")
    var currencyFormat: CurrencyFormat? = null
}
