package eu.vnagy.openbudget.model.entites

import java.math.BigDecimal
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "accounts")
data class Account (

    @Id
    @Column(name = "id")
    override val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "budget_id", referencedColumnName = "id")
    val budget: Budget,

    @ManyToOne
    @JoinColumn(name = "main_currency", referencedColumnName = "currency")
    var currency: Currency,

): BaseEntity<UUID> {

    @Version
    @Column(name = "version")
    override val version: Instant = Instant.now()

    @Column(name = "name")
    var name: String = ""

    @Column(name = "on_budget")
    var onBudget: Boolean = true

    @Column(name = "closed")
    var closed: Boolean = false

    @Column(name = "relative_zero_balance")
    var relativeZeroBalance: BigDecimal = BigDecimal.ZERO

    @OneToMany(mappedBy = "account")
    val transactions: List<Transaction> = listOf()

    @OrderBy("dateTime")
    @OneToMany(mappedBy = "account")
    val reconciliations: List<Reconciliation> = listOf()

}
