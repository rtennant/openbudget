package eu.vnagy.openbudget.model.entites

import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDate
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "budgets_for_months")
data class BudgetForMonth (

    @Id
    @Column(name = "id")
    override val id: UUID = UUID.randomUUID(),

    @Version
    @Column(name = "version")
    override val version: Instant = Instant.now(),

    @ManyToOne
    @JoinColumn(name = "budget_id", referencedColumnName = "id")
    val budget: Budget,

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    val category: Category,

    @Column(name = "first_day_of_month")
    val firstDayOfMonth: LocalDate
): BaseEntity<UUID> {

    @Column(name = "budgeted_money")
    lateinit var budgetedMoney: BigDecimal

}
