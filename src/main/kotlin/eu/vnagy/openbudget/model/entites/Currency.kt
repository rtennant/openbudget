package eu.vnagy.openbudget.model.entites

import java.time.Instant
import javax.persistence.*

@Entity
@Table(name = "currencies")
data class Currency(

    @Id
    @Column(name = "currency")
    val currency: String,

    @Version
    @Column(name = "version")
    override val version: Instant = Instant.now(),

) : BaseEntity<String> {
    override val id: String
        get() = currency

    @Column(name = "symbol")
    var symbol: String? = null
    
}
