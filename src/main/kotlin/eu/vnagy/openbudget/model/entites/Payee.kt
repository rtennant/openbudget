package eu.vnagy.openbudget.model.entites

import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "payees")
data class Payee (

    @Id
    @Column(name = "id")
    override val id: UUID = UUID.randomUUID(),

    @Version
    @Column(name = "version")
    override val version: Instant = Instant.now(),

    @ManyToOne
    @JoinColumn(name = "budget_id", referencedColumnName = "id")
    val budget: Budget,
): BaseEntity<UUID> {

    @Column(name = "name")
    lateinit var name: String

}
