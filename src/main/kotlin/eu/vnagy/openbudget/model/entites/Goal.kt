package eu.vnagy.openbudget.model.entites

import io.ebean.annotation.EnumValue
import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDate
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "goals")
data class Goal(

    @Id
    @Column(name = "id")
    override val id: UUID = UUID.randomUUID(),

    @Version
    @Column(name = "version")
    override val version: Instant = Instant.now(),

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    val category: Category,

    @Column(name = "name")
    var name: String = "",

) : BaseEntity<UUID> {

    @Column(name = "start_month_first_day")
    var startMonthFirstDay: LocalDate = LocalDate.now()

    @Column(name = "end_month_last_day")
    var endMonthLastDay: LocalDate = LocalDate.now()

    @Column(name = "goal_amount")
    var goalAmount: BigDecimal = BigDecimal.ZERO

    @Column(name = "type")
    var type: GoalType = GoalType.TOTAL

}

enum class GoalType {

    @EnumValue("M")
    MONTHLY,

    @EnumValue("T")
    TOTAL

}
