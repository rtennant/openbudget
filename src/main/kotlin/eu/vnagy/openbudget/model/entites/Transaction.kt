package eu.vnagy.openbudget.model.entites

import eu.vnagy.openbudget.model.entites.Transaction.Companion.TABLE_NAME
import io.ebean.annotation.EnumValue
import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDate
import java.util.*
import javax.persistence.*

@Entity
@Table(name = TABLE_NAME)
data class Transaction (

    @Id
    @Column(name = "id")
    override val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = COLUMN_ACCOUNT_ID, referencedColumnName = "id")
    val account: Account,

    @ManyToOne
    @JoinColumn(name = COLUMN_PARENT_TRANSACTION_ID, referencedColumnName = "id")
    val parentTransaction: Transaction? = null,

): BaseEntity<UUID> {

    companion object {
        const val TABLE_NAME = "transactions"
        const val COLUMN_AMOUNT = "amount"
        const val COLUMN_ACCOUNT_ID = "account_id"
        const val COLUMN_PARENT_TRANSACTION_ID = "parent_transaction_id"
        const val COLUMN_TRANSACTION_STATUS = "status"
    }

    @Version
    @Column(name = "version")
    override val version: Instant = Instant.now()

    @OneToOne
    @JoinColumn(name = "transfer_transaction_id", referencedColumnName = "id")
    var transferTransaction: Transaction? = null

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    var category: Category? = null

    @ManyToOne(cascade = [CascadeType.PERSIST])
    @JoinColumn(name = "payee_id", referencedColumnName = "id")
    var payee: Payee? = null

    @Column(name = "note")
    var note: String? = null

    @Column(name = "date")
    var date: LocalDate = LocalDate.now()

    @Column(name = COLUMN_AMOUNT)
    var amount: BigDecimal = BigDecimal.ZERO

    @Column(name = COLUMN_TRANSACTION_STATUS)
    var status: TransactionStatus = TransactionStatus.UNCLEARED

    @OneToMany(mappedBy = "parentTransaction", fetch = FetchType.LAZY)
    val childTransactions: List<Transaction> = listOf()

    @OneToOne(mappedBy = "transaction", fetch = FetchType.LAZY)
    val reconciliation: Reconciliation? = null

}

enum class TransactionStatus(
    private val inToggleLoop: Boolean
) {

    @EnumValue("UNC")
    UNCLEARED(true),
    @EnumValue("C")
    CLEARED(true),
    @EnumValue("R")
    RECONCILED(false);

    companion object {
        fun getToggleableStatuses(): List<TransactionStatus> {
            return TransactionStatus.values().filter { it.inToggleLoop }.toList()
        }
    }
}
