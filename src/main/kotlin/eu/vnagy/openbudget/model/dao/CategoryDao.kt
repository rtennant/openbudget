package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.model.dtos.CategorySumForNewTransaction
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Category
import eu.vnagy.openbudget.model.entites.Payee
import eu.vnagy.openbudget.model.utils.*
import io.ebean.Database
import io.ebean.Query
import org.springframework.stereotype.Repository
import java.math.BigDecimal
import java.time.LocalDate
import java.time.YearMonth
import java.util.*
import java.util.stream.Stream

@Repository
class CategoryDao constructor(
    private val database: Database
) : BaseDao<UUID, Category>(database, Category::class) {

    fun findTopLevelNotHiddenCategoriesByBudget(budget: Budget): List<Category> {
        return database
            .find(Category::class.java)
            .eq(Category::budget, budget)
            .eq(Category::parentCategory, null)
            .eq(Category::hidden, false)
            .eq(Category::deleted, false)
            .not(ilike(Category::name, "%internal%"))
            .findList()
    }
    fun findLeafCategoriesForNewTransaction(
        budget: Budget,
        payee: Payee?,
        searchString: String,
        offset: Int = 0,
        limit: Int = Int.MAX_VALUE
    ): List<CategorySumForNewTransaction> {
        return database.sqlQuery(
            """
                select (select count(1)
                        from transactions t
                        where t.category_id = c.id
                          and t.payee_id = :payee_id
                          and t.date > :two_months_ago) transaction_count,
                       (select (budgeted.budgeted + spent_child_transactions.spent) as available
                        from (select sum(budgeted_money) budgeted from budgets_for_months where category_id = c.id and first_day_of_month < :start_of_next_month) budgeted,
                             (select sum(t.amount) spent
                              from transactions t
                              where t.id not in (select t2.parent_transaction_id from transactions t2 where t2.parent_transaction_id is not null)
                                and category_id = c.id
                                and t.date < :start_of_next_month) spent_child_transactions) as available,
                       (select -1*sum(amount) amount
                        from transactions t
                        where t.category_id = c.id
                        and t.date >= :start_of_this_month and t.date < :start_of_next_month
                        and t.id not in (select t2.parent_transaction_id from transactions t2 where t2.parent_transaction_id is not null)
                        ) spent_money,
                       c.id category_id,
                       c.name category_name
                from categories c
                where c.budget_id = :budget_id
                    and c.deleted = :deleted
                    and c.hidden = :hidden
                    and lower(c.name) like :name_like
                    and not exists(select 1 from categories x where x.parent_category_id = c.id)
                order by transaction_count desc;
            """.trimIndent()
        )
            .setParameter("payee_id", payee?.id ?: UUID.randomUUID())
            .setParameter("two_months_ago", LocalDate.now().minusMonths(2))
            .setParameter("start_of_next_month", YearMonth.now().plusMonths(1).atDay(1))
            .setParameter("start_of_this_month", YearMonth.now().atDay(1))
            .setParameter("budget_id", budget.id)
            .setParameter("deleted", false)
            .setParameter("hidden", false)
            .setParameter("name_like", "%${searchString.lowercase(Locale.getDefault())}%")
            .findList()
            .map { row ->
                CategorySumForNewTransaction(
                    transactionCount = row.getInteger("transaction_count"),
                    yetAvailable = row.getBigDecimal("available") ?: BigDecimal.ZERO,
                    alreadySpent = row.getBigDecimal("spent_money") ?: BigDecimal.ZERO,
                    categoryId = row.getUUID("category_id"),
                    categoryName = row.getString("category_name")
                )
            }
    }

    fun countLeafCategoriesForNewTransaction(
        budget: Budget,
        searchString: String,
        offset: Int = 0,
        limit: Int = Int.MAX_VALUE
    ): Int {
        return database
            .find(Category::class.java)
            .eq(Category::budget, budget)
            .containsIgnoreCase(Category::name, searchString)
            .eq(Category::deleted, false)
            .eq(Category::hidden, false)
            .isEmpty(Category::childCategories)
            .findCount()
    }

    fun findByBudget(budget: Budget): List<Category> {
        return database
            .find(Category::class.java)
            .eq(Category::budget, budget)
            .findList()
    }

    fun countCategoriesWithParentCategoryWithoutInternal(budget: Budget, parentCategory: Category?, deleted: Boolean? = false): Int {
        return queryCategoriesWithParentCategoryWithoutInternal(budget, parentCategory, deleted)
            .findCount()
    }

    fun findCategoriesWithParentCategoryWithoutInternal(budget: Budget, parentCategory: Category?, deleted: Boolean? = false): Stream<Category> {
        return queryCategoriesWithParentCategoryWithoutInternal(budget, parentCategory, deleted)
            .findStream()
    }

    private fun queryCategoriesWithParentCategoryWithoutInternal(budget: Budget, parentCategory: Category?, deleted: Boolean?): Query<Category> {
        val baseQuery = database
            .find(Category::class.java)
            .eq(Category::parentCategory, parentCategory)
            .eq(Category::budget, budget)
            .eq(Category::hidden, false)
            .not(ilike(Category::name, "%internal%"))
            .orderBy(Category::order, SortOrder.ASC)
        return if (deleted != null) {
            baseQuery.eq(Category::deleted, deleted)
        } else {
            baseQuery
        }
    }

    fun findById(id: UUID, budget: Budget): Category? {
        return database
            .find(Category::class.java)
            .eq(Category::budget, budget)
            .eq(Category::id, id)
            .findOne()
    }

    fun findIncomeCategoryForBudget(budget: Budget): Category {
        return database
            .find(Category::class.java)
            .eq(Category::budget, budget)
            .containsIgnoreCase(Category::name, "Inflow: Ready to Assign")
            .findOne()!!
    }
}
