package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Category
import eu.vnagy.openbudget.model.entites.Payee
import eu.vnagy.openbudget.model.entites.User
import io.ebean.Database
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Repository
class UserDao constructor(
    private val database: Database
): BaseDao<UUID, User>(database, User::class)
