package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.model.entites.BaseEntity
import io.ebean.Database
import org.springframework.transaction.annotation.Transactional
import kotlin.reflect.KClass

abstract class BaseDao<ID_TYPE: Any, T: BaseEntity<ID_TYPE>> constructor(
    private val database: Database,
    private val entityClass: KClass<T>
){

    @Transactional
    open fun save(entity: T) {
        database.save(entity)
    }

    @Transactional
    open fun delete(entity: T) {
        database.delete(entity)
    }

    @Transactional
    open fun deleteAll(entities: Collection<T>) {
        database.deleteAll(entities)
    }

    @Transactional
    open fun <C_TYPE: Collection<T>> saveAll(entities: C_TYPE) {
        database.saveAll(entities)
    }

    @Transactional
    open fun findById(id: ID_TYPE): T? {
        return database.find(entityClass.java, id)
    }

    @Transactional
    open fun findAll(): Collection<T> {
        return database.find(entityClass.java).findSet()
    }

}
