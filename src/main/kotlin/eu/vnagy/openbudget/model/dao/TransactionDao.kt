package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Category
import eu.vnagy.openbudget.model.entites.Transaction
import eu.vnagy.openbudget.model.pojo.AndExpression
import eu.vnagy.openbudget.model.pojo.FilterExpression
import eu.vnagy.openbudget.model.pojo.SortOrderDescriptor
import eu.vnagy.openbudget.model.utils.*
import io.ebean.Database
import io.ebean.Expr
import io.ebean.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.time.LocalDate
import java.time.YearMonth
import java.util.*
import java.util.stream.Stream

@Repository
class TransactionDao constructor(
    private val database: Database
): BaseDao<UUID, Transaction>(database, Transaction::class) {

    @Transactional
    override fun delete(entity: Transaction) {
        val backwardsTransaction = entity.transferTransaction
        if (backwardsTransaction != null) {
            backwardsTransaction.transferTransaction = null
            entity.transferTransaction = null
            val transactions = listOf(backwardsTransaction, entity)
            saveAll(transactions)
            deleteAll(transactions)
        } else {
            if (entity.childTransactions.isNotEmpty()) {
                entity.childTransactions.forEach { delete(it) }
                database.delete(Transaction::class.java, entity.id)
            } else {
                super.delete(entity)
            }
        }
    }

    fun getChildrenTransactionCount(
        account: Collection<Account>,
        parent: Transaction? = null,
        offset: Int = 0,
        limit: Int = Int.MAX_VALUE,
        filters: Collection<FilterExpression> = emptyList()
    ): Int {
        return transactionQuery(account, parent, offset, limit, emptyList(), filters).findCount()
    }

    fun findTransactionStream(
        account: Collection<Account>,
        parent: Transaction? = null,
        offset: Int = 0,
        limit: Int = Int.MAX_VALUE,
        sortOrders: List<SortOrderDescriptor> = emptyList(),
        filters: Collection<FilterExpression> = emptyList()
    ): Stream<Transaction> {
        return transactionQuery(account, parent, offset, limit, sortOrders, filters).findStream()
    }

    private fun transactionQuery(
        account: Collection<Account>,
        parent: Transaction?,
        offset: Int,
        limit: Int,
        sortOrders: List<SortOrderDescriptor>,
        filters: Collection<FilterExpression>,
    ): Query<Transaction> {
        // TODO this subquery won't work with multi level transactions. And it's possibly inefficient. But it's okayish for now...
        return database
            .find(Transaction::class.java)
            .offset(offset)
            .limit(limit)
            .`in`(Transaction::account, account)
            .eq(Transaction::parentTransaction, parent)
            .orderBy(sortOrders)
            .where(Expr.or(
                AndExpression(filters.toList()).asEbeanExpression(),
                Expr.`in`(
                    Transaction::id.name,
                    database
                        .find(Transaction::class.java)
                        .alias("t1")
                        .where(filters)
                        .select(Transaction::parentTransaction.name)
                )
            ))
    }

    fun getAllIncomeInMonth(budget: Budget, yearAndMonth: YearMonth): BigDecimal {
        return database
            .sqlQuery(
                //language=PostgreSQL
                """
                select sum(t.amount) total_amount
                from transactions t, categories c
                where t.category_id = c.id
                  and lower(c.name) like '%ready to assign%'
                  and t.date <= :end_of_month
                  and t.date >= :start_of_month
            """.trimIndent())
            .setParameter("end_of_month", yearAndMonth.atEndOfMonth())
            .setParameter("start_of_month", yearAndMonth.atDay(1))
            .findOne()
            ?.getBigDecimal("total_amount") ?: BigDecimal.ZERO
    }

    fun getAllIncomeBeforeMonth(budget: Budget, yearAndMonth: YearMonth): BigDecimal {
        return database
            .sqlQuery(
                //language=PostgreSQL
                """
                select sum(t.amount) total_amount
                from transactions t, categories c, accounts a
                where t.category_id = c.id
                  and a.id = t.account_id
                  and a.budget_id = :budget_id
                  and lower(c.name) like '%ready to assign%'
                  and t.date <= :end_of_month
            """.trimIndent())
            .setParameter("end_of_month", yearAndMonth.atEndOfMonth())
            .setParameter("budget_id", budget.id)
            .findOne()
            ?.getBigDecimal("total_amount") ?: BigDecimal.ZERO
    }

    fun sumSpentAndIncomeByCategoryInMonth(category: Category, yearAndMonth: YearMonth): BigDecimal {
        return sumSpentAndIncomeByCategoryBetween(category, yearAndMonth.atDay(1), yearAndMonth.atEndOfMonth())
    }

    fun sumSpentAndIncomeByCategoryBetween(category: Category, after: LocalDate, before: LocalDate): BigDecimal {
        return database
            .sqlQuery(
                //language=PostgreSQL
                """
                select sum(t.amount) total_amount
                from transactions t
                where t.category_id = :category_id
                  and t.date <= :end_of_month
                  and t.date >= :start_of_month
            """.trimIndent())
            .setParameter("category_id", category.id)
            .setParameter("end_of_month", before)
            .setParameter("start_of_month", after)
            .findOne()
            ?.getBigDecimal("total_amount") ?: BigDecimal.ZERO
    }

    fun findByIdAndAccount(id: UUID, account: Account): Transaction? {
        return database
            .find(Transaction::class.java)
            .eq(Transaction::id, id)
            .eq(Transaction::account, account)
            .findOne()
    }

    fun findMinTransactionDateForAccounts(accounts: Collection<Account>): LocalDate? {
        return database
            .find(Transaction::class.java)
            .`in`(Transaction::account, accounts)
            .select("min(${Transaction::date.name})")
            .findSingleAttribute()
    }
}
