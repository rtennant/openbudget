package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.model.entites.BudgetForMonth
import eu.vnagy.openbudget.model.entites.Category
import eu.vnagy.openbudget.model.entites.Goal
import eu.vnagy.openbudget.model.utils.eq
import eu.vnagy.openbudget.model.utils.greaterThanOrEquals
import eu.vnagy.openbudget.model.utils.lessThanOrEquals
import io.ebean.Database
import org.springframework.stereotype.Repository
import java.math.BigDecimal
import java.time.YearMonth
import java.util.*

@Repository
class GoalDao constructor(
    private val database: Database
): BaseDao<UUID, Goal>(database, Goal::class) {

    fun findActiveGoalsForCategory(category: Category, yearMonth: YearMonth): List<Goal> {
        return database
            .find(Goal::class.java)
            .eq(Goal::category, category)
            .greaterThanOrEquals(Goal::endMonthLastDay, yearMonth.atEndOfMonth())
            .lessThanOrEquals(Goal::startMonthFirstDay, yearMonth.atDay(1))
            .findList()
    }

    fun sumBudgetedAmountForGoal(goal: Goal, yearMonth: YearMonth): BigDecimal {
        return database
            .find(BudgetForMonth::class.java)
            .eq(BudgetForMonth::category, goal.category)
            .greaterThanOrEquals(BudgetForMonth::firstDayOfMonth, goal.startMonthFirstDay)
            .lessThanOrEquals(BudgetForMonth::firstDayOfMonth, goal.endMonthLastDay) // TODO this line is not tested at all
            .lessThanOrEquals(BudgetForMonth::firstDayOfMonth, yearMonth.atDay(1))
            .findList()
            .sumOf { it.budgetedMoney } // TODO we should sum on the psql side.
    }

}
