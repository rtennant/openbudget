package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.model.entites.Currency
import eu.vnagy.openbudget.model.entites.CurrencyFormat
import eu.vnagy.openbudget.model.utils.eq
import io.ebean.Database
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import javax.annotation.PostConstruct

@Repository
class CurrencyDao constructor(
    private val database: Database
): BaseDao<String, Currency>(database, Currency::class) {
    
    @PostConstruct
    private fun setup() {
        createAllCurrencies()
    }

    private fun createAllCurrencies() {
        val missingCurrencies = java.util.Currency.getAvailableCurrencies()
            .parallelStream()
            .filter { jdkCurrency -> database.find(Currency::class.java, jdkCurrency.currencyCode) == null }
            .map { jdkCurrency -> Currency(currency = jdkCurrency.currencyCode).apply { symbol = jdkCurrency.symbol } }
            .toList()
        database.saveAll(missingCurrencies)
    }

    @Transactional
    fun getCurrencyByIsoCode(isoCode: String): Currency {
        val currency = database
            .find(Currency::class.java)
            .forUpdate()
            .eq(Currency::currency, isoCode.uppercase())
            .findOne()

        if (currency != null) {
            return currency
        } else {
            TODO("Unknown currency: $isoCode")
        }
    }

    fun findAllCurrencyFormats(): List<CurrencyFormat> {
        return database.find(CurrencyFormat::class.java).findList()
    }

}
