package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.model.entites.*
import eu.vnagy.openbudget.model.utils.*
import io.ebean.Database
import io.ebean.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class PayeeDao constructor(
    private val database: Database
): BaseDao<UUID, Payee>(database, Payee::class) {
    fun findCountForUser(
        user: User,
        budget: Budget,
        offset: Int = 0,
        limit: Int = Int.MAX_VALUE,
        search: String = ""
    ): Int {
        if (user != budget.user) {
            throw IllegalAccessException()
        }
        return findForUserQuery(budget, offset, limit, search).findCount()
    }

    private fun findForUserQuery(
        budget: Budget,
        offset: Int = 0,
        limit: Int = Int.MAX_VALUE,
        search: String = ""
    ): Query<Payee> {
        return database
            .find(Payee::class.java)
            .eq(Payee::budget, budget)
            .containsIgnoreCase(Payee::name, search)
            .offset(offset)
            .limit(limit)
    }

    fun findForUser(
        user: User,
        budget: Budget,
        offset: Int = 0,
        limit: Int = Int.MAX_VALUE,
        search: String = ""
    ): List<Payee> {
        if (user != budget.user) {
            throw IllegalAccessException()
        }
        return findForUserQuery(budget, offset, limit, search).findList()
    }
}
