package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.model.entites.*
import eu.vnagy.openbudget.model.utils.*
import io.ebean.Database
import io.ebean.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.*


@Repository
class AccountDao constructor(
    private val database: Database,
    private val categoryDao: CategoryDao
) : BaseDao<UUID, Account>(database, Account::class) {

    fun getBalanceForAccount(
        account: Account,
        transactionStatuses: Collection<TransactionStatus> = TransactionStatus.values().toList(),
    ): BigDecimal {
        val balanceByDate = getAbsoluteBalanceForAccount(account, transactionStatuses)
        return if (transactionStatuses.contains(TransactionStatus.RECONCILED)) {
            balanceByDate + account.relativeZeroBalance
        } else {
            balanceByDate
        }
    }

    fun getAbsoluteBalanceForAccount(
        account: Account,
        transactionStatuses: Collection<TransactionStatus> = TransactionStatus.values().toList()
    ): BigDecimal {
        val balanceByDate = database.sqlQuery(
            """
                select sum(${Transaction.COLUMN_AMOUNT}) amount
                  from ${Transaction.TABLE_NAME}
                 where ${Transaction.COLUMN_ACCOUNT_ID} = :account_id
                   and ${Transaction.COLUMN_PARENT_TRANSACTION_ID} is null
                   and ${Transaction.COLUMN_TRANSACTION_STATUS} in (:transaction_statuses)
            """.trimIndent()
        )
            .setParameter("account_id", account.id)
            .setParameter("transaction_statuses", transactionStatuses)
            .findOne()!!
            .getBigDecimal("amount") ?: BigDecimal.ZERO
        return balanceByDate
    }

    fun findCountForUser(
        user: User,
        budget: Budget,
        offset: Int = 0,
        limit: Int = Int.MAX_VALUE,
        search: String = ""
    ): Int {
        if (user != budget.user) {
            throw IllegalAccessException()
        }
        return getFindForUserQuery(budget, offset, limit, search).findCount()
    }

    fun findForUser(
        user: User,
        budget: Budget,
        offset: Int = 0,
        limit: Int = Int.MAX_VALUE,
        search: String = ""
    ): List<Account> {
        if (user != budget.user) {
            throw IllegalAccessException()
        }
        return getFindForUserQuery(budget, offset, limit, search).findList()
    }

    private fun getFindForUserQuery(budget: Budget, offset: Int, limit: Int, search: String): Query<Account> {
        return database
            .find(Account::class.java)
            .eq(Account::budget, budget)
            .containsIgnoreCase(Account::name, search)
            .offset(offset)
            .limit(limit)
    }

    @Transactional
    fun reconcileAccount(
        account: Account,
        amountToCreateTransaction: BigDecimal,
        dateTime: LocalDateTime = LocalDateTime.now()
    ) {
        var transaction: Transaction? = null
        if (amountToCreateTransaction.signum() != 0) {
            val inflowCategory = categoryDao.findIncomeCategoryForBudget(account.budget)
            transaction = Transaction(account = account).apply { 
                amount = amountToCreateTransaction
                date = dateTime.toLocalDate()
                category = inflowCategory
                note = "Reconciliation transaction @ $dateTime"
                status = TransactionStatus.CLEARED
            }
        }

        val reconciliation = Reconciliation(
            account = account,
            dateTime = dateTime,
            transaction = transaction
        )
        
        database.save(reconciliation)

        database
            .update(Transaction::class.java)
            .set(Transaction::status.name, TransactionStatus.RECONCILED)
            .where()
            .eq(Transaction::status, TransactionStatus.CLEARED)
            .eq(Transaction::account, account)
            .eq(Transaction::parentTransaction, null)
            .update()

        database
            .update(Transaction::class.java)
            .set(Transaction::status.name, TransactionStatus.RECONCILED)
            .where()
            .neq(Transaction::status, TransactionStatus.RECONCILED)
            .where()
            .`in`(
                Transaction::parentTransaction.name,
                database
                    .find(Transaction::class.java)
                    .alias("t2")
                    .eq(Transaction::status, TransactionStatus.RECONCILED)
                    .select(Transaction::id.name)
            )
            .update()
    }
}
