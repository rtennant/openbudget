package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Transaction
import eu.vnagy.openbudget.model.entites.User
import eu.vnagy.openbudget.model.utils.eq
import eu.vnagy.openbudget.model.utils.`in`
import eu.vnagy.openbudget.model.utils.lessThanOrEquals
import io.ebean.Database
import org.springframework.stereotype.Repository
import java.math.BigDecimal
import java.time.LocalDate
import java.util.*

@Repository
class BudgetDao constructor(
    private val database: Database
): BaseDao<UUID, Budget>(database, Budget::class) {

    fun findByUser(user: User): Collection<Budget> {
        return database
            .find(Budget::class.java)
            .eq(Budget::user, user)
            .findList()
    }

    fun findByIdAndUser(id: UUID, user: User): Budget? {
        return database
            .find(Budget::class.java)
            .eq(Budget::user, user)
            .eq(Budget::id, id)
            .findOne()
    }

    fun getAbsoluteBalanceForAccountsByDate(
        accounts: Collection<Account>,
        dateBefore: LocalDate
    ): BigDecimal {
        return database
            .find(Transaction::class.java)
            .`in`(Transaction::account, accounts)
            .lessThanOrEquals(Transaction::date, dateBefore)
            .eq(Transaction::parentTransaction, null)
            .select("sum(${Transaction::amount.name})")
            .findSingleAttribute<BigDecimal>() ?: BigDecimal.ZERO
    }

    fun calculateNetWorthBetweenDates(
        accounts: Collection<Account>, 
        fromDate: LocalDate, 
        toDate: LocalDate
    ): List<Pair<LocalDate, BigDecimal>> {
        val findList = database.sqlQuery(
            """
            select days.days as date, coalesce(sum(t.amount), 0) sum
            from generate_series(:from_date, :to_date, interval '1 day') days
                 left outer join transactions t on t.date <= days.days
            where t.parent_transaction_id is null
              and t.account_id in (:account_ids)
            group by days.days
            order by days
        """.trimIndent()
        )
            .setParameter("from_date", fromDate)
            .setParameter("to_date", toDate)
            .setParameter("account_ids", accounts.map { it.id })
            .findList()
        return findList.map { 
                it.getDate("date").toLocalDate() to it.getBigDecimal("sum")
            }
    }
}
