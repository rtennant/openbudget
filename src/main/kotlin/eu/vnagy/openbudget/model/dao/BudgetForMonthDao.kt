package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.BudgetForMonth
import eu.vnagy.openbudget.model.entites.Category
import eu.vnagy.openbudget.model.utils.eq
import io.ebean.Database
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Repository
import java.math.BigDecimal
import java.time.YearMonth
import java.util.*

@Repository
class BudgetForMonthDao constructor(
    private val database: Database,
    private val transactionDao: TransactionDao
) : BaseDao<UUID, BudgetForMonth>(database, BudgetForMonth::class) {


    companion object {
        private val logger = LoggerFactory.getLogger(BudgetForMonthDao::class.java)
    }
    fun findBudgetedForCategoryAndMonth(category: Category, yearAndMonth: YearMonth): BudgetForMonth? {
        return database
            .find(BudgetForMonth::class.java)
            .eq(BudgetForMonth::category, category)
            .eq(BudgetForMonth::firstDayOfMonth, yearAndMonth.atDay(1))
            .findOne()
    }

    fun calcAvailableMoneyUntilMonthInCategory(category: Category, yearAndMonth: YearMonth): BigDecimal? {
        return database
            .sqlQuery(
                //language=PostgreSQL
                """
                select (coalesce(budgeted.budgeted, 0) + spent_child_transactions.spent) as available
                from (select sum(budgeted_money) budgeted
                      from budgets_for_months
                      where category_id = :category_id
                        and first_day_of_month <= :date) budgeted,
                     (select sum(t.amount) spent
                      from transactions t
                      where t.id not in (select t2.parent_transaction_id from transactions t2 where t2.parent_transaction_id is not null)
                        and category_id = :category_id
                        and t.date <= :date
                      ) spent_child_transactions;
            """.trimIndent())
            .setParameter("category_id", category.id)
            .setParameter("date", yearAndMonth.atEndOfMonth())
            .findOne()
            ?.getBigDecimal("available")
    }

    fun getAllBudgetedInMonth(budget: Budget, yearAndMonth: YearMonth): BigDecimal {
        return database.sqlQuery(
            //language=PostgreSQL
            """
            select sum(b.budgeted_money) budgeted
            from budgets_for_months b, categories c
            where b.category_id = c.id
              and lower(c.name) not like '%ready to assign%'
              and b.first_day_of_month = :start_of_month
        """.trimIndent())
            .setParameter("start_of_month", yearAndMonth.atDay(1))
            .findOne()
            ?.getBigDecimal("budgeted") ?: BigDecimal.ZERO
    }

    fun getAllBudgetedBeforeMonth(budget: Budget, yearAndMonth: YearMonth): BigDecimal {
        return database.sqlQuery(
            //language=PostgreSQL
            """
            select sum(b.budgeted_money) budgeted
            from budgets_for_months b, categories c
            where b.category_id = c.id
              and b.budget_id = :budget_id
              and lower(c.name) not like '%ready to assign%'
              and b.first_day_of_month <= :start_of_month
        """.trimIndent())
            .setParameter("start_of_month", yearAndMonth.atDay(1))
            .setParameter("budget_id", budget.id)
            .findOne()
            ?.getBigDecimal("budgeted") ?: BigDecimal.ZERO
    }

    fun getAllFreeMoneyAtMonth(budget: Budget, yearAndMonth: YearMonth): BigDecimal {
        return transactionDao.getAllIncomeBeforeMonth(budget, yearAndMonth) - getAllBudgetedBeforeMonth(budget, yearAndMonth)
    }

    fun getByCategoryAndMonth(category: Category, yearAndMonth: YearMonth): BudgetForMonth? {
        val one = database
            .find(BudgetForMonth::class.java)
            .eq(BudgetForMonth::category, category)
            .eq(BudgetForMonth::firstDayOfMonth, yearAndMonth.atDay(1))
            .findOne()
        if (one == null) {
            logger.info("Cannot find budgeted value for: {}/{}", yearAndMonth, category.name)
        }
        return one
    }
}
