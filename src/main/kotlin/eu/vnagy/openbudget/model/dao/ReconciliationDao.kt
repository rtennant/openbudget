package eu.vnagy.openbudget.model.dao

import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Reconciliation
import eu.vnagy.openbudget.model.utils.SortOrder
import eu.vnagy.openbudget.model.utils.eq
import eu.vnagy.openbudget.model.utils.limit
import eu.vnagy.openbudget.model.utils.orderBy
import io.ebean.Database
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class ReconciliationDao constructor(
    private val database: Database
): BaseDao<UUID, Reconciliation>(database, Reconciliation::class) {
    
    fun findReconciliationsForAccount(account: Account): List<Reconciliation> {
        return database
            .find(Reconciliation::class.java)
            .eq(Reconciliation::account, account)
            .findList()
    }
    
    fun findLatestReconciliationForAccount(account: Account): Reconciliation? {
        return database
            .find(Reconciliation::class.java)
            .eq(Reconciliation::account, account)
            .orderBy(Reconciliation::dateTime, SortOrder.DESC)
            .limit(1)
            .findOne()
    }
}