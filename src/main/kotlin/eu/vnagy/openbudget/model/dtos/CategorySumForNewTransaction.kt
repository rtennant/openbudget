package eu.vnagy.openbudget.model.dtos

import java.math.BigDecimal
import java.util.*

data class CategorySumForNewTransaction(
    val transactionCount: Int,
    val yetAvailable: BigDecimal,
    val alreadySpent: BigDecimal,
    val categoryId: UUID,
    val categoryName: String
)
