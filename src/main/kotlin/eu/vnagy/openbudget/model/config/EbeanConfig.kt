@file:Suppress("DEPRECATION")

package eu.vnagy.openbudget.model.config

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import io.ebean.Database
import io.ebean.DatabaseFactory
import io.ebean.config.DatabaseConfig
import io.ebean.spring.txn.SpringJdbcTransactionManager
import liquibase.integration.spring.SpringLiquibase
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource

@Configuration
class EbeanConfig {

    @Autowired
    private lateinit var dataSource: DataSource

    @Autowired
    private lateinit var liquibase: SpringLiquibase

    @Bean
    fun createEbeanDatabase(): Database {
        // TODO https://github.com/hexagonframework/spring-data-ebean/issues/9
        val config = DatabaseConfig()
        config.dataSource = dataSource;
        config.externalTransactionManager = SpringJdbcTransactionManager();
        config.objectMapper = JsonMapper.builder()
            .addModule(Jdk8Module())
            .addModule(JavaTimeModule())
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .build()
        return DatabaseFactory.create(config)
    }
}

