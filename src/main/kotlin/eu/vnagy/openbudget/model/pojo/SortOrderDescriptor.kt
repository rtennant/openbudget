package eu.vnagy.openbudget.model.pojo

import eu.vnagy.openbudget.model.utils.SortOrder

data class SortOrderDescriptor(
    val property: String,
    val sortOrder: SortOrder
)
