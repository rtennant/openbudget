package eu.vnagy.openbudget.model.pojo

import io.ebean.Expr
import io.ebean.Expression

interface FilterExpression {
    fun asEbeanExpression(): Expression
}

data class EqualsFilterExpression(
    private val property: String,
    private val value: Any?
) : FilterExpression {

    override fun asEbeanExpression(): Expression {
        if (value != null) {
            return Expr.eq(property, value)
        } else {
            return Expr.isNull(property)
        }
    }

}

data class LessThanFilterExpression(
    private val property: String,
    private val value: Any?
) : FilterExpression {

    override fun asEbeanExpression(): Expression {
        if (value != null) {
            return Expr.lt(property, value)
        } else {
            return Expr.isNull(property)
        }
    }

}
data class LessThanOrEqualsFilterExpression(
    private val property: String,
    private val value: Any?
) : FilterExpression {

    override fun asEbeanExpression(): Expression {
        if (value != null) {
            return Expr.le(property, value)
        } else {
            return Expr.isNull(property)
        }
    }

}
data class GreaterThanFilterExpression(
    private val property: String,
    private val value: Any?
) : FilterExpression {

    override fun asEbeanExpression(): Expression {
        if (value != null) {
            return Expr.gt(property, value)
        } else {
            return Expr.isNull(property)
        }
    }

}
data class GreaterThanOrEqualsFilterExpression(
    private val property: String,
    private val value: Any?
) : FilterExpression {

    override fun asEbeanExpression(): Expression {
        if (value != null) {
            return Expr.ge(property, value)
        } else {
            return Expr.isNull(property)
        }
    }

}

data class NotExpression(
    private val expression: FilterExpression
) : FilterExpression {

    override fun asEbeanExpression(): Expression = Expr.not(expression.asEbeanExpression())

}

data class OrExpression(
    private val expressions: List<FilterExpression>
): FilterExpression {
    override fun asEbeanExpression(): Expression {
        return mapExpressionsToOr(expressions)
    }

    private fun mapExpressionsToOr(expressions: List<FilterExpression>): Expression {
        return when(expressions.size) {
            0 -> Expr.raw("1=1")
            1 -> return expressions.first().asEbeanExpression()
            2 -> return Expr.or(expressions[0].asEbeanExpression(), expressions[1].asEbeanExpression())
            else -> return Expr.or(expressions.first().asEbeanExpression(), mapExpressionsToOr(expressions.subList(1, expressions.size)))
        }
    }
}

data class AndExpression(
    private val expressions: List<FilterExpression>
): FilterExpression {
    override fun asEbeanExpression(): Expression {
        return mapExpressionsToOr(expressions)
    }

    private fun mapExpressionsToOr(expressions: List<FilterExpression>): Expression {
        return when(expressions.size) {
            0 -> Expr.raw("1=1")
            1 -> return expressions.first().asEbeanExpression()
            2 -> return Expr.and(expressions[0].asEbeanExpression(), expressions[1].asEbeanExpression())
            else -> return Expr.and(expressions.first().asEbeanExpression(), mapExpressionsToOr(expressions.subList(1, expressions.size)))
        }
    }
}
