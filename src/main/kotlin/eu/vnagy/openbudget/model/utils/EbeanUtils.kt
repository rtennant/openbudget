package eu.vnagy.openbudget.model.utils

import eu.vnagy.openbudget.model.pojo.FilterExpression
import eu.vnagy.openbudget.model.pojo.SortOrderDescriptor
import io.ebean.Expr
import io.ebean.Expression
import io.ebean.ExpressionList
import io.ebean.Query
import kotlin.internal.OnlyInputTypes
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty1

fun <T, @OnlyInputTypes R: Any?> Query<T>.eq(kProperty1: KProperty1<T, R>, value: R): Query<T> {
    return if (value != null) {
        where().eq(kProperty1.name, value).query()
    } else {
        where().isNull(kProperty1.name).query()
    }
}
fun <T, @OnlyInputTypes R: Any?> ExpressionList<T>.eq(kProperty1: KProperty1<T, R>, value: R): ExpressionList<T> {
    return if (value != null) {
        eq(kProperty1.name, value)
    } else {
        isNull(kProperty1.name)
    }
}
fun <T, @OnlyInputTypes R: Any?> ExpressionList<T>.neq(kProperty1: KProperty1<T, R>, value: R): ExpressionList<T> {
    return if (value != null) {
        not(Expr.eq(kProperty1.name, value))
    } else {
        TODO()
    }
}
fun <T, @OnlyInputTypes R: Any?> Query<T>.eq(propertyPath: String, value: R): Query<T> {
    return if (value != null) {
        where().eq(propertyPath, value).query()
    } else {
        where().isNull(propertyPath).query()
    }
}
fun <T, @OnlyInputTypes R: Any?> Query<T>.orderBy(kProperty1: KProperty1<T, R>, sortOrder: SortOrder): Query<T> {
    return when(sortOrder) {
        SortOrder.ASC -> orderBy("${kProperty1.name} asc")
        SortOrder.DESC -> orderBy("${kProperty1.name} desc")
    }
}
fun <T> Query<T>.orderBy(property: String, sortOrder: SortOrder): Query<T> {
    return when(sortOrder) {
        SortOrder.ASC -> orderBy("$property asc")
        SortOrder.DESC -> orderBy("$property desc")
    }
}
fun <T> Query<T>.orderBy(sortOrderDescriptors: List<SortOrderDescriptor>): Query<T> {
    for (sortOrderDescriptor in sortOrderDescriptors) {
        orderBy(sortOrderDescriptor.property, sortOrderDescriptor.sortOrder)
    }
    return this
}

fun <T, R: Any> Query<T>.lessThan(kProperty1: KProperty1<T, R?>, value: R): Query<T> {
    return where().lt(kProperty1.name, value).query()
}

fun <T, R: Any> Query<T>.lessThanOrEquals(kProperty1: KProperty1<T, R?>, value: R): Query<T> {
    return where().le(kProperty1.name, value).query()
}

fun <T, R: Any> Query<T>.greaterThanOrEquals(kProperty1: KProperty1<T, R?>, value: R): Query<T> {
    return where().ge(kProperty1.name, value).query()
}

fun <T> Query<T>.limit(limit: Int): Query<T> {
    return setMaxRows(limit)
}

fun <T, R: Any> Query<T>.`in`(kProperty1: KProperty1<T, R?>, values: Collection<R>): Query<T> {
    return where().`in`(kProperty1.name, values).query()
}
fun <T> Query<T>.offset(offset: Int): Query<T> {
    return setFirstRow(offset)
}

fun <T, @OnlyInputTypes R: Any?> eq(kProperty1: KProperty1<T, R>, value: R): Expression {
    return if (value != null) {
        Expr.eq(kProperty1.name, value)
    } else {
        Expr.isNull(kProperty1.name)
    }
}
fun <T, @OnlyInputTypes R: String?> ilike(kProperty1: KProperty1<T, R>, value: R): Expression {
    return if (value != null) {
        Expr.ilike(kProperty1.name, value)
    } else {
        Expr.isNull(kProperty1.name)
    }
}
fun <T, R: Any> lessThanOrEquals(kProperty1: KProperty1<T, R?>, value: R): Expression {
    return Expr.le(kProperty1.name, value)
}
fun <T, R: Any> greaterThanOrEquals(kProperty1: KProperty1<T, R?>, value: R): Expression {
    return Expr.ge(kProperty1.name, value)
}

fun <T> Query<T>.or(vararg expressions: Expression): Query<T> {
    val buildOrConditions = buildOrConditions(expressions.toList())
    return if (buildOrConditions != null) {
        where(buildOrConditions)
    } else {
        this
    }
}
fun <T> Query<T>.not(expression: Expression): Query<T> {
    return where(Expr.not(expression))
}

private fun buildOrConditions(expressions: List<Expression>): Expression? {
    return when (expressions.size) {
        0 -> null
        1 -> expressions[0]
        2 -> Expr.or(expressions[0], expressions[1])
        else -> Expr.or(expressions[0], buildOrConditions(expressions.subList(1, expressions.size)))
    }
}

enum class SortOrder {
    ASC,
    DESC
}

fun <T> Query<T>.where(filters: Collection<FilterExpression>): Query<T> {
    for (filter in filters) {
        where(filter.asEbeanExpression())
    }
    return this
}

fun <T> Query<T>.contains(property: KProperty1<T, String>, value: String): Query<T>  {
    return where().contains(property.name, value).query()
}

fun <T> Query<T>.containsIgnoreCase(property: KProperty1<T, String>, value: String): Query<T>  {
    return where().icontains(property.name, value).query()
}

fun <T> Query<T>.isEmpty(kProperty1: KMutableProperty1<T, out Collection<T>>): Query<T> {
    return where().isEmpty(kProperty1.name).query()
}

fun <T> Query<T>.isNotEmpty(kProperty1: KMutableProperty1<T, out Collection<T>>): Query<T> {
    return where().isNotEmpty(kProperty1.name).query()
}
