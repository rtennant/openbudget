package eu.vnagy.openbudget.services.utils.date

import java.time.LocalDate

data class LocalDateRange(
    override val start: LocalDate,
    override val endInclusive: LocalDate
): Iterable<LocalDate>, ClosedRange<LocalDate> {
    
    override fun iterator(): Iterator<LocalDate> {
        return object : Iterator<LocalDate> {
            private var currentDate = start
            
            override fun hasNext(): Boolean {
                return currentDate <= endInclusive
            }

            override fun next(): LocalDate {
                val next = currentDate
                currentDate = currentDate.plusDays(1)
                return next
            }
        } 
    }
    
}
