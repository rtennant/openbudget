package eu.vnagy.openbudget.services

import eu.vnagy.openbudget.services.utils.date.LocalDateRange
import java.time.LocalDate

operator fun LocalDate.rangeTo(that: LocalDate): LocalDateRange {
    return LocalDateRange(this, that)
}