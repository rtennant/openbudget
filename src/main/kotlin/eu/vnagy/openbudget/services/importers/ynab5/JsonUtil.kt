package eu.vnagy.openbudget.services.importers.ynab5

import com.google.gson.JsonElement

val JsonElement.asStringOrNull: String?
    get() = if (isJsonNull) {
        null
    } else if (isJsonPrimitive) {
        asString
    } else {
        null
    }
