package eu.vnagy.openbudget.services.importers.ynab5

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import eu.vnagy.openbudget.model.dao.*
import eu.vnagy.openbudget.model.entites.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.io.InputStream
import java.time.LocalDate
import java.util.*
import java.util.concurrent.ConcurrentHashMap

@Service
class YNAB5ImporterService {

    @Autowired
    private lateinit var currencyDao: CurrencyDao

    @Autowired
    private lateinit var budgetDao: BudgetDao

    @Autowired
    private lateinit var accountDao: AccountDao

    @Autowired
    private lateinit var categoryDao: CategoryDao

    @Autowired
    private lateinit var payeeDao: PayeeDao

    @Autowired
    private lateinit var transactionDao: TransactionDao

    @Autowired
    private lateinit var budgetForMonthDao: BudgetForMonthDao

    companion object {
        private val gson = Gson()
        private val logger = LoggerFactory.getLogger(YNAB5ImporterService::class.java)
    }

    @Transactional
    fun importYnab5Export(user: User, inputStream: InputStream): Budget {
        val parsedExportJson = gson.fromJson(inputStream.reader(), JsonObject::class.java)
        val parsedBudgetJson = parsedExportJson.getAsJsonObject("data").getAsJsonObject("budget")
        val parsedAccountsJson = parsedBudgetJson.getAsJsonArray("accounts")
        val parsedCategoriesJson = parsedBudgetJson.getAsJsonArray("categories")
        val parsedParentCategoriesJson = parsedBudgetJson.getAsJsonArray("category_groups")
        val parsedMonthsJson = parsedBudgetJson.getAsJsonArray("months")
        val parsedPayeesJson = parsedBudgetJson.getAsJsonArray("payees")
        val parsedTransactionsJson = parsedBudgetJson.getAsJsonArray("transactions")
        val parsedSubTransactionsJson = parsedBudgetJson.getAsJsonArray("subtransactions")
//        TODO ez azért volt itt, mert az eredeti budget_for_month importot elbasztam
//        val budget = budgetDao.findById(UUID.fromString("bce0b9f6-334c-4694-85c3-000000000000"))!!
//        val categoriesById = categoryDao.findByBudget(budget).associateBy { it.id }
        val budget = parseAndSaveBudget(user, parsedBudgetJson)
        val accountsById = parseAndSaveAccountsById(budget, parsedAccountsJson)
        val categoriesById = parseAndSaveCategoriesById(budget, parsedParentCategoriesJson, parsedCategoriesJson)
        val budgetsByMonths = parseAndSaveBudgetsByMonths(budget, categoriesById, parsedMonthsJson)
        val payeesById = parseAndSavePayeesById(budget, parsedPayeesJson)
        parseAndSaveTransactions(parsedTransactionsJson, parsedSubTransactionsJson, accountsById, payeesById, categoriesById)
        return budget
    }

    private fun parseAndSaveTransactions(
        parsedTransactionsJson: JsonArray,
        parsedSubTransactionsJson: JsonArray,
        accountsById: Map<UUID, Account>,
        payeesById: Map<UUID, Payee>,
        categoriesById: Map<UUID, Category>
    ) {
        val transactionIdMap: ConcurrentHashMap<String, UUID> = ConcurrentHashMap()

        fun getTransactionIdFromMap(stringId: String): UUID {
            return transactionIdMap.computeIfAbsent(stringId) {
                try {
                    UUID.fromString(it)
                } catch (e: Exception) {
                    UUID.randomUUID()
                }
            }
        }

        val transactionsById = parsedTransactionsJson
            .map { it.asJsonObject }
            .map {
                Transaction(
                    id = getTransactionIdFromMap(it["id"].asString),
                    account = accountsById[UUID.fromString(it["account_id"].asString)]!!,
                    parentTransaction = null
                ).apply {
                    category = categoriesById[it["category_id"]?.asStringOrNull?.let { categoryId -> UUID.fromString(categoryId) }]
                    payee = payeesById[it["payee_id"]?.asStringOrNull?.let { payeeId -> UUID.fromString(payeeId) }]
                    note = it["memo"]?.asStringOrNull
                    date = LocalDate.parse(it["date"].asString)
                    amount = it.getAsJsonPrimitive("amount").asBigDecimal.movePointLeft(3)
                    status = parseYnabTransactionStatus(it.getAsJsonPrimitive("cleared")?.asString)
                }
            }
            .associateBy { it.id }

        val subTransactions = parsedSubTransactionsJson
            .map { it.asJsonObject }
            .map { it to transactionsById[getTransactionIdFromMap(it["transaction_id"].asString)]!! }
            .map { (asJson, parentTransaction) ->
                Transaction(
                    id = getTransactionIdFromMap(asJson["id"].asString),
                    account = parentTransaction.account,
                    parentTransaction = parentTransaction
                ).apply {
                    category = categoriesById[asJson["category_id"]?.asStringOrNull?.let { categoryId -> UUID.fromString(categoryId) }]
                    amount = asJson.getAsJsonPrimitive("amount").asBigDecimal.movePointLeft(3)
                    note = asJson["memo"]?.asStringOrNull
                    date = parentTransaction.date
                    payee = payeesById[asJson["payee_id"]?.asStringOrNull?.let { payeeId -> UUID.fromString(payeeId) }]
                }
            }

        val allTransactionsById = (transactionsById.values + subTransactions).associateBy { it.id }
        transactionDao.saveAll(allTransactionsById.values)

        // fix based on the `transfer_transaction_id` field
        logger.info("Matching top level transactions based on `transfer_transaction_id`.")
        parsedTransactionsJson
            .map { it.asJsonObject }
            .filter { it["transfer_transaction_id"].isJsonNull.not() }
            .forEach {
                val transaction = transactionsById[getTransactionIdFromMap(it["id"].asString)]!!
                val transferTransaction = transactionsById[getTransactionIdFromMap(it["transfer_transaction_id"].asString)]!!

                if ((transaction.amount + transferTransaction.amount).toInt() != 0) {
                    throw IllegalStateException("TODO")
                } else {
                    transaction.transferTransaction = transferTransaction
                }
            }

        fun tryToMatchRemoteTransactionsBasedOnBaseList(possibleRemoteTransactions: Collection<Transaction>, oneUnmatched: Transaction, comment: String) {
            val possiblePairsForIt = possibleRemoteTransactions
                .filter { it.transferTransaction == null && it.payee == null } // && it.category == null
                .filter { it.date == oneUnmatched.date }
                .filter { it.note == oneUnmatched.note }
                .filter { (it.amount + oneUnmatched.amount).toInt() == 0 }
            if (possiblePairsForIt.isEmpty()) {
                throw IllegalStateException("TODO")
            } else {
                val firstPossibleMatch = possiblePairsForIt.first()
                if (possiblePairsForIt.size > 1) {
                    logger.info(
                        "There are multiple (`{}`) possible pairs for transaction (`{}`). Picking the first one: `{}`",
                        possiblePairsForIt.size, oneUnmatched.id, firstPossibleMatch.id
                    )
                } else {
                    logger.info(
                        "There is exactly one possible pair for transaction (`{}`). Picking the only one: `{}`",
                        oneUnmatched.id, firstPossibleMatch.id
                    )
                }
                oneUnmatched.transferTransaction = firstPossibleMatch
                oneUnmatched.note += " - ${comment}"
                firstPossibleMatch.transferTransaction = oneUnmatched
                firstPossibleMatch.note += " - ${comment}"
            }
        }

        logger.info("Matching sub-transactions based on `transfer_account_id`.")
        parsedSubTransactionsJson
            .map { it.asJsonObject }
            .filter { it["transfer_account_id"].isJsonNull.not() }
            .forEach { jsonObject ->
                val remoteAccount: Account = accountsById[UUID.fromString(jsonObject["transfer_account_id"].asString)]!!
                val unmatchedTransaction = allTransactionsById[getTransactionIdFromMap(jsonObject["id"].asString)]!!
                if (!(unmatchedTransaction.transferTransaction == null && (unmatchedTransaction.category == null || !remoteAccount.onBudget))) {
                    throw IllegalStateException("TODO - ezeknek mindnek kellene remote-ot keresni")
                }

                val possibleRemoteTransactions = allTransactionsById.values
                    .filter { possibleRemoteTransaction -> possibleRemoteTransaction.account.id == remoteAccount.id }
                    .filter { possibleRemoteTransaction -> !remoteAccount.onBudget || possibleRemoteTransaction.category == null }

                tryToMatchRemoteTransactionsBasedOnBaseList(possibleRemoteTransactions, unmatchedTransaction, "Automatically matched during import")
            }

        // fix based on semi-matching
        logger.info("Matching sub-transactions based on date and amount heuristics.")
        var hasToDoNewRound = true
        while (hasToDoNewRound) {
            hasToDoNewRound = false
            val oneUnmatched = allTransactionsById
                .values
                .asSequence()
                .filter { it.transferTransaction == null && it.category == null && it.payee == null }
                .filter { possibleOneUnmatched -> allTransactionsById.values.none { possibleSubTransaction -> possibleSubTransaction.parentTransaction?.id == possibleOneUnmatched.id } }
                .firstOrNull()
            if (oneUnmatched != null) {
                hasToDoNewRound = true
                tryToMatchRemoteTransactionsBasedOnBaseList(allTransactionsById.values, oneUnmatched, "Automatically matched during import based on the date & amount & comment")
            }
        }

        transactionDao.saveAll(allTransactionsById.values)
    }

    private fun parseYnabTransactionStatus(statusString: String?): TransactionStatus {
        return when (statusString) {
            "cleared" -> TransactionStatus.CLEARED
            "uncleared" -> TransactionStatus.UNCLEARED
            "reconciled" -> TransactionStatus.RECONCILED
            null -> TransactionStatus.UNCLEARED
            else -> TODO()
        }
    }

    private fun parseAndSavePayeesById(budget: Budget, parsedPayeesJson: JsonArray): Map<UUID, Payee> {
        val payees = parsedPayeesJson
            .map { it.asJsonObject }
            .filter { it["transfer_account_id"].isJsonNull }
            .map {
                Payee(
                    id = UUID.fromString(it["id"].asString),
                    budget = budget
                ).apply {
                    name = it["name"].asString
                }
            }
        payeeDao.saveAll(payees)
        return payees.associateBy { it.id }
    }

    private fun parseAndSaveBudgetsByMonths(budget: Budget, categoriesById: Map<UUID, Category>, parsedMonthsJson: JsonArray): Collection<BudgetForMonth> {
        val parsedBudgetsForMonth = parsedMonthsJson
            .map { it.asJsonObject }
            .flatMap {
                val month = LocalDate.parse(it.get("month").asString)
                it.getAsJsonArray("categories").map { category -> month to category.asJsonObject }
            }
            .map {
                BudgetForMonth(
                    budget = budget,
                    category = categoriesById[UUID.fromString(it.second["id"].asString)]!!,
                    firstDayOfMonth = it.first,
                ).apply {
                    budgetedMoney = it.second.getAsJsonPrimitive("budgeted").asBigDecimal.movePointLeft(3)
                }
            }
        budgetForMonthDao.saveAll(parsedBudgetsForMonth)
        return parsedBudgetsForMonth
    }

    private fun parseAndSaveCategoriesById(budget: Budget, parsedParentCategoriesJson: JsonArray, parsedCategoriesJson: JsonArray): Map<UUID, Category> {
        val allParentCategories = parsedParentCategoriesJson
            .map {
                val parsedAccountJson = it.asJsonObject
                Category(
                    id = UUID.fromString(parsedAccountJson["id"].asString),
                    budget = budget
                ).apply {
                    name = parsedAccountJson["name"].asString
                    hidden = parsedAccountJson["hidden"].asBoolean
                    deleted = parsedAccountJson["deleted"].asBoolean
                }
            }.associateBy { it.id }
        val allCategories = parsedCategoriesJson
            .map {
                val parsedAccountJson = it.asJsonObject
                Category(
                    id = UUID.fromString(parsedAccountJson["id"].asString),
                    budget = budget
                ).apply {
                    name = parsedAccountJson["name"].asString
                    parentCategory = allParentCategories[getCategoryIdWithDeletableCategory(parsedAccountJson)]
                    hidden = parsedAccountJson["hidden"].asBoolean
                    deleted = parsedAccountJson["deleted"].asBoolean
                }
            }
        val parentAndSubCategories = (allParentCategories.values + allCategories)
        categoryDao.saveAll(parentAndSubCategories)
        return parentAndSubCategories.associateBy { it.id }
    }

    private fun getCategoryIdWithDeletableCategory(parsedAccountJson: JsonObject): UUID {
        val parentCategoryId = if (!parsedAccountJson["original_category_group_id"].isJsonNull) {
            parsedAccountJson["original_category_group_id"]
        } else {
            parsedAccountJson["category_group_id"]
        }
        return UUID.fromString(parentCategoryId.asString)
    }

    private fun parseAndSaveAccountsById(budget: Budget, parsedAccountsJson: JsonArray): Map<UUID, Account> {
        val allAccounts = parsedAccountsJson
            .map {
                val parsedAccountJson = it.asJsonObject
                Account(
                    id = UUID.fromString(parsedAccountJson["id"].asString),
                    budget = budget,
                    currency = budget.mainCurrency,
                ).apply {
                    name = parsedAccountJson["name"].asString
                    onBudget = parsedAccountJson["on_budget"].asBoolean
                    closed = parsedAccountJson["closed"].asBoolean
                }
            }
        accountDao.saveAll(allAccounts)
        return allAccounts.associateBy { it.id }
    }

    private fun parseAndSaveBudget(user: User, parsedBudgetJson: JsonObject): Budget {
        val isoCode = parsedBudgetJson["currency_format"].asJsonObject["iso_code"].asString
        val currency = currencyDao.getCurrencyByIsoCode(isoCode)
        val budget = Budget(
            id = UUID.fromString(parsedBudgetJson["id"].asString),
            user = user,
            name = parsedBudgetJson["name"].asString,
            mainCurrency = currency
        )
        budgetDao.save(budget)
        return budget
    }

}
