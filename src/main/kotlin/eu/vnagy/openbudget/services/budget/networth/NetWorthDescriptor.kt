package eu.vnagy.openbudget.services.budget.networth

import java.math.BigDecimal
import java.time.LocalDate

data class NetWorthDescriptor(
    val netWorthByDate: Map<LocalDate, BigDecimal>
)
