package eu.vnagy.openbudget.services.budget.montecarlo

import java.math.BigDecimal
import java.time.LocalDate

data class MonteCarloSimulationResultDescriptor(
    val orderedMonteCarloResults: List<SingleMonteCarloSimulationResult>,
    val factDataFromPast: Map<LocalDate, BigDecimal>,
)

data class SingleMonteCarloSimulationResult(
    val predictedValueByDate: Map<LocalDate, BigDecimal>,
)
