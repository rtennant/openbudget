package eu.vnagy.openbudget.services.budget.networth

import eu.vnagy.openbudget.model.dao.BudgetDao
import eu.vnagy.openbudget.model.entites.Account
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class BudgetNetWorthService {
    
    @Autowired
    private lateinit var budgetDao: BudgetDao
    
    fun calculateNetWorthBetweenDates(
        accounts: Collection<Account>,
        fromDate: LocalDate,
        toDate: LocalDate = LocalDate.now()
    ): NetWorthDescriptor {
        val netWorthByDate = budgetDao.calculateNetWorthBetweenDates(accounts, fromDate, toDate)
        return NetWorthDescriptor(netWorthByDate.toMap())
    }
}