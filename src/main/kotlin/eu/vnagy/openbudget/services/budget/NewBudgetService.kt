package eu.vnagy.openbudget.services.budget

import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.model.dao.BudgetDao
import eu.vnagy.openbudget.model.dao.CategoryDao
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Category
import eu.vnagy.openbudget.model.entites.Currency
import eu.vnagy.openbudget.model.entites.CurrencyFormat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class NewBudgetService {

    @Autowired
    private lateinit var budgetDao: BudgetDao

    @Autowired
    private lateinit var categoryDao: CategoryDao

    @Autowired
    private lateinit var numberFormatServices: NumberFormatServices


    fun createNewBudget(budget: Budget) {
        budgetDao.save(budget)
        val internalMasterCategory = Category(budget = budget).apply {
            name = "Internal Master Category"
            hidden = true
        }
        categoryDao.save(internalMasterCategory)
        val toBeBudgetedCategory = Category(budget = budget).apply {
            name = "Inflow: Ready to Assign"
            parentCategory = internalMasterCategory
            hidden = true
        }
        categoryDao.save(toBeBudgetedCategory)
    }
    
    fun formatNumber(number: Number, currency: Currency, format: CurrencyFormat): String {
        return numberFormatServices.formatNumber(number, currency, format)
    }
}
