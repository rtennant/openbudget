package eu.vnagy.openbudget.services.budget.montecarlo

import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.services.budget.networth.BudgetNetWorthService
import eu.vnagy.openbudget.services.budget.networth.NetWorthDescriptor
import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.apache.commons.math3.random.JDKRandomGenerator
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.concurrent.atomic.AtomicInteger
import java.util.stream.IntStream
import kotlin.math.exp
import kotlin.streams.asSequence
import org.apache.commons.math3.random.RandomGenerator as ApacheRandomGenerator
import org.apache.commons.math3.util.Pair as ApachePair

@Service
class BudgetMonteCarloSimulationService(
    private val netWorthService: BudgetNetWorthService,
) {

    private val random: ApacheRandomGenerator = JDKRandomGenerator()

    companion object {
        private val logger = LoggerFactory.getLogger(BudgetMonteCarloSimulationService::class.java)
    }

    fun createMonteCarloSimulations(
        simulationNumber: Int,
        accounts: Collection<Account>,
        factDataSince: LocalDate,
        factDataUntil: LocalDate = LocalDate.now(),
        predictUntil: LocalDate = LocalDate.now().plusYears(10),
        predictionProgressCallback: (Double) -> Unit = {}
    ): MonteCarloSimulationResultDescriptor {
        val factData = netWorthService.calculateNetWorthBetweenDates(accounts, factDataSince, factDataUntil)
        predictionProgressCallback(0.10)
        val groupedExpensesByWeeks = groupFactDataByWeeks(factData, factDataSince, factDataUntil)
        predictionProgressCallback(0.20)
        val enumeratedDecimal: EnumeratedDistribution<BigDecimal> = createEnumeratedDistributionFromGroupedExpenses(groupedExpensesByWeeks)
        predictionProgressCallback(0.25)
        return runMonteCarloSimulationsWith(simulationNumber, factData, factDataUntil, predictUntil, enumeratedDecimal) { subTaskProgress ->
            predictionProgressCallback(0.25 + 0.75*subTaskProgress)
        }
    }

    private fun runMonteCarloSimulationsWith(
        simulationNumber: Int,
        factData: NetWorthDescriptor,
        factDataEnd: LocalDate,
        toDate: LocalDate,
        enumeratedDecimal: EnumeratedDistribution<BigDecimal>,
        predictionProgressCallback: (Double) -> Unit = {}
    ): MonteCarloSimulationResultDescriptor {
        val predictedWeeks = ChronoUnit.WEEKS.between(factDataEnd, toDate).toInt()
        val predictionsFinished = AtomicInteger(0)
        val simulationList: List<SingleMonteCarloSimulationResult> = IntStream
            .range(0, simulationNumber)
            .parallel()
            .mapToObj {
                val result = runASingleMonteCarloSimulation(factData, predictedWeeks, enumeratedDecimal, factDataEnd)
                predictionProgressCallback(0.9 / simulationNumber * predictionsFinished.incrementAndGet())
                result
            }
            .asSequence()
            .sortedBy { it.predictedValueByDate.maxBy { it.key }.value }
            .toList()
        predictionProgressCallback(1.0)
        return MonteCarloSimulationResultDescriptor(
            simulationList,
            factData.netWorthByDate
        )
    }

    private fun runASingleMonteCarloSimulation(
        factData: NetWorthDescriptor,
        predictedWeeks: Int,
        enumeratedDecimal: EnumeratedDistribution<BigDecimal>,
        factDataEnd: LocalDate
    ): SingleMonteCarloSimulationResult {
        val predictedValueByWeek: MutableMap<LocalDate, BigDecimal> = mutableMapOf()
        var lastKnownNetWorth = factData.netWorthByDate.maxBy { it.key }.value
        IntStream
            .range(0, predictedWeeks)
            .parallel()
            .mapToObj { idx ->
                if (idx == 0) {
                    0 to BigDecimal.ZERO
                } else {
                    idx to enumeratedDecimal.sample()
                }
            }
            .sorted { o1, o2 -> o1.first.compareTo(o2.first) }
            .sequential()
            .forEach { (weekSinceFactDataEnd, transactionSumForWeek) ->
                val netWorthByWeek = lastKnownNetWorth + transactionSumForWeek
                predictedValueByWeek[factDataEnd.plusWeeks(weekSinceFactDataEnd.toLong())] = netWorthByWeek
                lastKnownNetWorth = netWorthByWeek
            }
        logger.info("Single Monte Carlo sim run successfully")
        return SingleMonteCarloSimulationResult(predictedValueByWeek)
    }

    private fun createEnumeratedDistributionFromGroupedExpenses(transactions: Map<LocalDate, BigDecimal>): EnumeratedDistribution<BigDecimal> {
        val start = transactions.keys.min()
        val end = transactions.keys.max()
        val interval = end.toEpochDay() - start.toEpochDay()
        val inflectionPoint = interval * 0.6
        val elements = transactions.map { (date, value) ->
            val age = date.toEpochDay() - start.toEpochDay()
            val weight = 1.0 / (1 + exp(-1.0 * (age - inflectionPoint)))
            ApachePair(value, weight)
        }
        return EnumeratedDistribution(elements)
    }

    private fun groupFactDataByWeeks(factData: NetWorthDescriptor, fromDate: LocalDate, factDataEnd: LocalDate): Map<LocalDate, BigDecimal> {
        val retMap: MutableMap<LocalDate, BigDecimal> = mutableMapOf()
        var loopDate = atStartOfWeek(fromDate)
        while (loopDate <= factDataEnd) {
            val oneWeekLater = loopDate.plusWeeks(1)
            val netWorthByStartLoopWeek = factData.netWorthByDate[loopDate] ?: BigDecimal.ZERO
            val netWorthByNextWeek = factData.netWorthByDate[oneWeekLater] ?: netWorthByStartLoopWeek
            retMap[loopDate] = (netWorthByNextWeek - netWorthByStartLoopWeek)
            loopDate = oneWeekLater
        }
        return retMap
    }

    private fun atStartOfWeek(fromDate: LocalDate): LocalDate = fromDate.minusDays(fromDate.dayOfWeek.value.toLong() - 1)
}