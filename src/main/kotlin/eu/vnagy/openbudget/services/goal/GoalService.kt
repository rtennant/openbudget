package eu.vnagy.openbudget.services.goal

import eu.vnagy.openbudget.model.dao.BudgetForMonthDao
import eu.vnagy.openbudget.model.dao.GoalDao
import eu.vnagy.openbudget.model.dao.TransactionDao
import eu.vnagy.openbudget.model.entites.Goal
import eu.vnagy.openbudget.model.entites.GoalType
import eu.vnagy.openbudget.services.goal.poko.GoalStatistics
import eu.vnagy.openbudget.services.min
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.math.BigInteger
import java.math.RoundingMode
import java.time.YearMonth
import java.time.temporal.ChronoUnit

@Service
class GoalService(
    private val goalDao: GoalDao,
    private val transactionDao: TransactionDao,
    private val budgetForMonthDao: BudgetForMonthDao,
) {

    fun calculateGoalStatistics(goal: Goal, yearMonth: YearMonth): GoalStatistics {
        val totalMonthsForGoal = ChronoUnit.MONTHS.between(goal.startMonthFirstDay, goal.endMonthLastDay.plusDays(1)).toBigInteger()
        val totalMonthsForGoalBigDecimal = totalMonthsForGoal.toBigDecimal()
        val totalGoalAmount = when (goal.type) {
            GoalType.MONTHLY -> goal.goalAmount * totalMonthsForGoalBigDecimal
            GoalType.TOTAL -> goal.goalAmount
        }
        val goalMonthlyAmount = when (goal.type) {
            GoalType.MONTHLY -> goal.goalAmount
            GoalType.TOTAL -> goal.goalAmount.divide(totalMonthsForGoalBigDecimal, 10, RoundingMode.HALF_DOWN)
        }
        val goalEndOrCurrentMonth = min(goal.endMonthLastDay, yearMonth.atEndOfMonth())
        val goalStartOrCurrentMonth = min(goal.startMonthFirstDay, yearMonth.atDay(1))
        val durationInMonths = (ChronoUnit.MONTHS.between(goal.startMonthFirstDay, goal.endMonthLastDay) + 1).toBigInteger()
        val passedMonths = (ChronoUnit.MONTHS.between(goal.startMonthFirstDay, goalEndOrCurrentMonth) + 1).toBigInteger()
        val alreadySpentValue = transactionDao.sumSpentAndIncomeByCategoryBetween(
            goal.category,
            goalStartOrCurrentMonth,
            goalEndOrCurrentMonth
        ).negate()
        val budgetedForGoalIncludingCurrentMonth = goalDao.sumBudgetedAmountForGoal(goal, yearMonth)
        return GoalStatistics(
            goal = goal,
            statsUntilYearMonth = yearMonth,
            goalMonthlyAmount = goalMonthlyAmount,
            goalTotalAmount = totalGoalAmount,
            durationInMonthsIncludingFirstAndLast = durationInMonths,
            alreadyPassedMonthsIncludingCurrentOne = passedMonths,
            monthsLeftOverExcludingCurrentOne = durationInMonths - passedMonths,
            budgetedForGoalIncludingCurrentMonth = budgetedForGoalIncludingCurrentMonth,
            spentForGoalIncludingCurrentMonth = alreadySpentValue,
            shouldBudgetOnceTheFollowingToBeOnTrackThisMonth = calculateValueToBudgetThisMonthToBeOnTrack(
                totalGoalAmount,
                totalMonthsForGoalBigDecimal,
                passedMonths.toBigDecimal(),
                budgetedForGoalIncludingCurrentMonth,
                yearMonth,
                goal
            ),
            shouldBudgetAlwaysTheFollowingToBeOnTrackFromNowOn = calculateValueToBudgetFromNowOnToBeOnTrack(
                totalGoalAmount,
                totalMonthsForGoal,
                passedMonths,
                budgetedForGoalIncludingCurrentMonth,
                yearMonth,
                goal
            ),
        )
    }

    private fun calculateValueToBudgetThisMonthToBeOnTrack(
        totalGoalAmount: BigDecimal,
        totalMonthsForGoal: BigDecimal,
        passedMonths: BigDecimal,
        budgetedForGoalIncludingCurrentMonth: BigDecimal,
        yearMonth: YearMonth,
        goal: Goal,
    ): BigDecimal {
        val expectedAmountToBeBudgeted = (totalGoalAmount.setScale(10) / totalMonthsForGoal) * passedMonths
        val budgetedLastMonth = budgetForMonthDao.getByCategoryAndMonth(goal.category, yearMonth)?.budgetedMoney ?: BigDecimal.ZERO
        return expectedAmountToBeBudgeted - (budgetedForGoalIncludingCurrentMonth - budgetedLastMonth)
    }

    private fun calculateValueToBudgetFromNowOnToBeOnTrack(
        totalGoalAmount: BigDecimal,
        totalMonthsForGoal: BigInteger,
        passedMonths: BigInteger,
        budgetedForGoalIncludingCurrentMonth: BigDecimal,
        yearMonth: YearMonth,
        goal: Goal,
    ): BigDecimal {
        val budgetedLastMonth = budgetForMonthDao.getByCategoryAndMonth(goal.category, yearMonth)?.budgetedMoney ?: BigDecimal.ZERO
        val budgetedExcludingCurrentMonth = budgetedForGoalIncludingCurrentMonth - budgetedLastMonth
        val monthsLeftOver = totalMonthsForGoal - passedMonths + BigInteger.ONE
        return (totalGoalAmount - budgetedExcludingCurrentMonth).divide(monthsLeftOver.toBigDecimal(), 10, RoundingMode.HALF_UP)
    }
}
