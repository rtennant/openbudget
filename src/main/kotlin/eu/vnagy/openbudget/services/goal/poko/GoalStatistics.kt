package eu.vnagy.openbudget.services.goal.poko

import eu.vnagy.openbudget.model.entites.Goal
import java.math.BigDecimal
import java.math.BigInteger
import java.time.YearMonth

data class GoalStatistics(
    val goal: Goal,
    val statsUntilYearMonth: YearMonth,
    val goalMonthlyAmount: BigDecimal,
    val goalTotalAmount: BigDecimal,
    val durationInMonthsIncludingFirstAndLast: BigInteger,
    val alreadyPassedMonthsIncludingCurrentOne: BigInteger,
    val monthsLeftOverExcludingCurrentOne: BigInteger,
    val budgetedForGoalIncludingCurrentMonth: BigDecimal,
    val spentForGoalIncludingCurrentMonth: BigDecimal,
    val shouldBudgetOnceTheFollowingToBeOnTrackThisMonth: BigDecimal,
    val shouldBudgetAlwaysTheFollowingToBeOnTrackFromNowOn: BigDecimal,
)
