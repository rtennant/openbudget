package eu.vnagy.openbudget.services

fun <T: Comparable<T>> min(t1: T, t2: T): T {
    if (t1 < t2) {
        return t1
    } else {
        return t2
    }
}
