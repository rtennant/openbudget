package eu.vnagy.openbudget.frontend.budget

import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery
import eu.vnagy.openbudget.model.dao.CategoryDao
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Category
import java.util.stream.Stream

class CategoryTreeGridDataProvider(
    private val budget: Budget,
    private val categoryDao: CategoryDao
): AbstractBackEndHierarchicalDataProvider<Category, Unit>() {

    override fun getChildCount(query: HierarchicalQuery<Category, Unit>): Int {
        val parentCategory = query.parent
        return categoryDao.countCategoriesWithParentCategoryWithoutInternal(budget, parentCategory)
    }

    override fun hasChildren(item: Category): Boolean {
        return categoryDao.countCategoriesWithParentCategoryWithoutInternal(budget, item) > 0
    }

    override fun fetchChildrenFromBackEnd(query: HierarchicalQuery<Category, Unit>): Stream<Category> {
        val parentCategory = query.parent
        return categoryDao.findCategoriesWithParentCategoryWithoutInternal(budget, parentCategory)
    }

}
