package eu.vnagy.openbudget.frontend.budget

import com.github.mvysny.karibudsl.v10.*
import com.github.mvysny.kaributools.expandAll
import com.github.mvysny.kaributools.refresh
import com.vaadin.flow.component.Component
import com.vaadin.flow.component.Text
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.grid.ColumnTextAlign
import com.vaadin.flow.component.grid.Grid.Column
import com.vaadin.flow.component.grid.GridVariant
import com.vaadin.flow.component.grid.dnd.GridDropEvent
import com.vaadin.flow.component.grid.dnd.GridDropLocation
import com.vaadin.flow.component.grid.dnd.GridDropMode
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.FlexLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.component.progressbar.ProgressBar
import com.vaadin.flow.component.progressbar.ProgressBarVariant
import com.vaadin.flow.component.tabs.Tab
import com.vaadin.flow.component.tabs.Tabs
import com.vaadin.flow.component.tabs.TabsVariant
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.component.textfield.TextFieldVariant
import com.vaadin.flow.component.treegrid.TreeGrid
import com.vaadin.flow.data.renderer.LitRenderer
import com.vaadin.flow.data.renderer.Renderer
import com.vaadin.flow.router.*
import com.vaadin.flow.shared.Registration
import com.vaadin.starter.business.ui.components.ListItem
import com.vaadin.starter.business.ui.components.detailsdrawer.DetailsDrawer
import com.vaadin.starter.business.ui.components.detailsdrawer.DetailsDrawerHeader
import com.vaadin.starter.business.ui.util.UIUtils
import com.vaadin.starter.business.ui.util.css.WhiteSpace
import com.vaadin.starter.business.ui.views.SplitViewFrame
import elemental.json.impl.JreJsonString
import eu.vnagy.openbudget.frontend.base.HasBudget
import eu.vnagy.openbudget.frontend.base.KtMainLayout
import eu.vnagy.openbudget.frontend.budget.BudgetView.Companion.ROUTE_NAME
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.frontend.vaadin.getCurrencySymbol
import eu.vnagy.openbudget.model.dao.*
import eu.vnagy.openbudget.model.entites.*
import eu.vnagy.openbudget.services.goal.GoalService
import eu.vnagy.openbudget.services.goal.poko.GoalStatistics
import org.springframework.beans.factory.annotation.Autowired
import java.math.BigDecimal
import java.time.LocalDate
import java.time.YearMonth
import java.util.*
import javax.annotation.security.PermitAll
import kotlin.math.roundToInt


@PermitAll
@Route(ROUTE_NAME, layout = KtMainLayout::class)
class BudgetView @Autowired constructor(
    private val budgetDao: BudgetDao,
    private val goalDao: GoalDao,
    private val budgetForMonthDao: BudgetForMonthDao,
    private val categoryDao: CategoryDao,
    private val transactionDao: TransactionDao,
    private val numberFormatServices: NumberFormatServices,
    private val goalService: GoalService,
) : SplitViewFrame(), HasBudget, RouterLayout, BeforeEnterObserver {

    companion object {
        const val PARAMETER_BUDGET_ID = "budgetId"
        const val PARAMETER_YEAR_AND_MONTH = "yearAndMonth"
        const val ROUTE_NAME = "budget/:$PARAMETER_BUDGET_ID/:$PARAMETER_YEAR_AND_MONTH"

        fun navigateHere(budget: Budget, yearMonth: YearMonth) {
            UI.getCurrent().navigate(
                BudgetView::class.java, RouteParameters(
                    RouteParam(PARAMETER_BUDGET_ID, budget.id.toString()), RouteParam(PARAMETER_YEAR_AND_MONTH, yearMonth.toString())
                )
            )
        }
    }

    override lateinit var budget: Budget
        private set

    private lateinit var yearMonth: YearMonth

    private var focusedItem: Category? = null

    private var grid: TreeGrid<Category>? = null
    private var budgetedInMonthTextFieldColumn: Column<*>? = null
    private var listItemForBudgetedAmountInDetails: ListItem? = null
    private var availableToBudgetLayout: VerticalLayout? = null

    private var resizeListener: Registration? = null
    private var mobileLayout: Boolean = false

    private var detailsDrawer: DetailsDrawer? = null
    private var detailsDetailsTab = Tab("Details")
    private var detailsProgressTab = Tab("Progress")
    private var detailsGoalsTab = Tab("Goals")
    private var tabs = Tabs(detailsDetailsTab, detailsProgressTab, detailsGoalsTab)

    override fun beforeEnter(event: BeforeEnterEvent) {
        val urlParameters: RouteParameters = event.routeParameters
        val budgetId = urlParameters.get(PARAMETER_BUDGET_ID).orElse(null)?.let { UUID.fromString(it) } ?: throw NotFoundException()
        this.budget = budgetDao.findById(budgetId) ?: throw NotFoundException()
        this.yearMonth = YearMonth.parse(urlParameters.get(PARAMETER_YEAR_AND_MONTH).orElseThrow())
        setViewContent(VerticalLayout().createSecondaryView())
        setViewDetails(createDetailsDrawer())


        if (resizeListener == null) {
            resizeListener = event.ui.page.addBrowserWindowResizeListener { resizeEvent ->
                updateMobileLayout(resizeEvent.width)
            }
        }

        event.ui.page.retrieveExtendedClientDetails { receiver ->
            updateMobileLayout(receiver.screenWidth)
        }
    }

    private fun updateMobileLayout(width: Int) {
        mobileLayout = width < 650
        budgetedInMonthTextFieldColumn?.isVisible = !mobileLayout
        listItemForBudgetedAmountInDetails?.isVisible = mobileLayout
    }


    private fun createDetailsDrawer(): DetailsDrawer {
        val detailsDrawer = DetailsDrawer(DetailsDrawer.Position.RIGHT)
        this.detailsDrawer = detailsDrawer
        tabs.addThemeVariants(TabsVariant.LUMO_EQUAL_WIDTH_TABS)
        tabs.addSelectedChangeListener { configureTabs() }
        val detailsDrawerHeader = DetailsDrawerHeader("Category Details", tabs)
        detailsDrawerHeader.addCloseListener {
            this.grid?.select(null)
            this.detailsDrawer?.hide()
        }
        detailsDrawer.setHeader(detailsDrawerHeader)
        return detailsDrawer
    }

    private fun configureTabs() {
        val selectedTab = tabs.selectedTab
        val selectedCategory = grid?.selectionModel?.firstSelectedItem?.orElse(null)
        val detailsDrawer = this.detailsDrawer
        if (selectedCategory?.parentCategory != null && detailsDrawer != null) {
            when (selectedTab) {
                detailsDetailsTab -> {
                    detailsDrawer.setContent(createCategoryDetailsTab(selectedCategory))
                }

                detailsProgressTab -> {
                    detailsDrawer.setContent(createCategoryProgressTab(selectedCategory))
                }

                detailsGoalsTab -> {
                    detailsDrawer.setContent(createCategoryGoalsTab(selectedCategory))
                }

                else -> TODO()
            }
            detailsDrawer.show()
        } else {
            detailsDrawer?.hide()
        }
    }

    private fun VerticalLayout.createSecondaryView(): VerticalLayout {
        flexLayout {
            width = "100%"
            setFlexDirection(FlexLayout.FlexDirection.ROW)
            alignItems = FlexComponent.Alignment.END
            verticalLayout {
                h2(budget.name)
            }
            availableToBudgetLayout = verticalLayout()
            renderAvailableToBudget()
            setFlexGrow(1.0, availableToBudgetLayout)
        }
        horizontalLayout {
            setWidthFull()
            verticalLayout {
                alignSelf = FlexComponent.Alignment.START
                horizontalLayout {
                    setWidthFull()
                    horizontalLayout {
                        isPadding = false
                        isMargin = false
                        flexGrow = 1.0
                        button("<<") {
                            onLeftClick {
                                navigateHere(budget, yearMonth.minusMonths(1))
                            }
                            addThemeVariants(ButtonVariant.LUMO_SMALL)
                        }
                        h5(yearMonth.toString())
                        button(">>") {
                            onLeftClick {
                                navigateHere(budget, yearMonth.plusMonths(1))
                            }
                            addThemeVariants(ButtonVariant.LUMO_SMALL)
                        }
                    }
                    button("New category") {
                        addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL)
                        onLeftClick {
                            NewCategoryDialog(budget, categoryDao) {
                                grid?.refresh()
                            }.open()
                        }
                    }
                }
                grid = treeGrid(
                    CategoryTreeGridDataProvider(
                        budget, categoryDao
                    )
                ) {
                    isAllRowsVisible = true
                    addThemeVariants(GridVariant.LUMO_COMPACT)
                    var draggedItem: Category? = null
                    isRowsDraggable = false // TODO
                    setDropFilter { category: Category? ->
                        return@setDropFilter category?.parentCategory == draggedItem?.parentCategory
                    }
                    addDragStartListener { event ->
                        draggedItem = event.draggedItems[0]
                        dropMode = GridDropMode.BETWEEN
                    }
                    addDragEndListener {
                        draggedItem = null
                        dropMode = null
                    }
                    addDropListener { event -> afterDropHappened(event, draggedItem) }
                    addHierarchyColumn(Category::name).apply {
                        setHeader("Category")
                        this@apply.width = "175px"
                    }
                    budgetedInMonthTextFieldColumn = addColumn(budgetedInMonthTextFieldRenderer()).apply {
                        setHeader("Budgeted")
                        this@apply.width = "100px"
                    }
                    addColumn(::getActivityForBudgetInMonth).apply {
                        setHeader("Activity")
                        textAlign = ColumnTextAlign.END
                        this@apply.width = "100px"
                    }
                    addColumn(::availableToSpendInMonth).apply {
                        setHeader("Available")
                        textAlign = ColumnTextAlign.END
                        this@apply.width = "100px"
                    }
                    addSelectionListener {
                        configureTabs()
                    }
                    expandAll()
                    addCellFocusListener { event ->
                        focusedItem = event.item.orElse(null)
                    }
                    gridContextMenu {
                        item("Hide category", clickListener = { category ->
                            if (category != null) {
                                category.hidden = true
                                categoryDao.save(category)
                                this@treeGrid.refresh()
                            }
                        })
                    }
                }
            }
        }
        return this
    }

    private fun renderAvailableToBudget() {
        availableToBudgetLayout?.apply {
            removeAll()
            label("Available to budget:")
            text(
                numberFormatServices.formatBigDecimalAsUnparseableText(
                    budgetForMonthDao.getAllFreeMoneyAtMonth(budget, yearMonth),
                    budget
                )
            )
            alignItems = FlexComponent.Alignment.END
            style["gap"] = "0"
        }
    }

    private fun budgetedInMonthTextFieldRenderer(): Renderer<Category> {
        val currencySymbol = getCurrencySymbol(null, budget)
        return LitRenderer.of<Category>(
            """
                <vaadin-text-field 
                        style='display: ${"$"}{item.display}; width: calc(min(150px, 100%));' 
                        theme="align-right small" 
                        @change="${"$"}{e => valueChanged(e.target.value)}" 
                        @focus="${"$"}{e => elementFocused()}" 
                        value='${"$"}{item.value}'>
                    <div slot="suffix">${"$"}{item.currencySymbol}</div>
                </vaadin-text-field>
                """.trimIndent()
        ).withProperty("value") { category ->
            numberFormatServices.formatBigDecimalForInput(
                budgetForMonthDao.findBudgetedForCategoryAndMonth(category, yearMonth)?.budgetedMoney,
                budget
            )
        }.withProperty("currencySymbol") {
            currencySymbol
        }.withProperty("display") { category ->
            if (category.parentCategory != null) {
                "inline-flex"
            } else {
                "none"
            }
        }.withFunction("valueChanged") { category, arguments ->
            val value = arguments.get<JreJsonString>(0).string
            updateBudgetedValueForCategory(value, category)
        }.withFunction("elementFocused") { category, arguments ->
            if (grid?.selectedItems?.firstOrNull() != null) {
                grid?.select(category)
            }
        }
    }

    private fun updateBudgetedValueForCategory(value: String?, category: Category) {
        val asBigDecimal = numberFormatServices.parseBigDecimalFromInput(value, budget)
        var budgetForMonth = budgetForMonthDao.findBudgetedForCategoryAndMonth(category, yearMonth)
        if (budgetForMonth == null) {
            budgetForMonth = BudgetForMonth(budget = budget, category = category, firstDayOfMonth = yearMonth.atDay(1))
        }
        budgetForMonth.budgetedMoney = asBigDecimal
        budgetForMonthDao.save(budgetForMonth)
        refreshBudgetView()
    }

    private fun refreshBudgetView() {
        val localFocusedItem = focusedItem
        if (localFocusedItem != null) {
            grid?.dataProvider?.refreshItem(localFocusedItem)
        } else {
            // TODO???
        }
        renderAvailableToBudget()
    }

    private fun createCategoryGoalsTab(category: Category): Component {
        val goals = goalDao.findActiveGoalsForCategory(category, yearMonth)
        return Div().apply {
            verticalLayout {
                style["padding"] = "var(--lumo-space-wide-r-l)"
                button("New goal") {
                    addThemeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_PRIMARY)
                    alignSelf = FlexComponent.Alignment.END
                    onLeftClick {
                        GoalEditorDialog(
                            goal = Goal(category = category),
                            goalDao = goalDao,
                            numberFormatServices = numberFormatServices,
                            budget = budget,
                            onCloseCallback = { configureTabs() }
                        ).open()
                    }
                }
            }
            goals.forEach { goal ->
                horizontalLayout {
                    style["padding"] = "var(--lumo-space-wide-r-l)"
                    strong(goal.name) {
                        flexGrow = 1.0
                    }
                    button("Edit goal") {
                        addThemeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_SUCCESS)
                        onLeftClick {
                            GoalEditorDialog(
                                goal = goal,
                                goalDao = goalDao,
                                budget = budget,
                                numberFormatServices = numberFormatServices,
                            ).open()
                        }
                    }
                }
                val goalStatistics: GoalStatistics = goalService.calculateGoalStatistics(goal, yearMonth)
                val goalText = when (goal.type) {
                    GoalType.MONTHLY -> {
                        val amount = numberFormatServices.formatBigDecimalAsUnparseableText(goalStatistics.goalMonthlyAmount, budget)
                        "$amount every month until ${YearMonth.from(goal.endMonthLastDay)}"
                    }

                    GoalType.TOTAL -> {
                        val amount = numberFormatServices.formatBigDecimalAsUnparseableText(goalStatistics.goalTotalAmount, budget)
                        "Total amount of $amount until ${YearMonth.from(goal.endMonthLastDay)}"
                    }
                }

                init(
                    ListItem(
                        UIUtils.createSecondaryIcon(VaadinIcon.CHART),
                        goalText,
                        "Goal"
                    )
                ) {
                    setReverse(true)
                    setWhiteSpace(WhiteSpace.PRE_LINE)
                }

                val plannedProgress = ProgressBar(
                    0.0,
                    goalStatistics.goalTotalAmount.toDouble(),
                    goalStatistics.budgetedForGoalIncludingCurrentMonth.toDouble()
                )
                val timeProgress = ProgressBar(
                    0.0,
                    goalStatistics.durationInMonthsIncludingFirstAndLast.toDouble(),
                    goalStatistics.alreadyPassedMonthsIncludingCurrentOne.toDouble()
                )

                init(
                    ListItem(
                        UIUtils.createSecondaryIcon(VaadinIcon.PROGRESSBAR),
                        plannedProgress,
                        "Planned goal progress"
                    )
                ) {
                    setReverse(true)
                    setWhiteSpace(WhiteSpace.PRE_LINE)
                }
                init(
                    ListItem(
                        UIUtils.createSecondaryIcon(VaadinIcon.PROGRESSBAR),
                        timeProgress,
                        "Time progress"
                    )
                ) {
                    setReverse(true)
                    setWhiteSpace(WhiteSpace.PRE_LINE)
                }
                ul {
                    style["font-size"] = "0.8em"
                    li {
                        style["margin-bottom"] = "5px"
                        strong("Total goal: ")
                        text(numberFormatServices.formatBigDecimalAsUnparseableText(goalStatistics.goalTotalAmount, budget))
                    }
                    li {
                        style["margin-bottom"] = "5px"
                        strong("Total budgeted yet: ")
                        text(numberFormatServices.formatBigDecimalAsUnparseableText(goalStatistics.budgetedForGoalIncludingCurrentMonth, budget))
                    }
                    li {
                        style["margin-bottom"] = "5px"
                        strong("Spent until today: ")
                        text(numberFormatServices.formatBigDecimalAsUnparseableText(goalStatistics.spentForGoalIncludingCurrentMonth, budget))
                    }
                    li {
                        style["margin-bottom"] = "5px"
                        strong("Goal%: ")
                        text("${(plannedProgress.percentValue * 10000).roundToInt() / 100.0}%")
                    }
                    li {
                        style["margin-bottom"] = "5px"
                        strong("Time%: ")
                        text("${(timeProgress.percentValue * 10000).roundToInt() / 100.0}%")
                    }
                    li {
                        style["margin-bottom"] = "5px"
                        strong("Budget to catch up this month: ")
                        text(numberFormatServices.formatBigDecimalAsUnparseableText(goalStatistics.shouldBudgetOnceTheFollowingToBeOnTrackThisMonth, budget))
                    }
                    li {
                        style["margin-bottom"] = "5px"
                        strong("Budget every month to catch up by the deadline: ")
                        text(numberFormatServices.formatBigDecimalAsUnparseableText(goalStatistics.shouldBudgetAlwaysTheFollowingToBeOnTrackFromNowOn, budget))
                    }
                }
                hr()
            }
        }
    }

    private fun createCategoryProgressTab(category: Category): Component {
        val spentInThisMonth = transactionDao.sumSpentAndIncomeByCategoryInMonth(category, yearMonth).negate()
        val availableForTheRest = budgetForMonthDao.calcAvailableMoneyUntilMonthInCategory(category, yearMonth) ?: BigDecimal.ZERO
        val availableForThisMonthAtFirstDay = spentInThisMonth + availableForTheRest
        val (budgetProgressMin, budgetProgressMax, budgetProgressValue) =
            if (availableForThisMonthAtFirstDay > BigDecimal.ZERO && spentInThisMonth > BigDecimal.ZERO && spentInThisMonth < availableForThisMonthAtFirstDay) {
                Triple(0.0, availableForThisMonthAtFirstDay.toDouble(), spentInThisMonth.toDouble())
            } else if (spentInThisMonth > BigDecimal.ZERO) {
                Triple(0.0, 1.0, 1.0)
            } else {
                Triple(0.0, 1.0, 0.0)
            }

        val (monthProgressMin, monthProgressMax, monthProgressValue) = if (yearMonth != YearMonth.now()) {
            Triple(0.0, 1.0, 1.0)
        } else {
            val daysOfMonth = YearMonth.now().atEndOfMonth().dayOfMonth
            val currentDay = LocalDate.now().dayOfMonth
            Triple(1.0, daysOfMonth.toDouble(), currentDay.toDouble())
        }

        val budgetProgressBar = ProgressBar(budgetProgressMin, budgetProgressMax, budgetProgressValue)
        val monthProgressBar = ProgressBar(monthProgressMin, monthProgressMax, monthProgressValue)
        if ((monthProgressBar.percentValue) <= (budgetProgressBar.percentValue)) {
            // overspent
            budgetProgressBar.addThemeVariants(ProgressBarVariant.LUMO_ERROR)
        } else {
            budgetProgressBar.addThemeVariants(ProgressBarVariant.LUMO_SUCCESS)
        }
        val items = listOf(
            ListItem(
                UIUtils.createSecondaryIcon(VaadinIcon.MONEY_WITHDRAW),
                budgetProgressBar,
                "Progress of Budgeted money"
            ),
            ListItem(
                UIUtils.createSecondaryIcon(VaadinIcon.CALENDAR),
                monthProgressBar,
                "Progress of month"
            ),
            ListItem(
                UIUtils.createSecondaryIcon(VaadinIcon.WALLET),
                numberFormatServices.formatBigDecimalAsUnparseableText(availableForThisMonthAtFirstDay, budget),
                "Total available for month"
            ),
            ListItem(
                UIUtils.createSecondaryIcon(VaadinIcon.MONEY_WITHDRAW),
                numberFormatServices.formatBigDecimalAsUnparseableText(spentInThisMonth, budget),
                "Total spent for month"
            ),
            ListItem(
                UIUtils.createSecondaryIcon(VaadinIcon.CALENDAR),
                "${(monthProgressBar.percentValue * 10000).roundToInt() / 100.0}%",
                "Month %"
            ),
            ListItem(
                UIUtils.createSecondaryIcon(VaadinIcon.MONEY_WITHDRAW),
                "${(budgetProgressBar.percentValue * 10000).roundToInt() / 100.0}%",
                "Spent %"
            )
        )
        return Div().apply {
            items.forEach {
                it.setReverse(true)
                it.setWhiteSpace(WhiteSpace.PRE_LINE)
                this@apply.add(it)
            }
        }
    }

    private fun createCategoryDetailsTab(category: Category): Component {
        val leftOver = budgetForMonthDao.calcAvailableMoneyUntilMonthInCategory(category, yearMonth.minusMonths(1)) ?: BigDecimal.ZERO
        val spentLastMonth = transactionDao.sumSpentAndIncomeByCategoryInMonth(category, yearMonth.minusMonths(1))
        val spentAYearAgo = transactionDao.sumSpentAndIncomeByCategoryInMonth(category, yearMonth.minusYears(1))
        val budgetedForCategoryTextField = TextField().apply {
            suffixComponent = Div(Text(budget.mainCurrency.currency))
            value = numberFormatServices.formatBigDecimalForInput(
                budgetForMonthDao.findBudgetedForCategoryAndMonth(category, yearMonth)?.budgetedMoney,
                budget
            )
            addThemeVariants(TextFieldVariant.LUMO_ALIGN_RIGHT, TextFieldVariant.LUMO_SMALL)
            addValueChangeListener { event ->
                updateBudgetedValueForCategory(event.value, category)
            }
            width = "calc(min(100%, 250px))"
        }
        val listItemForBudgetedAmountInDetails = ListItem(
            UIUtils.createSecondaryIcon(VaadinIcon.WALLET),
            budgetedForCategoryTextField,
            "Budgeted for Category"
        ).apply {
            isVisible = mobileLayout
        }
        this.listItemForBudgetedAmountInDetails = listItemForBudgetedAmountInDetails
        return Div().apply {
            val items = listOf(
                listItemForBudgetedAmountInDetails,
                ListItem(
                    UIUtils.createSecondaryIcon(VaadinIcon.WALLET),
                    numberFormatServices.formatBigDecimalAsUnparseableText(leftOver, budget),
                    "Leftover from previous month"
                ),
                ListItem(
                    UIUtils.createSecondaryIcon(VaadinIcon.MONEY_WITHDRAW),
                    numberFormatServices.formatBigDecimalAsUnparseableText(spentLastMonth, budget),
                    "Spent in previous month"
                ),
                ListItem(
                    UIUtils.createSecondaryIcon(VaadinIcon.MONEY_WITHDRAW),
                    numberFormatServices.formatBigDecimalAsUnparseableText(spentAYearAgo, budget),
                    "Spent in same month last year"
                )
            )
            items.forEach {
                it.setReverse(true)
                it.setWhiteSpace(WhiteSpace.PRE_LINE)
                this@apply.add(it)
            }
        }
    }

    private fun afterDropHappened(
        event: GridDropEvent<Category>, draggedItem: Category?
    ) {
        val targetCategory = event.dropTargetItem.orElse(null)
        val dropLocation = event.dropLocation

        val categoryWasDroppedOnItself = draggedItem == targetCategory

        if (targetCategory == null || categoryWasDroppedOnItself) {
            // should do nothing
        } else {
            val childCategories = targetCategory.parentCategory!!.childCategories.filter { !it.deleted }
            val indexOfItem = if (dropLocation == GridDropLocation.BELOW) {
                childCategories.indexOf(targetCategory) + 1
            } else {
                childCategories.indexOf(targetCategory)
            }
            draggedItem?.order = indexOfItem
            childCategories.subList(indexOfItem, childCategories.size).forEachIndexed { index, childCategory -> childCategory.order = index + indexOfItem }
            categoryDao.saveAll(childCategories)
            grid?.refresh()
        }
    }

    private fun availableToSpendInMonth(category: Category): String {
        if (category.parentCategory != null) {
            val leftoverFromPreviousMonth = budgetForMonthDao.calcAvailableMoneyUntilMonthInCategory(category, yearMonth) ?: BigDecimal.ZERO
            return numberFormatServices.formatBigDecimalAsUnparseableText(leftoverFromPreviousMonth, budget)
        }
        return ""
    }

    private fun getActivityForBudgetInMonth(category: Category): String {
        return if (category.parentCategory != null) {
            numberFormatServices.formatBigDecimalAsUnparseableText(transactionDao.sumSpentAndIncomeByCategoryInMonth(category, yearMonth), budget)
        } else {
            ""
        }
    }

    private val ProgressBar?.percentValue: Double
        get() {
            if (this == null) {
                return 0.0
            } else {
                return (value - min) / (max - min)
            }
        }

}
