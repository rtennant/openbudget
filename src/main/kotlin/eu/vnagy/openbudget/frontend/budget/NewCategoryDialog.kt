package eu.vnagy.openbudget.frontend.budget

import com.github.mvysny.karibudsl.v10.*
import com.github.mvysny.karibudsl.v23.footer
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.formlayout.FormLayout
import eu.vnagy.openbudget.model.dao.CategoryDao
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Category

class NewCategoryDialog(
    private val budget: Budget,
    private val categoryDao: CategoryDao,
    private val onSaveCallback: () -> Unit = {},
) : Dialog() {

    private val binder = beanValidationBinder<Category>()

    init {
        headerTitle = "New transaction"
        width = "700px"
        height = "850px"
        isResizable = true
        formLayout {
            setSizeFull()
            setResponsiveSteps(
                FormLayout.ResponsiveStep("0", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP),
                FormLayout.ResponsiveStep("600px", 1, FormLayout.ResponsiveStep.LabelsPosition.ASIDE)
            )
            formItem("Name") {
                width = "100%"
                textField {
                    tabIndex = 1
                    width = "80%"
                    value = ""
                    isRequired = true
                    bind(binder).bind(Category::name)
                }
            }
            formItem("Parent category") {
                width = "100%"
                comboBox<Category> {
                    isRequired = false
                    width = "80%"
                    tabIndex = 2
                    setItems(categoryDao.findTopLevelNotHiddenCategoriesByBudget(budget)) // TODO this could be a lazy loading stuff.
                    setItemLabelGenerator { category -> category?.name }
                    bind(binder).bind(Category::parentCategory)
                }
            }
        }
        footer {
            button("Cancel") {
                onLeftClick { close() }
            }
            button("Save") {
                addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                onLeftClick {
                    val category = Category(budget = budget)
                    binder.writeBean(category)
                    categoryDao.save(category)
                    onSaveCallback()
                    close()
                }
            }
        }
    }
}
