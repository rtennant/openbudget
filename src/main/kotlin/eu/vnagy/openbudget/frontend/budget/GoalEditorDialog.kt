package eu.vnagy.openbudget.frontend.budget

import com.github.mvysny.karibudsl.v10.*
import com.vaadin.flow.data.binder.Binder
import eu.vnagy.openbudget.frontend.base.BaseEditorDialog
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.frontend.withDefaultModelValue
import eu.vnagy.openbudget.model.dao.GoalDao
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Goal
import eu.vnagy.openbudget.model.entites.GoalType
import java.math.BigDecimal
import java.time.YearMonth
import java.util.*

class GoalEditorDialog(
    goal: Goal,
    goalDao: GoalDao,
    numberFormatServices: NumberFormatServices,
    budget: Budget,
    onCloseCallback: () -> Unit = {}
) : BaseEditorDialog<UUID, Goal, GoalDao, Pair<NumberFormatServices, Budget>>(goal, goalDao, Pair(numberFormatServices, budget), onCloseCallback) {

    override fun onEnter(binder: Binder<Goal>, onEnterElements: Pair<NumberFormatServices, Budget>) {
        super.onEnter(binder, onEnterElements)
        val (numberFormatServices, budget) = onEnterElements
        formLayout {
            setSizeFull()
            formItem("Goal's name") {
                textField {
                    isRequired = true
                    bind(binder).bind(Goal::name)
                }
            }
            formItem("First day of goal") {
                datePicker {
                    bind(binder).bind(Goal::startMonthFirstDay)
                    isRequired = true
                    addValueChangeListener { event ->
                        if (event.value.dayOfMonth != 1) {
                            this@datePicker.value = event.value.withDayOfMonth(1)
                        }
                    }
                }
            }
            formItem("Last day of goal") {
                datePicker {
                    bind(binder).bind(Goal::endMonthLastDay)
                    isRequired = true
                    addValueChangeListener { event ->
                        if (event.value != YearMonth.from(event.value).atEndOfMonth()) {
                            this@datePicker.value = YearMonth.from(event.value).atEndOfMonth()
                        }
                    }
                }
            }
            formItem("Goal type") {
                comboBox<GoalType> {
                    bind(binder).bind(Goal::type)
                    isRequired = true
                    setItems(GoalType.values().toList())
                    setItemLabelGenerator { item -> item.name } // TODO
                }
            }
            formItem("Amount") {
                textField {
                    bind(binder)
                        .withConverter(numberFormatServices.createBigDecimalConverter(budget))
                        .withDefaultModelValue(BigDecimal.ZERO)
                        .bind(Goal::goalAmount)
                    isRequired = true
                    addValueChangeListener {e ->
                        numberFormatServices.formulaSensitiveTextFieldParser(e, budget)
                    }
                }
            }
        }
    }
}
