package eu.vnagy.openbudget.frontend.vaadin.bigdecimal

import java.math.BigDecimal
import kotlin.contracts.contract

fun BigDecimal?.hasNonZeroValue(): Boolean {
    contract {
        returns(false) implies (this@hasNonZeroValue != null)
    }
    return this != null && this.signum() != 0
}