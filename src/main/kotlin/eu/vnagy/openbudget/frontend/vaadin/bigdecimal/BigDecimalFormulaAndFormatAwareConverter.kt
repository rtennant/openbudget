package eu.vnagy.openbudget.frontend.vaadin.bigdecimal

import com.vaadin.flow.data.binder.Result
import com.vaadin.flow.data.binder.ValueContext
import com.vaadin.flow.data.converter.Converter
import eu.vnagy.openbudget.model.entites.Budget
import java.math.BigDecimal

class BigDecimalFormulaAndFormatAwareConverter(
    private val numberFormatServices: NumberFormatServices,
    private val budget: Budget
) : Converter<String, BigDecimal> {
    override fun convertToModel(value: String?, context: ValueContext?): Result<BigDecimal?> {
        if (value.isNullOrBlank()) {
            return Result.ok(null)
        } else {
            return Result.of(
                { numberFormatServices.parseBigDecimalFromInput(value, budget) },
                { exception -> "ERROR" }
            )
        }
    }

    override fun convertToPresentation(value: BigDecimal?, context: ValueContext?): String {
        return if (value.hasNonZeroValue()) {
            numberFormatServices.formatBigDecimalForInput(value, budget)
        } else {
            ""
        }
    }
}