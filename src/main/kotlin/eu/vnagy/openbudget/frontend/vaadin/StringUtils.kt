package eu.vnagy.openbudget.frontend.vaadin

import java.util.*

fun String.toUUID(): UUID? {
    return try {
        UUID.fromString(this)
    } catch (e: Exception) {
        return null
    }
}