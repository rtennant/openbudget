package eu.vnagy.openbudget.frontend.vaadin.bigdecimal

import com.vaadin.flow.component.AbstractField
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.converter.Converter
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Currency
import eu.vnagy.openbudget.model.entites.CurrencyFormat
import eu.vnagy.openbudget.model.entites.SymbolPosition
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import parser.MathExpression
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.regex.Pattern

@Service
class NumberFormatServices @Autowired constructor(

) {

    companion object {
        val DEFAULT_CURRENCY_FORMAT = CurrencyFormat(
            symbolPosition = SymbolPosition.POST,
            groupingSeparator = ',',
            decimalSeparator = '.'
        )
    }

    private fun createNumberFormatterForCurrencyAndFormat(budget: Budget): DecimalFormat {
        val currencyFormat = budget.currencyFormat ?: DEFAULT_CURRENCY_FORMAT
        return createNumberFormattedForCurrencyFormat(currencyFormat)
    }

    private fun createNumberFormattedForCurrencyFormat(currencyFormat: CurrencyFormat): DecimalFormat {
        val decimalFormatSymbols = DecimalFormatSymbols()
        decimalFormatSymbols.groupingSeparator = currencyFormat.groupingSeparator
        decimalFormatSymbols.decimalSeparator = currencyFormat.decimalSeparator
        val numberFormat = DecimalFormat("#,###.##", decimalFormatSymbols)
        numberFormat.isParseBigDecimal = true
        return numberFormat
    }

    fun formatBigDecimalAsUnparseableText(value: BigDecimal, budget: Budget): String {
        val currencyFormat = budget.currencyFormat ?: DEFAULT_CURRENCY_FORMAT
        val currency = budget.mainCurrency
        return formatNumber(value, currency, currencyFormat)
    }

    fun formatBigDecimalForInput(value: BigDecimal?, budget: Budget): String {
        val nonNullValue = value ?: BigDecimal.ZERO
        val scaledNonNullValue = nonNullValue.setScale(2, RoundingMode.HALF_DOWN)
        return createNumberFormatterForCurrencyAndFormat(budget).format(scaledNonNullValue)
    }

    fun parseBigDecimalFromInput(expression: String?, budget: Budget): BigDecimal {
        if (expression.isNullOrBlank()) {
            return BigDecimal.ZERO
        }
        val currencyFormat = budget.currencyFormat ?: DEFAULT_CURRENCY_FORMAT
        val groupingSeparator = currencyFormat.groupingSeparator
        val decimalSeparator = currencyFormat.decimalSeparator
        val numberFormatRegexp =
            Pattern.compile("((\\d{1,3})(\\$groupingSeparator(\\d{3}))*(\\$decimalSeparator(\\d)+)?)")
        val matcher = numberFormatRegexp.matcher(expression)
        val replacedValue = matcher.replaceAll { result ->
            val group = result.group(1)
            group
                .replace(groupingSeparator.toString(), "")
                .replace(decimalSeparator, '.')
        }
        return parseExpression(replacedValue)
    }

    private fun parseExpression(expression: String?) = MathExpression(expression).solve().toBigDecimal()

    fun createJavascriptFormatter(budget: Budget): String {
        //language=JavaScript
        return """
            function format(val, index) {
                return new Intl.NumberFormat('hu-HU').format((val).toFixed(2)) + ' Ft'; 
            }
            """.trimIndent();
    }

    fun formulaSensitiveTextFieldParser(event: AbstractField.ComponentValueChangeEvent<TextField, String>, budget: Budget) {
        val parsedNumber = parseBigDecimalFromInput(event.value, budget)
        val parsedNumberAsString = formatBigDecimalForInput(parsedNumber, budget)
        if (event.value != parsedNumberAsString) {
            event.source.value = parsedNumberAsString
        }
    }

    fun createBigDecimalConverter(budget: Budget): Converter<String, BigDecimal> {
        return BigDecimalFormulaAndFormatAwareConverter(this, budget)
    }

    fun formatNumber(number: Number, currency: Currency, format: CurrencyFormat): String {
        val numberFormatted = createNumberFormattedForCurrencyFormat(format).format(number)
        val symbolOrCurrency = currency.symbol ?: currency.currency
        return if (format.symbolPosition == SymbolPosition.POST) {
            "$numberFormatted $symbolOrCurrency"
        } else {
            "$symbolOrCurrency $numberFormatted"
        }
    }
}
