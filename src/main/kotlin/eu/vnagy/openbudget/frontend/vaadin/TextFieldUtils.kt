package eu.vnagy.openbudget.frontend.vaadin

import com.vaadin.flow.component.html.Span
import com.vaadin.flow.component.textfield.TextField
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.SymbolPosition

fun TextField.appendCurrency(account: Account) {
    val format = account.budget.currencyFormat ?: NumberFormatServices.DEFAULT_CURRENCY_FORMAT
    when(format.symbolPosition) {
        SymbolPosition.POST -> suffixComponent = Span(getCurrencySymbol(account, account.budget))
        SymbolPosition.PRE -> prefixComponent = Span(getCurrencySymbol(account, account.budget))
        else -> TODO()
    }
}

fun getCurrencySymbol(account: Account?, budget: Budget): String {
    val currency = account?.currency ?: budget.mainCurrency
    return currency.symbol ?: currency.currency
}