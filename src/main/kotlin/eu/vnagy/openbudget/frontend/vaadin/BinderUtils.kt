package eu.vnagy.openbudget.frontend

import com.vaadin.flow.data.binder.Binder.BindingBuilder
import java.util.*

fun <BEAN, TARGET> BindingBuilder<BEAN, TARGET>.withDefaultModelValue(defaultValue: TARGET): BindingBuilder<BEAN, TARGET> {
    return withConverter(
        { modelValue: TARGET ->
            if (Objects.isNull(modelValue)) {
                defaultValue
            } else {
                modelValue
            }
        },
        { it }
    )
}
