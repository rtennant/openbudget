package eu.vnagy.openbudget.frontend

import eu.vnagy.openbudget.model.dao.UserDao
import eu.vnagy.openbudget.model.entites.User
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser
import org.springframework.stereotype.Service
import java.time.Instant
import java.util.*


@Service
class UserProvider(
    private val userDao: UserDao,
    private val clientService: OAuth2AuthorizedClientService,
) {

    fun getAccessToken(): String {
        val authentication = SecurityContextHolder.getContext().authentication
        val token = authentication as OAuth2AuthenticationToken
        val client: OAuth2AuthorizedClient = clientService.loadAuthorizedClient(
            token.authorizedClientRegistrationId,
            token.name
        )
        val accessToken = client.accessToken.tokenValue
        return accessToken
    }
    
    fun getCurrentLocale(): Locale {
        val authentication = SecurityContextHolder.getContext().authentication
        val principal = authentication.principal as DefaultOidcUser
        return principal.locale?.let { Locale(it) } ?: Locale.ENGLISH
    }

    fun getCurrentUser(): User {
        val authentication = SecurityContextHolder.getContext().authentication
        val principal = authentication.principal as OAuth2AuthenticatedPrincipal

        val givenName = principal.getAttribute<String>("given_name")!!
        val familyName = principal.getAttribute<String>("family_name")!!
        val email = principal.getAttribute<String>("email")!!
        val subjectId = (principal as DefaultOidcUser).subject

        val userIdAsUUID = UUID.fromString(subjectId)
        var user = userDao.findById(userIdAsUUID)
        if (user != null) {
            user.familyName = familyName
            user.givenName = givenName
            user.email = email
        } else {
            user = User(
                id = userIdAsUUID,
                version = Instant.now(),
                familyName = familyName,
                givenName = givenName,
                email = email,
            )
        }
        userDao.save(user)
        return user
    }
}
