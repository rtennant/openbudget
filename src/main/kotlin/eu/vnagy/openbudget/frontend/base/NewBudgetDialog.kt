package eu.vnagy.openbudget.frontend.base

import com.github.mvysny.karibudsl.v10.*
import com.github.mvysny.karibudsl.v23.footer
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.component.upload.receivers.FileBuffer
import eu.vnagy.openbudget.frontend.UserProvider
import eu.vnagy.openbudget.frontend.base.models.NewBudgetModel
import eu.vnagy.openbudget.model.dao.CurrencyDao
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Currency
import eu.vnagy.openbudget.model.entites.CurrencyFormat
import eu.vnagy.openbudget.services.budget.NewBudgetService
import eu.vnagy.openbudget.services.importers.ynab5.YNAB5ImporterService
import java.math.BigDecimal

class NewBudgetDialog(
    private val newBudgetService: NewBudgetService,
    private val currencyDao: CurrencyDao,
    private val ynaB5ImporterService: YNAB5ImporterService,
    private val userProvider: UserProvider,
    private val onCloseCallback: (Budget) -> Unit = {}
) : Dialog() {

    private val mainLayout: VerticalLayout

    private var currencyUnitComboBox: ComboBox<Currency>? = null
    private var currencyFormatComboBox: ComboBox<CurrencyFormat>? = null

    init {
        headerTitle = "New Budget"
        width = "500px"
        height = "500px"
        mainLayout = verticalLayout {

        }
        footer {
            button("Cancel") {
                onLeftClick { close() }
            }
        }

        addInitialContent()
    }

    private fun addInitialContent() {
        mainLayout.apply {
            removeAll()
            verticalLayout {
                button("New budget from scratch") {
                    onLeftClick {
                        addNewBudgetFormContent()
                    }
                }
                button("Import from YNAB5") {
                    onLeftClick {
                        addYnab5ImportLayout()
                    }
                }
            }
        }
    }

    private fun addYnab5ImportLayout() {
        val fileBuffer = FileBuffer()
        mainLayout.removeAll()
        mainLayout.apply {
            text("Upload your YNAB5 export.")
            text(
                """you can get your export with: `curl -X GET "https://api.youneedabudget.com/v1/budgets/last-used" -H "accept: application/json" -H "Authorization: Bearer <TOKEN>"`
                """
            )
            upload {
                receiver = fileBuffer
                maxFileSize = 1024 * 1024 * 50 // 50 Mb
            }
        }
        footer.removeAll()
        footer {
            button("Cancel") {
                onLeftClick { close() }
            }
            button("Import") {
                addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                onLeftClick {
                    val budget = ynaB5ImporterService.importYnab5Export(
                        userProvider.getCurrentUser(),
                        fileBuffer.inputStream
                    )
                    onCloseCallback(budget)
                    close()
                }
            }
        }
    }

    private fun addNewBudgetFormContent() {
        val binder = beanValidationBinder<NewBudgetModel>()
        mainLayout.apply {
            removeAll()
            val availableCurrencies = currencyDao.findAll()
            val availableCurrencyFormats = currencyDao.findAllCurrencyFormats()
            formLayout {
                setSizeFull()
                formItem("Budget name") {
                    textField {
                        isRequired = true
                        bind(binder).bind(NewBudgetModel::budgetName)
                    }
                }
                formItem("Currency") {
                    currencyUnitComboBox = comboBox {
                        isRequired = true
                        setItems(availableCurrencies)
                        value = availableCurrencies.first()
                        setItemLabelGenerator { listOfNotNull(it.currency, it.symbol).joinToString(" / ") }
                        bind(binder).bind(NewBudgetModel::currency)
                        addValueChangeListener { updateCurrencyDisplay() }
                    }
                }
                formItem("Currency format") {
                    currencyFormatComboBox = comboBox {
                        isRequired = true
                        setItems(availableCurrencyFormats)
                        setItemLabelGenerator {
                            val currency = currencyUnitComboBox?.value
                            newBudgetService.formatNumber(BigDecimal("12345.67"), currency!!, it)
                        }
                        bind(binder).bind(NewBudgetModel::currencyFormat)
                        addValueChangeListener { updateCurrencyDisplay() }
                    }
                }
            }
        }

        footer.removeAll()
        footer {
            button("Cancel") {
                onLeftClick { close() }
            }
            button("Save") {
                addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                onLeftClick {
                    val newTransactionModel = NewBudgetModel()
                    binder.writeBean(newTransactionModel)
                    val budget = Budget(
                        user = userProvider.getCurrentUser(),
                        name = newTransactionModel.budgetName,
                        mainCurrency = newTransactionModel.currency!!
                    ).apply {
                        currencyFormat = newTransactionModel.currencyFormat
                    }
                    newBudgetService.createNewBudget(budget)
                    onCloseCallback(budget)
                    close()
                }
            }
        }
    }

    private fun updateCurrencyDisplay() {
        val oldValue = currencyFormatComboBox?.value
        currencyFormatComboBox?.value = oldValue
    }
}
