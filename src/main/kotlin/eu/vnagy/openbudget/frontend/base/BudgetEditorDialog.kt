package eu.vnagy.openbudget.frontend.base

import com.github.mvysny.karibudsl.v10.*
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.data.binder.Binder
import eu.vnagy.openbudget.model.dao.BudgetDao
import eu.vnagy.openbudget.model.dao.CurrencyDao
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Currency
import eu.vnagy.openbudget.model.entites.CurrencyFormat
import eu.vnagy.openbudget.services.budget.NewBudgetService
import java.math.BigDecimal
import java.util.*

class BudgetEditorDialog(
    budget: Budget,
    budgetDao: BudgetDao,
    currencyDao: CurrencyDao,
    newBudgetService: NewBudgetService,
    onCloseCallback: () -> Unit = {}
) : BaseEditorDialog<UUID, Budget, BudgetDao, Pair<CurrencyDao, NewBudgetService>>(budget, budgetDao, Pair(currencyDao, newBudgetService), onCloseCallback) {

    private var currencyUnitComboBox: ComboBox<Currency>? = null
    private var currencyFormatComboBox: ComboBox<CurrencyFormat>? = null

    override fun onEnter(binder: Binder<Budget>, onEnterElements: Pair<CurrencyDao, NewBudgetService>) {
        super.onEnter(binder, onEnterElements)
        val (currencyDao, newBudgetService) = onEnterElements
        val availableCurrencies = currencyDao.findAll()
        val availableCurrencyFormats = currencyDao.findAllCurrencyFormats()
        
        formLayout {
            setSizeFull()
            formItem("Name") {
                textField {
                    isRequired = true
                    bind(binder).bind(Budget::name)
                }
            }
            formItem("Currency") {
                currencyUnitComboBox = comboBox {
                    isRequired = true
                    isReadOnly = true
                    setItems(availableCurrencies)
                    value = availableCurrencies.first()
                    setItemLabelGenerator { listOfNotNull(it.currency, it.symbol).joinToString(" / ") }
                    bind(binder).bindReadOnly(Budget::mainCurrency)
                }
            }
            formItem("Currency format") {
                currencyFormatComboBox = comboBox {
                    isRequired = true
                    setItems(availableCurrencyFormats)
                    setItemLabelGenerator {
                        val currency = currencyUnitComboBox?.value
                        newBudgetService.formatNumber(BigDecimal("12345.67"), currency!!, it)
                    }
                    bind(binder).bind(Budget::currencyFormat)
                    addValueChangeListener { updateCurrencyDisplay() }
                }
            }
        }
    }

    private fun updateCurrencyDisplay() {
        val oldValue = currencyFormatComboBox?.value
        currencyFormatComboBox?.value = oldValue
    }
}