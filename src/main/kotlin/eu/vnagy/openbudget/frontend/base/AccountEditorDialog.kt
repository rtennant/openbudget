package eu.vnagy.openbudget.frontend.base

import com.github.mvysny.karibudsl.v10.*
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.notification.Notification
import com.vaadin.flow.data.binder.Binder
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.frontend.withDefaultModelValue
import eu.vnagy.openbudget.model.dao.AccountDao
import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Budget
import java.math.BigDecimal
import java.util.*

class AccountEditorDialog(
    account: Account,
    accountDao: AccountDao,
    numberFormatServices: NumberFormatServices,
    budget: Budget,
    onCloseCallback: () -> Unit = {}
) : BaseEditorDialog<UUID, Account, AccountDao, Pair<NumberFormatServices, Budget>>(account, accountDao, Pair(numberFormatServices, budget), onCloseCallback) {

    override fun onEnter(binder: Binder<Account>, onEnterElements: Pair<NumberFormatServices, Budget>) {
        super.onEnter(binder, onEnterElements)
        val (numberFormatServices, budget) = onEnterElements
        var advancedLayout: KFormLayout? = null
        formLayout {
            setSizeFull()
            formItem("Account's name") {
                textField {
                    isRequired = true
                    bind(binder).bind(Account::name)
                }
            }
            formItem("On Budget") {
                checkBox {
                    bind(binder).bind(Account::onBudget)
                }
            }
            formItem("Closed") {
                checkBox {
                    bind(binder).bind(Account::closed)
                }
            }
            button("Show advanced features") {
                addThemeVariants(ButtonVariant.LUMO_SMALL)
                onLeftClick {
                    val localAdvancedLayout = advancedLayout
                    if (localAdvancedLayout != null) {
                        localAdvancedLayout.isVisible = !localAdvancedLayout.isVisible
                        this.text = if (localAdvancedLayout.isVisible) "Hide advanced features" else "Show advanced features"
                    }
                }
            }
            advancedLayout = formLayout {
                isVisible = false
                formItem("Reposition zero-balance") {
                    textField {
                        bind(binder)
                            .withConverter(numberFormatServices.createBigDecimalConverter(budget))
                            .withDefaultModelValue(BigDecimal.ZERO)
                            .bind(Account::relativeZeroBalance)
                        addValueChangeListener {e ->
                            numberFormatServices.formulaSensitiveTextFieldParser(e, budget)
                        }
                    }
                }
            }
        }
    }

    override fun saveEntity() {
        if (!entity.closed) {
            super.saveEntity()
        } else {
            val balanceForAccount = dao.getAbsoluteBalanceForAccount(entity)
            if (balanceForAccount.signum() != 0) {
                Notification.show("Cannot close an account with non-null balance...") // TODO
            } else {
                super.saveEntity()
            }
        }
    }
}

