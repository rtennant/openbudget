package eu.vnagy.openbudget.frontend.base

import com.github.mvysny.karibudsl.v10.button
import com.github.mvysny.karibudsl.v10.onLeftClick
import com.github.mvysny.karibudsl.v23.footer
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.data.binder.BeanValidationBinder
import com.vaadin.flow.data.binder.Binder
import eu.vnagy.openbudget.model.dao.BaseDao
import eu.vnagy.openbudget.model.entites.BaseEntity

abstract class BaseEditorDialog<ID : Any, ENTITY : BaseEntity<ID>, DAO: BaseDao<ID, ENTITY>, ON_ENTER_ELEMENTS>(
    protected val entity: ENTITY,
    protected val dao: DAO,
    protected val onEnterElements: ON_ENTER_ELEMENTS,
    val onCloseCallback: () -> Unit,
) : Dialog() {

    private val binder: Binder<ENTITY> = BeanValidationBinder(entity::class.java) as Binder<ENTITY>
    private var enterCallbackCalled = false

    init {
        doRunOnEnter()
        width = "500px"
        height = "500px"
        open()
    }

    final override fun open() {
        super.open()
    }

    private fun doRunOnEnter() {
        enterCallbackCalled = false
        onEnter(binder, onEnterElements)
        binder.readBean(entity)
        if (!enterCallbackCalled) {
            TODO() // throw meaningful exception
        }
    }

    protected open fun onEnter(binder: Binder<ENTITY>, onEnterElements: ON_ENTER_ELEMENTS) {
        enterCallbackCalled = true
        footer {
            button("Cancel") {
                onLeftClick { close() }
            }
            button("Save") {
                addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                onLeftClick {
                    binder.writeBean(entity)
                    saveEntity()
                    close()
                }
            }
        }
    }

    protected open fun saveEntity() {
        dao.save(entity)
    }

    override fun close() {
        super.close()
        onCloseCallback()
    }
}
