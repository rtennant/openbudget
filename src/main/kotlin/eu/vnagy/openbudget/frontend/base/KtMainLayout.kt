package eu.vnagy.openbudget.frontend.base

import com.github.mvysny.karibudsl.v10.*
import com.github.mvysny.kaributools.hasChildren
import com.github.mvysny.kaributools.textAlign
import com.vaadin.flow.component.Component
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.notification.Notification
import com.vaadin.flow.component.orderedlayout.FlexLayout
import com.vaadin.flow.router.AfterNavigationEvent
import com.vaadin.flow.router.RouteParameters
import com.vaadin.starter.business.ui.MainLayout
import eu.vnagy.openbudget.frontend.UserProvider
import eu.vnagy.openbudget.frontend.account.AccountDetailsView
import eu.vnagy.openbudget.frontend.account.AllAccountDetailsView
import eu.vnagy.openbudget.frontend.budget.BudgetView
import eu.vnagy.openbudget.frontend.cookie.CookieConsentDialog
import eu.vnagy.openbudget.frontend.statistics.BaseStatisticsView.Companion.PARAMETER_BUDGET_ID
import eu.vnagy.openbudget.frontend.statistics.NetWorthView
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.model.dao.AccountDao
import eu.vnagy.openbudget.model.dao.BudgetDao
import eu.vnagy.openbudget.model.dao.CurrencyDao
import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.services.budget.NewBudgetService
import eu.vnagy.openbudget.services.importers.ynab5.YNAB5ImporterService
import org.springframework.beans.factory.annotation.Autowired
import java.time.YearMonth
import kotlin.streams.asSequence

interface HasBudget {
    val budget: Budget
}

@Suppress("SpringJavaInjectionPointsAutowiringInspection")
class KtMainLayout : MainLayout() {

    @Autowired
    private lateinit var accountDao: AccountDao

    @Autowired
    private lateinit var userProvider: UserProvider

    @Autowired
    private lateinit var numberFormatServices: NumberFormatServices

    @Autowired
    private lateinit var newBudgetService: NewBudgetService

    @Autowired
    private lateinit var currencyDao: CurrencyDao

    @Autowired
    private lateinit var ynaB5ImporterService: YNAB5ImporterService

    @Autowired
    private lateinit var budgetDao: BudgetDao

    private var detailComponents: List<Triple<String, Boolean, (Account) -> Boolean>> = emptyList()
    private lateinit var budget: Budget

    companion object {
        fun get(): KtMainLayout? {
            return UI
                .getCurrent()
                .children
                .asSequence()
                .filterIsInstance(KtMainLayout::class.java)
                .firstOrNull()
        }
    }

    init {

    }

    fun refreshAccountsMenu() {
        val menu = naviDrawer.menu
        menu.removeAll()
        menu.addNaviItem(
            "Budget overview",
            BudgetView::class.java,
            RouteParameters(
                mapOf(
                    BudgetView.PARAMETER_BUDGET_ID to budget.id.toString(),
                    BudgetView.PARAMETER_YEAR_AND_MONTH to YearMonth.now().toString(),
                )
            )
        )
        menu.addNaviItem(
            "Statistics",
            NetWorthView::class.java,
            RouteParameters(
                mapOf(
                    PARAMETER_BUDGET_ID to budget.id.toString()
                )
            )
        )
        menu.addNaviItem(
            "All Accounts",
            AllAccountDetailsView::class.java,
            RouteParameters(AllAccountDetailsView.PARAMETER_BUDGET_ID, budget.id.toString())
        )

        val accounts = accountDao.findForUser(userProvider.getCurrentUser(), budget)
        detailComponents.forEach { (parentItemTitle, opened, accountFilter) ->
            val parentItem = menu.addNaviItem(parentItemTitle, null)
            accounts
                .filter(accountFilter)
                .forEach {
                    menu.addNaviItem(
                        parentItem,
                        renderAccountInMenuRow(it),
                        AccountDetailsView::class.java,
                        RouteParameters(AccountDetailsView.PARAMETER_ACCOUNT_ID, it.id.toString())
                    )
                }
            parentItem.setSubItemsVisible(opened)
        }
    }

    private fun renderAccountInMenuRow(accountForRow: Account): Component {
        return FlexLayout().apply {
            setWidthFull()
            label(accountForRow.name) {
                addClassName("font-size-xs")
                style["cursor"] = "pointer"
                style["flex-grow"] = "1"
            }
            label {
                text = numberFormatServices.formatBigDecimalAsUnparseableText(accountDao.getBalanceForAccount(accountForRow), budget)
                addClassName("font-size-xxs")
                textAlign = "end"
                style["cursor"] = "pointer"
            }
            onLeftClick {
                AccountDetailsView.navigateHere(accountForRow)
            }
            contextMenu {
                item(
                    "Edit account",
                    clickListener = { openEditAccountDialog(accountForRow) }
                )
                item(
                    "Delete account",
                    clickListener = { deleteAccount(accountForRow) }
                )
            }
        }
    }

    private fun deleteAccount(accountForRow: Account) {
        if (accountForRow.transactions.isEmpty()) {
            accountDao.delete(accountForRow)
            refreshAccountsMenu()
        } else {
            // TODO
            Notification.show("Cannot delete an account with transactions on it. Move the transactions to another account, or try closing the account.")
        }
    }

    private fun openEditAccountDialog(account: Account) {
        AccountEditorDialog(account, accountDao, numberFormatServices, budget) {
            refreshAccountsMenu()
        }
    }

    private fun initLeftMenuItems() {
        initAboveScrollMenuIfNecessary()
        detailComponents = listOf(
            Triple("Budget accounts", true) { it.onBudget && !it.closed },
            Triple("Tracking accounts", false) { !it.onBudget && !it.closed },
            Triple("Closed accounts", false) { it.closed },
        )
        refreshAccountsMenu()
    }

    private fun initAboveScrollMenuIfNecessary() {
        if (!naviDrawer.aboveMenuArea.hasChildren) {
            naviDrawer.aboveMenuArea.apply {
                verticalLayout {
                    style["gap"] = "0"
                    style["padding"] = "0"
                    h2(budget.name) {
                        style["margin"] = "0"
                        style["padding"] = "var(--lumo-space-m)"
                        style["text-align"] = "center"
                        style["width"] = "calc(100% - var(--lumo-space-m)*2)"
                        style["box-shadow"] = "inset 0 -1px var(--lumo-contrast-10pct)"
                    }
                    horizontalLayout {
                        style["padding"] = "var(--lumo-space-m)"
                        style["width"] = "100%"
                        style["box-shadow"] = "inset 0 -1px var(--lumo-contrast-10pct)"
                        val header = h3("Accounts")
                        button("Add Account") {
                            addThemeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_PRIMARY)
                            onLeftClick {
                                NewAccountDialog(accountDao, budget, numberFormatServices) {
                                    refreshAccountsMenu()
                                }
                            }
                        }
                        setFlexGrow(1.0, header)
                    }
                }
            }
        }
    }

    override fun afterNavigation(event: AfterNavigationEvent) {
        CookieConsentDialog.openIfNecessary()
        budget = findBudgetFromViewChain(event)
        initUsernameComponent()
        initLeftMenuItems()
        afterNavigationWithoutTabs(event)
    }

    private fun initUsernameComponent() {
        if (!appBar.usernameComponent.hasChildren) {
            appBar.usernameComponent.apply {
                removeAll()
                label("${userProvider.getCurrentUser().givenName} ${userProvider.getCurrentUser().familyName}") {
                    style["cursor"] = "pointer"
                }
                contextMenu {
                    isOpenOnClick = true
                    item("New budget") {
                        onLeftClick { NewBudgetDialog(newBudgetService, currencyDao, ynaB5ImporterService, userProvider).open() }
                    }
                    item("Open existing budget") {
                        budgetDao.findByUser(userProvider.getCurrentUser()).forEach { openBudget ->
                            item(openBudget.name) {
                                onLeftClick {
                                    BudgetView.navigateHere(openBudget, YearMonth.now())
                                }
                            }
                        }
                    }
                    item("Budget Settings") {
                        onLeftClick {
                            BudgetEditorDialog(budget, budgetDao, currencyDao, newBudgetService)
                        }
                    }
                    item("Profile Settings") {
                        onLeftClick {
                            val profileUrl = "https://sso.vnagy.eu/realms/openbudget/account?referrer=openbudget&referrer_uri=https://openbudget.vnagy.eu/app" // TODO
                            ui.get().page.executeJs("""
                                window.location="$profileUrl"
                            """.trimIndent())
                        }
                    }
                    item("Logout") {
                        onLeftClick { Notification.show("Not yet implemented...") }
                    }
                }
            }
        }
    }

    private fun findBudgetFromViewChain(event: AfterNavigationEvent): Budget {
        return event
            .activeChain
            .filterIsInstance(HasBudget::class.java)
            .first()
            .budget
    }

    private fun afterNavigationWithoutTabs(event: AfterNavigationEvent) {
        val active = getActiveItem(event)
        if (active != null) {
            appBar.title = active.text
        }
    }
}