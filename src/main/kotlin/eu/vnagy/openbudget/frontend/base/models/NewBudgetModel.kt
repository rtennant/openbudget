package eu.vnagy.openbudget.frontend.base.models

import eu.vnagy.openbudget.model.entites.Currency
import eu.vnagy.openbudget.model.entites.CurrencyFormat

data class NewBudgetModel(
    var budgetName: String = "",
    var currency: Currency? = null,
    var currencyFormat: CurrencyFormat? = null,
)
