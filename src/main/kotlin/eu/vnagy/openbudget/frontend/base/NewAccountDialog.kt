package eu.vnagy.openbudget.frontend.base

import com.github.mvysny.karibudsl.v10.button
import com.github.mvysny.karibudsl.v10.onLeftClick
import com.github.mvysny.karibudsl.v10.verticalLayout
import com.github.mvysny.karibudsl.v23.footer
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.model.dao.AccountDao
import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Budget

class NewAccountDialog(
    private val accountDao: AccountDao,
    private val budget: Budget,
    private val numberFormatServices: NumberFormatServices,
    private val onCloseCallback: () -> Unit
) : Dialog() {

    private val mainLayout: VerticalLayout

    init {
        headerTitle = "New Account"
        width = "500px"
        height = "500px"
        mainLayout = verticalLayout {
            verticalLayout {
                button("New unlinked account") {
                    onLeftClick {
                        close()
                        AccountEditorDialog(
                            Account(budget = budget, currency = budget.mainCurrency),
                            accountDao,
                            numberFormatServices,
                            budget,
                            onCloseCallback,
                        )
                    }
                }
                button("New linked account") {
                    isEnabled = false
                    onLeftClick {
                        TODO()
                    }
                }
            }
        }
        footer {
            button("Cancel") {
                onLeftClick { close() }
            }
        }
        open()
    }

    override fun close() {
        super.close()
        onCloseCallback()
    }
}
