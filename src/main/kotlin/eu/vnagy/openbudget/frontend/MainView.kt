package eu.vnagy.openbudget.frontend

import com.github.mvysny.karibudsl.v10.*
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.*
import eu.vnagy.openbudget.frontend.base.NewBudgetDialog
import eu.vnagy.openbudget.frontend.budget.BudgetView
import eu.vnagy.openbudget.frontend.cookie.CookieConsentDialog
import eu.vnagy.openbudget.model.dao.CurrencyDao
import eu.vnagy.openbudget.services.budget.NewBudgetService
import eu.vnagy.openbudget.services.importers.ynab5.YNAB5ImporterService
import org.springframework.beans.factory.annotation.Autowired
import java.time.YearMonth
import javax.annotation.security.PermitAll

@Route("")
@PermitAll
class MainView: VerticalLayout(), AfterNavigationObserver {

    @Autowired
    private lateinit var userProvider: UserProvider

    @Autowired
    private lateinit var ynab5ImporterService: YNAB5ImporterService

    @Autowired
    private lateinit var newBudgetService: NewBudgetService

    @Autowired
    private lateinit var currencyDao: CurrencyDao

    override fun afterNavigation(event: AfterNavigationEvent?) {
        val currentUser = userProvider.getCurrentUser()
        if (currentUser.budgets.isNotEmpty()) {
            BudgetView.navigateHere(currentUser.budgets.first(), YearMonth.now())
        } else {
            CookieConsentDialog.openIfNecessary()
            h2("Hello...")
            text("Welcome to OpenBudget, an open source envelope budgeting software.")
            text("It seems that you don't have any budget yet. Click on the button to create your first budget.")
            button("Create budget") {
                onLeftClick {
                    NewBudgetDialog(
                        newBudgetService,
                        currencyDao,
                        ynab5ImporterService,
                        userProvider
                    ) { budget -> BudgetView.navigateHere(budget, YearMonth.now()) }.open()
                }
            }
        }
    }


}
