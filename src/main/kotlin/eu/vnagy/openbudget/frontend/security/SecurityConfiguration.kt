package eu.vnagy.openbudget.frontend.security

import com.vaadin.flow.spring.security.VaadinWebSecurity;
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@EnableWebSecurity
@Configuration
class SecurityConfiguration(
    @Value("\${vaadin.url-mapping:null}") private val vaadinUrlMapping: String?
): VaadinWebSecurity() {

    override fun configure(http: HttpSecurity) {
        super.configure(http)
        val baseUrl = (vaadinUrlMapping ?: "").removeSuffix("/*")
        val loginUrl = baseUrl + "/" + LoginView.ROUTE_NAME
        http
            .oauth2Login()
            .loginPage(loginUrl)
            .defaultSuccessUrl(baseUrl)
            .permitAll()
    }

    override fun configure(web: WebSecurity) {
        super.configure(web)
        web
            .ignoring().antMatchers("/actuator/**")
    }
}
