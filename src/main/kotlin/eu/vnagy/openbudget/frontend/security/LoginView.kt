package eu.vnagy.openbudget.frontend.security

import com.vaadin.flow.component.html.Anchor
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.Route
import com.vaadin.flow.server.auth.AnonymousAllowed
import eu.vnagy.openbudget.frontend.cookie.CookieConsentDialog
import eu.vnagy.openbudget.frontend.security.LoginView.Companion.ROUTE_NAME


@Route(ROUTE_NAME)
@AnonymousAllowed
class LoginView : VerticalLayout() {

    companion object {
        const val ROUTE_NAME = "login"
        private const val OAUTH_URL = "/oauth2/authorization/vnagy-sso"
    }

    init {
        CookieConsentDialog.openIfNecessary()
        val loginLink = Anchor(OAUTH_URL, "Login with SSO") // TODO this should be a bit nicer...
        loginLink.element.setAttribute("router-ignore", true)
        add(loginLink)
        style.set("padding", "200px")
        alignItems = FlexComponent.Alignment.CENTER
    }
}
