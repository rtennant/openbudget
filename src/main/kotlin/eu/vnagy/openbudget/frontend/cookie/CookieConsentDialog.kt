package eu.vnagy.openbudget.frontend.cookie

import com.github.mvysny.karibudsl.v10.*
import com.github.mvysny.karibudsl.v23.footer
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.server.VaadinService
import java.time.Duration
import javax.servlet.http.Cookie

class CookieConsentDialog : Dialog() {

    companion object {
        private const val COOKIE_NAME = "openbudget-vnagy-eu-cookie-conset"
        private const val COOKIE_VALUE_ALL = "all"
        private const val COOKIE_VALUE_NECESSARY = "necessary"


        fun openIfNecessary() {
            val cookies = VaadinService.getCurrentRequest().cookies
            val consentCookie = cookies.firstOrNull { it.name == COOKIE_NAME }
            if (consentCookie != null) {
                if (consentCookie.value == COOKIE_VALUE_ALL) {
                    addGoogleAnalyticsTracker();
                }
            } else {
                CookieConsentDialog().open()
            }
        }

        private fun addGoogleAnalyticsTracker() {
            val page = UI.getCurrent().page
            page.executeJs("""
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());                
                gtag('config', 'G-C3LV1KRBQ8');
            """.trimIndent())
            page.addJavaScript("https://www.googletagmanager.com/gtag/js?id=G-C3LV1KRBQ8")
        }
    }

    init {
        width = "650px"
        isModal = true
        isCloseOnEsc = false
        isCloseOnOutsideClick = false
        verticalLayout {
            h2("We use cookies")
            text("We use cookies to improve your browsing experience on our website.")
        }
        footer {
            horizontalLayout {
                button("Accept necessary") {
                    onLeftClick {
                        addCookie(COOKIE_VALUE_NECESSARY)
                        close()
                    }
                }
                button("Accept all") {
                    addThemeVariants(ButtonVariant.LUMO_PRIMARY)
                    onLeftClick {
                        addCookie(COOKIE_VALUE_ALL)
                        addGoogleAnalyticsTracker()
                        close()
                    }
                }
            }
        }
    }

    private fun addCookie(value: String) {
        val myCookie = Cookie(COOKIE_NAME, value)
        myCookie.maxAge = Duration.ofDays(5 * 365).toSeconds().toInt() // 5 years
        myCookie.path = VaadinService.getCurrentRequest().contextPath
        VaadinService.getCurrentResponse().addCookie(myCookie)
    }


}
