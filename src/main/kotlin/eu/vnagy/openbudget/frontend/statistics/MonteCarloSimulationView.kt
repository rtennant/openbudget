package eu.vnagy.openbudget.frontend.statistics

import com.github.appreciated.apexcharts.ApexChartsBuilder
import com.github.appreciated.apexcharts.config.builder.*
import com.github.appreciated.apexcharts.config.chart.Type
import com.github.appreciated.apexcharts.config.chart.builder.ZoomBuilder
import com.github.appreciated.apexcharts.config.chart.zoom.ZoomType
import com.github.appreciated.apexcharts.config.legend.HorizontalAlign
import com.github.appreciated.apexcharts.config.stroke.Curve
import com.github.appreciated.apexcharts.config.subtitle.Align.LEFT
import com.github.appreciated.apexcharts.config.xaxis.XAxisType
import com.github.appreciated.apexcharts.config.yaxis.builder.LabelsBuilder
import com.github.appreciated.apexcharts.helper.Series
import com.github.mvysny.karibudsl.v10.*
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.datepicker.DatePicker
import com.vaadin.flow.component.datepicker.DatePickerVariant
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.progressbar.ProgressBar
import com.vaadin.flow.component.textfield.NumberField
import com.vaadin.flow.component.textfield.TextFieldVariant
import com.vaadin.flow.router.Route
import com.vaadin.flow.router.RouteParam
import com.vaadin.flow.router.RouteParameters
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.frontend.base.KtMainLayout
import eu.vnagy.openbudget.frontend.statistics.MonteCarloSimulationView.Companion.ROUTE_NAME
import eu.vnagy.openbudget.model.dao.AccountDao
import eu.vnagy.openbudget.model.dao.BudgetDao
import eu.vnagy.openbudget.model.dao.TransactionDao
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.services.budget.montecarlo.BudgetMonteCarloSimulationService
import eu.vnagy.openbudget.services.budget.montecarlo.MonteCarloSimulationResultDescriptor
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalDate
import java.time.ZoneId
import java.util.concurrent.CompletableFuture
import javax.annotation.security.PermitAll

@PermitAll
@Route(ROUTE_NAME, layout = KtMainLayout::class)
class MonteCarloSimulationView @Autowired constructor(
    private val budgetDao: BudgetDao,
    private val monteCarloSimulationService: BudgetMonteCarloSimulationService,
    private val accountDao: AccountDao,
    private val transactionDao: TransactionDao,
    private val numberFormatServices: NumberFormatServices
) : BaseStatisticsView(budgetDao) {

    companion object {
        private val logger = LoggerFactory.getLogger(MonteCarloSimulationView::class.java)
        
        const val PARAMETER_BUDGET_ID = "budgetId"
        const val ROUTE_NAME = "monte-carlo-sim/:$PARAMETER_BUDGET_ID"

        fun navigateHere(budget: Budget) {
            UI.getCurrent().navigate(
                MonteCarloSimulationView::class.java, RouteParameters(
                    RouteParam(PARAMETER_BUDGET_ID, budget.id.toString())
                )
            )
        }
    }

    private var progressBar: ProgressBar? = null
    private var predictButton: Button? = null
    private var predictionNumber: NumberField? = null
    private var predictionEndDatePicker: DatePicker? = null
    private var toFactDatePicker: DatePicker? = null
    private var fromFactDatePicker: DatePicker? = null
    private var chartLayout: HorizontalLayout? = null
    
    override fun updateChartData() {
        chartLayout?.apply {
            removeAll()
            progressBar = progressBar()
        }
        predictButton?.isEnabled = false
        CompletableFuture.supplyAsync {
            val accounts = accountDao.findForUser(budget.user, budget)
            val createMonteCarloSimulations = monteCarloSimulationService.createMonteCarloSimulations(
                predictionNumber?.value?.toInt() ?: 100,
                accounts,
                fromFactDatePicker?.value ?: LocalDate.now(),
                toFactDatePicker?.value ?: LocalDate.now(),
                predictionEndDatePicker?.value ?: LocalDate.now().plusYears(10)
            ) {
                updateSimulationProgress(it)
            }
            createMonteCarloSimulations
        }.whenComplete { result, exception ->
            if (result != null) {
                ui.ifPresent { ui ->
                    ui.access {
                        predictButton?.isEnabled = true    
                        updateChartData(result) 
                    }
                }
            }
        }
    }

    private fun updateSimulationProgress(progress: Double) {
        ui.ifPresent { ui ->
            ui.access {
                progressBar?.value = progress 
            }
        }
    }

    private fun updateChartData(result: MonteCarloSimulationResultDescriptor) {
        val factDataSeries = Series(
            "Historic net worth",
            *result
                .factDataFromPast
                .entries
                .map { arrayOf(
                    it.key.atStartOfDay(ZoneId.of("Europe/Budapest")).toInstant().toEpochMilli(),  
                    it.value
                ) }
                .toTypedArray()
        )
        val simulationSize = result.orderedMonteCarloResults.size
        val simulationSeries = listOf(
            0.1 to result.orderedMonteCarloResults[(0.1*simulationSize).toInt()],
            0.33 to result.orderedMonteCarloResults[(0.33*simulationSize).toInt()],
            0.5 to result.orderedMonteCarloResults[(0.5*simulationSize).toInt()],
            0.75 to result.orderedMonteCarloResults[(0.75*simulationSize).toInt()],
            0.9 to result.orderedMonteCarloResults[(0.9*simulationSize).toInt()],
        ).map {
            Series(
                "Monte Carlo ${it.first}",
                *it.second
                    .predictedValueByDate
                    .entries
                    .map { entry -> arrayOf(
                        entry.key.atStartOfDay(ZoneId.of("Europe/Budapest")).toInstant().toEpochMilli(),
                        entry.value
                    ) }
                    .toTypedArray()
            )
        }
        val allSeries = listOf(
            listOf(factDataSeries),
            simulationSeries
        ).flatten()
        chartLayout?.apply {
            removeAll()
            init(
                ApexChartsBuilder.get()
                    .withChart(
                        ChartBuilder.get()
                            .withType(Type.LINE)
                            .withHeight("100%")
                            .withZoom(
                                ZoomBuilder.get()
                                    .withEnabled(true)
                                    .withType(ZoomType.XY)
                                    .build()
                            )
                            .build()
                    )
                    .withDataLabels(
                        DataLabelsBuilder.get()
                            .withEnabled(false)
                            .build()
                    )
                    .withStroke(StrokeBuilder.get().withCurve(Curve.STRAIGHT).build())
                    .withSeries(
                        *allSeries.toTypedArray()
                    )
                    .withTitle(
                        TitleSubtitleBuilder.get()
                            .withText("Net worth & future prediction")
                            .withAlign(LEFT).build()
                    )
                    .withXaxis(XAxisBuilder.get().withType(XAxisType.DATETIME).build())
                    .withYaxis(
                        YAxisBuilder.get()
                            .withOpposite(true)
                            .withLabels(LabelsBuilder
                                .get()
                                .withFormatter(numberFormatServices.createJavascriptFormatter(budget))
                                .build())
                            .build()
                    )
                    .withLegend(LegendBuilder.get().withHorizontalAlign(HorizontalAlign.LEFT).build())
                    .build()
            ) {
                height = "100%"
            }
        }
        logger.info("Simlation rendered for budget: {}", budget.id)
    }

    override fun createBaseLayout() {
        val accounts = accountDao.findForUser(budget.user, budget)
        val minDate = transactionDao.findMinTransactionDateForAccounts(accounts) ?: LocalDate.now()
        horizontalLayout {
            fromFactDatePicker = datePicker("Fact data from") {
                min = minDate
                value = minDate
                max = LocalDate.now()
                helperText = "Use historic data since this date"
                addThemeVariants(DatePickerVariant.LUMO_HELPER_ABOVE_FIELD)
            }
            toFactDatePicker = datePicker("Fact data until") {
                min = minDate
                value = LocalDate.now()
                max = LocalDate.now()
                helperText = "Use historic data until this date"
                addThemeVariants(DatePickerVariant.LUMO_HELPER_ABOVE_FIELD)
            }
            predictionEndDatePicker = datePicker("Predict until") {
                min = minDate
                value = LocalDate.now().plusYears(10)
                max = LocalDate.now().plusYears(20)
                addThemeVariants(DatePickerVariant.LUMO_HELPER_ABOVE_FIELD)
                helperText = "Prediction end date"
            }
            predictionNumber = numberField("Simulation no") { 
                helperText = "Number of simulations ran"
                min = 1.0
                max = 500.0
                value = 100.0
                addThemeVariants(TextFieldVariant.LUMO_HELPER_ABOVE_FIELD)
                minWidth = "200px"
            }
            predictButton = button("Predict") { 
                onLeftClick {
                    predictButton?.isEnabled = false
                    updateChartData()
                }
                addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL)
            }
        }
        chartLayout = horizontalLayout {
            setSizeFull()
        }
    }


    override fun updateMobileLayout(width: Int) {
        // TODO
    }

}