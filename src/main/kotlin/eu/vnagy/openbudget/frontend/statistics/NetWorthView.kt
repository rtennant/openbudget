package eu.vnagy.openbudget.frontend.statistics

import com.github.appreciated.apexcharts.ApexChartsBuilder
import com.github.appreciated.apexcharts.config.builder.*
import com.github.appreciated.apexcharts.config.chart.Type
import com.github.appreciated.apexcharts.config.chart.builder.ZoomBuilder
import com.github.appreciated.apexcharts.config.legend.HorizontalAlign
import com.github.appreciated.apexcharts.config.stroke.Curve
import com.github.appreciated.apexcharts.config.subtitle.Align.LEFT
import com.github.appreciated.apexcharts.config.xaxis.XAxisType
import com.github.appreciated.apexcharts.config.yaxis.builder.LabelsBuilder
import com.github.appreciated.apexcharts.helper.Series
import com.github.mvysny.karibudsl.v10.*
import com.vaadin.flow.component.ItemLabelGenerator
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.combobox.MultiSelectComboBox
import com.vaadin.flow.component.datepicker.DatePicker
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.router.Route
import com.vaadin.flow.router.RouteParam
import com.vaadin.flow.router.RouteParameters
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.frontend.base.KtMainLayout
import eu.vnagy.openbudget.model.dao.AccountDao
import eu.vnagy.openbudget.model.dao.BudgetDao
import eu.vnagy.openbudget.model.dao.TransactionDao
import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.services.budget.networth.BudgetNetWorthService
import eu.vnagy.openbudget.services.budget.networth.NetWorthDescriptor
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalDate
import java.util.concurrent.CompletableFuture
import javax.annotation.security.PermitAll

@PermitAll
@Route(NetWorthView.ROUTE_NAME, layout = KtMainLayout::class)
class NetWorthView @Autowired constructor(
    private val budgetDao: BudgetDao,
    private val accountDao: AccountDao,
    private val transactionDao: TransactionDao,
    private val netWorthService: BudgetNetWorthService,
    private val numberFormatServices: NumberFormatServices
) : BaseStatisticsView(budgetDao) {

    companion object {
        const val ROUTE_NAME = "net-worth/:$PARAMETER_BUDGET_ID"

        fun navigateHere(budget: Budget) {
            UI.getCurrent().navigate(
                NetWorthView::class.java, RouteParameters(
                    RouteParam(PARAMETER_BUDGET_ID, budget.id.toString())
                )
            )
        }
    }

    private var accountSelectComboBox: MultiSelectComboBox<Account>? = null
    private var toDatePicker: DatePicker? = null
    private var fromDatePicker: DatePicker? = null

    private var chartLayout: HorizontalLayout? = null

    override fun updateChartData() {
        val accounts = accountSelectComboBox?.value ?: accountDao.findForUser(budget.user, budget)
        val fromDate = fromDatePicker?.value ?: (transactionDao.findMinTransactionDateForAccounts(accounts) ?: LocalDate.now())
        val toDate = toDatePicker?.value ?: LocalDate.now()
        chartLayout?.apply { 
            removeAll()
            progressBar { 
                isIndeterminate = true
            }
        }
        CompletableFuture
            .supplyAsync {
                netWorthService.calculateNetWorthBetweenDates(accounts = accounts, fromDate = fromDate, toDate = toDate)
            }
            .whenComplete { result, exception ->
                if (result != null) {
                    ui.ifPresent { ui ->
                        ui.access {
                            updateChartData(result)
                        }
                    }
                }
            }
    }

    private fun updateChartData(netWorthByDate: NetWorthDescriptor) {
        chartLayout?.apply {
            removeAll()
            init(
                ApexChartsBuilder.get()
                    .withChart(
                        ChartBuilder.get()
                            .withType(Type.AREA)
                            .withHeight("100%")
                            .withZoom(
                                ZoomBuilder.get()
                                    .withEnabled(false)
                                    .build()
                            )
                            .build()
                    )
                    .withDataLabels(
                        DataLabelsBuilder.get()
                            .withEnabled(false)
                            .build()
                    )
                    .withStroke(StrokeBuilder.get().withCurve(Curve.STRAIGHT).build())
                    .withSeries(
                        Series(
                            "Net worth",
                            *netWorthByDate
                                .netWorthByDate
                                .entries
                                .sortedBy { it.key }
                                .map { it.value }
                                .toTypedArray()
                        )
                    )
                    .withTitle(
                        TitleSubtitleBuilder.get()
                            .withText("Net worth of selected accounts")
                            .withAlign(LEFT).build()
                    )
                    .withLabels(
                        *netWorthByDate
                            .netWorthByDate
                            .entries
                            .sortedBy { it.key }
                            .map { it.key.toString() }
                            .toTypedArray()
                    )
                    .withXaxis(XAxisBuilder.get().withType(XAxisType.DATETIME).build())
                    .withYaxis(
                        YAxisBuilder.get()
                            .withOpposite(true)
                            .withLabels(
                                LabelsBuilder
                                    .get()
                                    .withFormatter(numberFormatServices.createJavascriptFormatter(budget))
                                    .build()
                            )
                            .build()
                    )
                    .withLegend(LegendBuilder.get().withHorizontalAlign(HorizontalAlign.LEFT).build())
                    .build()
            ) {
                height = "100%"
            }
        }
    }

    override fun createBaseLayout() {
        val accounts = accountDao.findForUser(budget.user, budget)
        val minDate = transactionDao.findMinTransactionDateForAccounts(accounts) ?: LocalDate.now()
        horizontalLayout {
            fromDatePicker = datePicker("From") {
                min = minDate
                value = minDate
                max = LocalDate.now()
                addValueChangeListener {
                    updateChartData()
                }
            }
            toDatePicker = datePicker("To") {
                min = minDate
                value = LocalDate.now()
                max = LocalDate.now()
                addValueChangeListener {
                    updateChartData()
                }
            }
            accountSelectComboBox = init(MultiSelectComboBox<Account>("Accounts")) {
                setItems(accounts)
                setValue(accounts)
                addValueChangeListener {
                    updateChartData()
                }
                itemLabelGenerator = ItemLabelGenerator { it.name }
                width = "300px"
            }
            button("All accounts") {
                onLeftClick {
                    accountSelectComboBox?.setValue(accounts)
                }
                addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL)
            }
            button("Budget accounts") {
                onLeftClick {
                    accountSelectComboBox?.setValue(accounts.filter { it.onBudget })
                }
                addThemeVariants(ButtonVariant.LUMO_SMALL)
            }
            button("Tracking accounts") {
                onLeftClick {
                    accountSelectComboBox?.setValue(accounts.filter { !it.onBudget })
                }
                addThemeVariants(ButtonVariant.LUMO_SMALL)
            }
        }
        chartLayout = horizontalLayout {
            setSizeFull()
        }
    }


    override fun updateMobileLayout(width: Int) {
        // TODO
    }

}