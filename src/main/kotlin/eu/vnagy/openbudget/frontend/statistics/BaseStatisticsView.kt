package eu.vnagy.openbudget.frontend.statistics

import com.vaadin.flow.component.UI
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.component.tabs.Tabs
import com.vaadin.flow.router.*
import com.vaadin.flow.shared.Registration
import com.vaadin.starter.business.ui.components.navigation.bar.AppBar
import eu.vnagy.openbudget.frontend.base.HasBudget
import eu.vnagy.openbudget.frontend.base.KtMainLayout
import eu.vnagy.openbudget.model.dao.BudgetDao
import eu.vnagy.openbudget.model.entites.Budget
import java.util.*

abstract class BaseStatisticsView(
    private val budgetDao: BudgetDao,
): VerticalLayout(), HasBudget, RouterLayout, BeforeEnterObserver, AfterNavigationObserver {

    companion object {
        const val PARAMETER_BUDGET_ID = "budgetId"
    }

    private var resizeListener: Registration? = null
    private var mobileLayout: Boolean = false

    final override lateinit var budget: Budget
        private set
    
    final override fun beforeEnter(event: BeforeEnterEvent) {
        val urlParameters: RouteParameters = event.routeParameters
        val budgetId = urlParameters.get(PARAMETER_BUDGET_ID).orElse(null)?.let { UUID.fromString(it) } ?: throw NotFoundException()
        this.budget = budgetDao.findById(budgetId) ?: throw NotFoundException()
    }

    final override fun afterNavigation(event: AfterNavigationEvent?) {
        initAppBar()
        createBaseLayout()
        updateChartData()


        if (resizeListener == null) {
            resizeListener = UI.getCurrent().page.addBrowserWindowResizeListener { resizeEvent ->
                updateMobileLayout(resizeEvent.width)
            }
        }

        UI.getCurrent().page.retrieveExtendedClientDetails { receiver ->
            updateMobileLayout(receiver.screenWidth)
        }
    }

    abstract fun updateMobileLayout(width: Int)

    abstract fun updateChartData()

    abstract fun createBaseLayout()

    private fun initAppBar() {
        val appBar: AppBar = KtMainLayout.get()?.appBar!!
        appBar.removeAllTabs()
        val netWorthTab = appBar.addTab("Net worth")
        val monteCarloSimulation = appBar.addTab("Simulate future worth")
        appBar.addTabSelectionListener { e: Tabs.SelectedChangeEvent ->
            when(e.selectedTab) {
                netWorthTab -> NetWorthView.navigateHere(budget)
                monteCarloSimulation -> MonteCarloSimulationView.navigateHere(budget)
                else -> TODO()
            }
        }
        when(this) {
            is MonteCarloSimulationView -> appBar.selectedTab = monteCarloSimulation 
            is NetWorthView -> appBar.selectedTab = netWorthTab 
            else -> TODO()
        }
    }
}