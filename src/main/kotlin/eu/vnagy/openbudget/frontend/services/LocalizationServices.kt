package eu.vnagy.openbudget.frontend.services

import org.springframework.stereotype.Service
import java.time.Duration
import java.time.LocalDateTime
import java.time.Period
import java.time.temporal.ChronoUnit

@Service
class LocalizationServices {

    fun prettyPrintDurationBetween(
        startInclusive: LocalDateTime,
        endExclusive: LocalDateTime
    ): String {
        val periodBetween = Period.between(startInclusive.toLocalDate(), endExclusive.toLocalDate())
        val durationBetween = Duration.between(startInclusive, endExclusive)

        if (durationBetween < Duration.ofSeconds(30)) {
            return "just now"
        }

        val years = periodBetween.years
        val months = periodBetween.months
        val days = durationBetween.toDaysPart()
        val hours = durationBetween.toHoursPart()
        val minutes = durationBetween.toMinutesPart()
        val seconds = durationBetween.toSecondsPart()

        val amountsByUnits = listOf(
            years to ChronoUnit.YEARS,
            months to ChronoUnit.MONTHS,
            days to ChronoUnit.DAYS,
            hours to ChronoUnit.HOURS,
            minutes to ChronoUnit.MINUTES,
            seconds to ChronoUnit.SECONDS,
        )

        val firstNonNullAndLimitedSmallerOne = amountsByUnits
            .dropWhile { it.first.toInt() == 0 }
            .take(2)

        return firstNonNullAndLimitedSmallerOne
            .filter { it.first.toInt() > 0 }
            .map { toHumanReadableUnit(it.first, it.second) }
            .joinToString(separator = " and ")
    }

    private fun toHumanReadableUnit(value: Number, unit: ChronoUnit): String {
        if (value.toInt() == 0) {
            return ""
        } else {
            val lowercaseUnit = unit.name.lowercase()
            if (value == 1) {
                return "$value ${lowercaseUnit.removeSuffix("s")}"
            } else {
                return "$value $lowercaseUnit"
            }
        }
    }

}