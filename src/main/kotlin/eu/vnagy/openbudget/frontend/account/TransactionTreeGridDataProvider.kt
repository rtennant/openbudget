package eu.vnagy.openbudget.frontend.account

import com.vaadin.flow.data.provider.SortDirection
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery
import eu.vnagy.openbudget.frontend.account.search.SearchDescriptor
import eu.vnagy.openbudget.model.dao.TransactionDao
import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Transaction
import eu.vnagy.openbudget.model.entites.TransactionStatus
import eu.vnagy.openbudget.model.pojo.EqualsFilterExpression
import eu.vnagy.openbudget.model.pojo.FilterExpression
import eu.vnagy.openbudget.model.pojo.NotExpression
import eu.vnagy.openbudget.model.pojo.SortOrderDescriptor
import eu.vnagy.openbudget.model.utils.SortOrder
import java.util.stream.Stream

class TransactionTreeGridDataProvider(
    private val transactionDao: TransactionDao,
    private val accountProvider: () -> Collection<Account>,
    private val showReconciledTransactions: () -> Boolean
) : AbstractBackEndHierarchicalDataProvider<Transaction, String>() {

    var transactionFilter: SearchDescriptor<TransactionFilterTypes>? = null

    override fun getId(item: Transaction): Any {
        return item.id
    }

    override fun getChildCount(query: HierarchicalQuery<Transaction, String>): Int {
        val reconciledFilter = createReconciledFilter(showReconciledTransactions())
        return transactionDao.getChildrenTransactionCount(
            accountProvider(),
            query.parent,
            query.offset,
            query.limit,
            listOfNotNull(
                reconciledFilter,
                transactionFilter?.mapToFilters()
            )
        )
    }

    private fun createReconciledFilter(showReconciledTransactions: Boolean): FilterExpression? {
        if (showReconciledTransactions) {
            return null
        } else {
            return NotExpression(
                EqualsFilterExpression(
                    Transaction::status.name, TransactionStatus.RECONCILED
                )
            )
        }
    }

    override fun hasChildren(item: Transaction): Boolean {
        return item.childTransactions.isNotEmpty()
    }

    override fun fetchChildrenFromBackEnd(query: HierarchicalQuery<Transaction, String>): Stream<Transaction> {
        val reconciledFilter = createReconciledFilter(showReconciledTransactions())
        val sortOrders = query.sortOrders.map { vaadinSortOrder ->
            SortOrderDescriptor(
                vaadinSortOrder.sorted,
                when (vaadinSortOrder.direction) {
                    SortDirection.ASCENDING -> SortOrder.ASC
                    SortDirection.DESCENDING -> SortOrder.DESC
                    null -> throw IllegalStateException()
                }
            )
        }
        return transactionDao.findTransactionStream(
            accountProvider(),
            query.parent,
            query.offset,
            query.limit,
            sortOrders,
            listOfNotNull(
                reconciledFilter,
                transactionFilter?.mapToFilters()
            )
        )
    }



}
