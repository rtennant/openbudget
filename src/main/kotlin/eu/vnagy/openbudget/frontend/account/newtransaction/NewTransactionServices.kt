package eu.vnagy.openbudget.frontend.account.newtransaction

import com.vaadin.flow.data.converter.Converter
import com.vaadin.flow.data.provider.BackEndDataProvider
import com.vaadin.flow.data.renderer.LitRenderer
import com.vaadin.flow.data.renderer.Renderer
import eu.vnagy.openbudget.frontend.UserProvider
import eu.vnagy.openbudget.frontend.account.newtransaction.models.NewTransactionCategoryModelDataProvider
import eu.vnagy.openbudget.frontend.account.newtransaction.models.NewTransactionModel
import eu.vnagy.openbudget.frontend.account.newtransaction.models.PayeeModel
import eu.vnagy.openbudget.frontend.account.newtransaction.models.PayeeModelDataProvider
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.hasNonZeroValue
import eu.vnagy.openbudget.model.dao.AccountDao
import eu.vnagy.openbudget.model.dao.CategoryDao
import eu.vnagy.openbudget.model.dao.PayeeDao
import eu.vnagy.openbudget.model.dao.TransactionDao
import eu.vnagy.openbudget.model.dtos.CategorySumForNewTransaction
import eu.vnagy.openbudget.model.entites.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.time.LocalDate


@Service
class NewTransactionServices {

    @Autowired
    private lateinit var payeeDao: PayeeDao

    @Autowired
    private lateinit var accountDao: AccountDao

    @Autowired
    private lateinit var categoryDao: CategoryDao

    @Autowired
    private lateinit var transactionDao: TransactionDao

    @Autowired
    private lateinit var userProvider: UserProvider

    @Autowired
    private lateinit var numberFormatServices: NumberFormatServices

    fun createPayeeModelDataProvider(budget: Budget): BackEndDataProvider<PayeeModel, String> {
        return PayeeModelDataProvider(payeeDao, accountDao, userProvider, budget)
    }

    fun createCategoryModelDataProvider(budget: Budget, payee: Payee?): BackEndDataProvider<CategorySumForNewTransaction, String> {
        return NewTransactionCategoryModelDataProvider(categoryDao, budget, payee)
    }

    @Transactional
    fun saveTransaction(newTransaction: NewTransactionModel) {
        if (newTransaction.splitCategories.isNotEmpty()) {
            saveSplitTransaction(newTransaction)
        } else {
            saveNonSplitTransaction(newTransaction)
        }
    }

    private fun saveSplitTransaction(newTransaction: NewTransactionModel) {
        val inflow = newTransaction.inflow
        val outflow = newTransaction.outflow
        val transaction = Transaction(
            account = newTransaction.account,
            parentTransaction = null,
        ).apply {
            this.category = mapCategoryModelToCategory(newTransaction.category, newTransaction.account.budget)
            this.payee = newTransaction.payeeModel?.payee
            this.note = newTransaction.note
            this.date = newTransaction.date
            this.amount = (inflow ?: BigDecimal.ZERO) - (outflow ?: BigDecimal.ZERO)
            this.status = newTransaction.status
        }
        transactionDao.save(transaction)
        newTransaction.splitCategories.forEach { childTransactionModel ->
            val transferAccount = childTransactionModel.payeeModel?.transferAccount
            if (transferAccount != null) {
                saveTransferTransactions(
                    childTransactionModel.inflow,
                    childTransactionModel.outflow,
                    newTransaction.account,
                    transferAccount,
                    mapCategoryModelToCategory(childTransactionModel.category, newTransaction.account.budget),
                    null,
                    null,
                    newTransaction.date,
                    transaction,
                    newTransaction.status
                )
            } else {
                saveTransaction(
                    newTransaction.account,
                    childTransactionModel.payeeModel?.payee,
                    mapCategoryModelToCategory(childTransactionModel.category, newTransaction.account.budget),
                    childTransactionModel.inflow,
                    childTransactionModel.outflow,
                    null,
                    newTransaction.date,
                    transaction,
                    newTransaction.status
                )
            }
        }
    }

    private fun mapCategoryModelToCategory(category: CategorySumForNewTransaction?, budget: Budget): Category? {
        return if (category != null) {
            categoryDao.findById(category.categoryId, budget)
        } else {
            null
        }
    }

    private fun saveNonSplitTransaction(newTransaction: NewTransactionModel) {
        val payeeModel = newTransaction.payeeModel ?: throw IllegalStateException("Payee cannot be null when the transaction is not split.")
        val inflow = newTransaction.inflow
        val outflow = newTransaction.outflow
        if (inflow.hasNonZeroValue() && outflow.hasNonZeroValue()) {
            throw IllegalStateException("Inflow & Outflow values can't be set together.")
        }
        if (payeeModel.payee != null) {
            val category =
                newTransaction.category ?: throw IllegalStateException("Category cannot be null when the transaction is not split or not a transfer.")
            saveTransaction(
                newTransaction.account,
                payeeModel.payee,
                mapCategoryModelToCategory(category, newTransaction.account.budget),
                inflow,
                outflow,
                newTransaction.note,
                newTransaction.date,
                null,
                newTransaction.status
            )
        } else if (payeeModel.transferAccount != null) {
            if (payeeModel.transferAccount.onBudget && newTransaction.category != null) {
                throw IllegalStateException("When the transaction is a transfer to an on-budget account the category should be null.")
            }
            saveTransferTransactions(
                inflow,
                outflow,
                newTransaction.account,
                payeeModel.transferAccount,
                mapCategoryModelToCategory(newTransaction.category, newTransaction.account.budget),
                null,
                newTransaction.note,
                newTransaction.date,
                null,
                newTransaction.status
            )
        }
    }

    private fun saveTransaction(
        account: Account,
        payee: Payee?,
        category: Category?,
        inflow: BigDecimal?,
        outflow: BigDecimal?,
        note: String?,
        date: LocalDate,
        parentTransaction: Transaction?,
        status: TransactionStatus
    ) {
        val transaction = Transaction(
            account = account,
            parentTransaction = parentTransaction,
        ).apply {
            this.category = category
            this.payee = payee
            this.note = note
            this.date = date
            this.amount = (inflow ?: BigDecimal.ZERO) - (outflow ?: BigDecimal.ZERO)
            this.status = status
        }
        transactionDao.save(transaction)
    }

    private fun saveTransferTransactions(
        inflow: BigDecimal?,
        outflow: BigDecimal?,
        account: Account,
        transferAccount: Account,
        forwardCategory: Category?,
        backwardCategory: Category?,
        note: String?,
        date: LocalDate,
        parentTransaction: Transaction?,
        status: TransactionStatus
    ) {
        val forwardTransaction = Transaction(
            account = account,
            parentTransaction = parentTransaction,
        ).apply {
            this.note = note
            this.date = date
            this.category = forwardCategory
            this.amount = (inflow ?: BigDecimal.ZERO) - (outflow ?: BigDecimal.ZERO)
            this.status = status
        }
        val backwardsTransaction = Transaction(
            account = transferAccount,
            parentTransaction = null,
        ).apply {
            this.note = note
            this.date = date
            this.category = backwardCategory
            this.amount = ((inflow ?: BigDecimal.ZERO) - (outflow ?: BigDecimal.ZERO)).negate()
            this.status = status
            this.transferTransaction = forwardTransaction
        }
        transactionDao.saveAll(listOf(forwardTransaction, backwardsTransaction))
        forwardTransaction.transferTransaction = backwardsTransaction
        transactionDao.saveAll(listOf(forwardTransaction, backwardsTransaction))
    }

    fun createCategoryRenderer(budget: Budget): Renderer<CategorySumForNewTransaction> {
        return LitRenderer.of<CategorySumForNewTransaction?>(
            """
            <vertical-layout>
                <strong>${'$'}{item.name}</strong>
                <small>${'$'}{item.available} / ${'$'}{item.budgeted}</small>
            </vertical-layout>
        """.trimIndent()
        )
            .withProperty("name") { item -> item.categoryName }
            .withProperty("available") { item -> numberFormatServices.formatBigDecimalAsUnparseableText(item.yetAvailable, budget) }
            .withProperty("budgeted") { item -> numberFormatServices.formatBigDecimalAsUnparseableText(item.yetAvailable + item.alreadySpent, budget) }
    }

    fun createBigDecimalConverter(budget: Budget): Converter<String, BigDecimal> {
        return numberFormatServices.createBigDecimalConverter(budget)
    }

}
