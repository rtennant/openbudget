package eu.vnagy.openbudget.frontend.account.newtransaction

import com.github.mvysny.karibudsl.v10.*
import com.github.mvysny.karibudsl.v23.footer
import com.github.mvysny.kaributools.label
import com.github.mvysny.kaributools.refresh
import com.vaadin.flow.component.ItemLabelGenerator
import com.vaadin.flow.component.Text
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.checkbox.Checkbox
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.formlayout.FormLayout
import com.vaadin.flow.component.formlayout.FormLayout.FormItem
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.notification.Notification
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.provider.ListDataProvider
import com.vaadin.flow.data.provider.Query
import eu.vnagy.openbudget.frontend.account.newtransaction.models.NewTransactionModel
import eu.vnagy.openbudget.frontend.account.newtransaction.models.PayeeModel
import eu.vnagy.openbudget.frontend.account.newtransaction.models.SplitCategoryModel
import eu.vnagy.openbudget.frontend.vaadin.appendCurrency
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.model.dtos.CategorySumForNewTransaction
import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Payee
import eu.vnagy.openbudget.model.entites.TransactionStatus
import java.math.BigDecimal
import java.time.LocalDate
import java.util.stream.Collectors

class NewTransactionDialog(
    private val newTransactionModel: NewTransactionModel,
    private val newTransactionServices: NewTransactionServices,
    private val numberFormatServices: NumberFormatServices,
    private val account: Account,
    private val onSaveCallback: () -> Unit
) : Dialog() {

    private var payeeComboBox: ComboBox<PayeeModel>? = null
    private val binder = beanValidationBinder<NewTransactionModel>()
    private val budget = account.budget

    init {
        var splitTransactionsLayout: VerticalLayout? = null
        var splitTransactionsGrid: Grid<SplitCategoryModel>? = null
        var categoryFormItem: FormItem? = null
        var payeeFormItem: FormItem? = null
        var categoryComboBox: ComboBox<CategorySumForNewTransaction>? = null
        var inflowField: TextField? = null
        var outflowField: TextField? = null
        var splitTransactionsCheckbox: Checkbox? = null
        var childTransactionDiffLabel: Text? = null

        fun enableOrDisableTheCategoryItem() {
            val enabled: Boolean
            val isSplitTransactions = splitTransactionsCheckbox?.value ?: false
            val transferAccount = payeeComboBox?.value?.transferAccount
            enabled = when {
                transferAccount != null && transferAccount.onBudget -> false
                isSplitTransactions -> false
                else -> true
            }
            categoryFormItem?.isEnabled = enabled
            categoryComboBox?.isEnabled = enabled
            updateCategoryComboBoxValues(categoryComboBox, payeeComboBox?.value?.payee)
        }

        val splitCategoryModels = mutableListOf<SplitCategoryModel>()

        headerTitle = "New transaction"
        width = "700px"
        height = "850px"
        isResizable = true
        formLayout {
            setSizeFull()
            setResponsiveSteps(
                FormLayout.ResponsiveStep("0", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP),
                FormLayout.ResponsiveStep("600px", 1, FormLayout.ResponsiveStep.LabelsPosition.ASIDE)
            )
            formItem("Date") {
                datePicker {
                    tabIndex = 1
                    width = "80%"
                    value = LocalDate.now()
                    isRequired = true
                    bind(binder).bind(NewTransactionModel::date)
                }
            }
            payeeFormItem = formItem("Payee") {
                payeeComboBox = comboBox {
                    tabIndex = 2
                    width = "80%"
                    setItems(newTransactionServices.createPayeeModelDataProvider(newTransactionModel.account.budget))
                    isAllowCustomValue = true
                    itemLabelGenerator = ItemLabelGenerator { payeeModel -> payeeModel.toString() }
                    bind(binder).bind(NewTransactionModel::payeeModel)
                    addValueChangeListener { event ->
                        val transferAccount = event.value.transferAccount
                        enableOrDisableTheCategoryItem()
                        if (transferAccount != null && transferAccount.onBudget) {
                            splitTransactionsLayout?.isEnabled = false
                            splitTransactionsGrid?.setItems(mutableListOf())
                        }
                    }
                    addCustomValueSetListener { event ->
                        val payee = Payee(budget = newTransactionModel.account.budget).apply {
                            name = event.detail
                        }
                        this@comboBox.value = PayeeModel(payee = payee)
                    }
                }
            }
            formItem("Split between categories") {
                splitTransactionsCheckbox = checkBox {
                    tabIndex = 3
                    width = "80%"
                    addValueChangeListener { event ->
                        splitTransactionsLayout?.isEnabled = event.value
                        enableOrDisableTheCategoryItem()
                    }
                }
            }
            categoryFormItem = formItem("Category") {
                categoryComboBox = comboBox {
                    tabIndex = 4
                    width = "80%"
                    updateCategoryComboBoxValues(this@comboBox, payeeComboBox?.value?.payee)
                    setRenderer(newTransactionServices.createCategoryRenderer(budget))
                    itemLabelGenerator = ItemLabelGenerator { category -> category.categoryName }
                    bind(binder).bind(NewTransactionModel::category)
                    isClearButtonVisible = true
                }
            }
            formItem("Notes") {
                textField {
                    tabIndex = 5
                    width = "80%"
                    bind(binder).bind(NewTransactionModel::note)
                }
            }
            formItem("Inflow") {
                inflowField = textField {
                    tabIndex = 6
                    width = "80%"
                    bind(binder).withConverter(numberFormatServices.createBigDecimalConverter(budget)).bind(NewTransactionModel::inflow)
                    addValueChangeListener { e ->
                        numberFormatServices.formulaSensitiveTextFieldParser(e, budget)
                    }
                    appendCurrency(account)
                }
            }
            formItem("Outflow") {
                outflowField = textField {
                    tabIndex = 7
                    width = "80%"
                    bind(binder).withConverter(numberFormatServices.createBigDecimalConverter(budget)).bind(NewTransactionModel::outflow)
                    addValueChangeListener { e ->
                        numberFormatServices.formulaSensitiveTextFieldParser(e, budget)
                    }
                    appendCurrency(account)
                }
            }
            formItem("Cleared") {
                checkBox {
                    tabIndex = 8
                    width = "80%"
                    isEnabled = newTransactionModel.status != TransactionStatus.RECONCILED
                    bind(binder).withConverter(
                        { checked: Boolean -> if (checked) TransactionStatus.CLEARED else TransactionStatus.UNCLEARED },
                        { status -> status == TransactionStatus.CLEARED || status == TransactionStatus.RECONCILED }
                    ).bind(NewTransactionModel::status)
                }
            }
            splitTransactionsLayout = verticalLayout {
                isPadding = false
                isEnabled = false
                width = "100%"
                val newBtn = button("Add new split") {
                    tabIndex = 9
                    addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL);
                    onLeftClick {
                        NewSplitCategoryTransactionDialog(
                            newTransactionServices,
                            newTransactionModel.account,
                        ) { splitCategoryModel ->
                            splitCategoryModels.add(splitCategoryModel)
                            childTransactionDiffLabel?.text = "The diff is: ${
                                calcDifferenceBetweenTransactionTotalAndChildTransactionsTotal(inflowField, outflowField, splitCategoryModels)
                            }"
                            splitTransactionsGrid?.refresh()
                        }.open()
                    }
                }
                splitTransactionsGrid = grid(ListDataProvider(splitCategoryModels)) {
                    label = "Splits"
                    height = "200px"
                    addColumn(SplitCategoryModel::payeeModel).apply {
                        setHeader("Payee")
                    }
                    addColumn { showCategoryRowValueForSplitTransaction(it) }.apply {
                        setHeader("Category")
                    }
                    addColumn(SplitCategoryModel::inflow).apply {
                        setHeader("Inflow")
                    }
                    addColumn(SplitCategoryModel::outflow).apply {
                        setHeader("Outflow")
                    }
                    gridContextMenu {
                        item("Delete split", clickListener = { split ->
                            if (split != null) {
                                splitCategoryModels.remove(split)
                                splitTransactionsGrid?.refresh()
                            }
                        })
                    }
                }
                childTransactionDiffLabel = text(
                    "The diff is: ${
                        calcDifferenceBetweenTransactionTotalAndChildTransactionsTotal(inflowField, outflowField, splitCategoryModels)
                    }"
                )
                setAlignSelf(FlexComponent.Alignment.END, newBtn)
            }
        }
        footer {
            button("Cancel") {
                onLeftClick { close() }
            }
            button("Save") {
                tabIndex = 10
                addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                onLeftClick {
                    saveTransaction(splitTransactionsCheckbox?.value, splitCategoryModels)
                }
            }
            button("Save & Add new transaction") {
                tabIndex = 11
                addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                onLeftClick {
                    saveTransaction(splitTransactionsCheckbox?.value, splitCategoryModels)
                    NewTransactionDialog(
                        NewTransactionModel(
                            account = newTransactionModel.account,
                            date = newTransactionModel.date,
                            status = newTransactionModel.status,
                        ),
                        newTransactionServices,
                        numberFormatServices,
                        account,
                        onSaveCallback
                    ).open()
                }
            }
        }
        binder.readBean(newTransactionModel)
    }

    private fun saveTransaction(splitTransaction: Boolean?, splitCategoryModels: MutableList<SplitCategoryModel>) {
        if (splitTransaction == true && splitCategoryModels.isEmpty()) {
            Notification.show("A split transaction should have at least one split.")
            return
        }

        this.binder.writeBean(newTransactionModel)
        newTransactionModel.splitCategories = splitCategoryModels
        if (newTransactionModel.splitCategories.isNotEmpty()) {
            if (!isSplitTransactionTotalAmountCorrect(newTransactionModel)) {
                Notification.show("Split transactions does not add up.", 5000, Notification.Position.MIDDLE)
                return
            }
        } else if (newTransactionModel.payeeModel == null) {
            Notification.show("Payee cannot be empty when the transaction is not split.", 5000, Notification.Position.MIDDLE)
        }

        newTransactionServices.saveTransaction(newTransactionModel)
        onSaveCallback()
        close()
    }

    private fun showCategoryRowValueForSplitTransaction(splitCategoryModel: SplitCategoryModel): String {
        val category = splitCategoryModel.category
        if (category != null) {
            return category.categoryName
        }
        val transferAccount = splitCategoryModel.payeeModel?.transferAccount
        if (transferAccount != null) {
            return "Transfer to: ${transferAccount.name}"
        } else {
            throw IllegalStateException()
        }
    }

    private fun updateCategoryComboBoxValues(categoryComboBox: ComboBox<CategorySumForNewTransaction>?, payee: Payee?) {
        if (categoryComboBox != null) {
            val dataProvider = newTransactionServices.createCategoryModelDataProvider(newTransactionModel.account.budget, payee)
            categoryComboBox.setItems(dataProvider)
            if (categoryComboBox.isEnabled) {
                val firstTwoItems = dataProvider
                    .fetch(Query(0, 2, emptyList(), null, null))
                    .collect(Collectors.toList())
                if (firstTwoItems.size > 1) {
                    if (firstTwoItems[0].transactionCount > 1.5 * firstTwoItems[1].transactionCount) {
                        categoryComboBox.value = firstTwoItems[0]
                    }
                }
            } else {
                categoryComboBox.value = null
            }
        }
    }

    private fun calcDifferenceBetweenTransactionTotalAndChildTransactionsTotal(
        inflowField: TextField?,
        outflowField: TextField?,
        splitCategoryModels: List<SplitCategoryModel>
    ): String {
        val inflowFieldValue = numberFormatServices.parseBigDecimalFromInput(inflowField?.value, budget)
        val outflowFieldValue = numberFormatServices.parseBigDecimalFromInput(outflowField?.value, budget)
        val parentSum = inflowFieldValue - outflowFieldValue
        val childLevelSum = splitCategoryModels
            .sumOf { transaction ->
                (transaction.inflow ?: BigDecimal.ZERO) - (transaction.outflow ?: BigDecimal.ZERO)
            }
        return numberFormatServices.formatBigDecimalAsUnparseableText(parentSum - childLevelSum, budget)
    }

    private fun isSplitTransactionTotalAmountCorrect(newTransactionModel: NewTransactionModel): Boolean {
        val topLevelAmount = (newTransactionModel.inflow ?: BigDecimal.ZERO) - (newTransactionModel.outflow ?: BigDecimal.ZERO)
        val childLevelSum = newTransactionModel
            .splitCategories
            .sumOf { transaction ->
                (transaction.inflow ?: BigDecimal.ZERO) - (transaction.outflow ?: BigDecimal.ZERO)
            }
        return topLevelAmount.stripTrailingZeros() == childLevelSum.stripTrailingZeros()
    }
}
