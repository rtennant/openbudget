package eu.vnagy.openbudget.frontend.account.newtransaction.models

import com.vaadin.flow.data.provider.AbstractBackEndDataProvider
import com.vaadin.flow.data.provider.Query
import eu.vnagy.openbudget.frontend.UserProvider
import eu.vnagy.openbudget.model.dao.AccountDao
import eu.vnagy.openbudget.model.dao.PayeeDao
import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Payee
import java.util.stream.Stream

class PayeeModelDataProvider(
    private val payeeDao: PayeeDao,
    private val accountDao: AccountDao,
    private val userProvider: UserProvider,
    private val budget: Budget
) : AbstractBackEndDataProvider<PayeeModel, String>() {

    override fun fetchFromBackEnd(query: Query<PayeeModel, String>): Stream<PayeeModel> {
        val payeesWithNameCount = payeeDao.findCountForUser(userProvider.getCurrentUser(), budget, search = query.filter.orElse(""))
        val payeesWithName: List<Payee>
        if (payeesWithNameCount < query.offset) {
            payeesWithName = emptyList()
        } else {
            payeesWithName = payeeDao.findForUser(userProvider.getCurrentUser(), budget, query.offset, query.limit, search = query.filter.orElse(""))
        }

        val accountsWithName: List<Account>
        if (payeesWithName.size != query.limit) {
            accountsWithName = accountDao.findForUser(
                userProvider.getCurrentUser(),
                budget,
                query.offset - payeesWithName.size,
                query.limit - payeesWithName.size,
                search = query.filter.orElse("")
            )
        } else {
            accountsWithName = emptyList()
        }


        val payeesToPayeeModel = payeesWithName.map { PayeeModel(payee = it) }
        val accountsToPayeeModel = accountsWithName.map { PayeeModel(transferAccount = it) }
        return Stream.concat(payeesToPayeeModel.stream(), accountsToPayeeModel.stream())
    }

    override fun sizeInBackEnd(query: Query<PayeeModel, String>): Int {
        val accountsNumberWithName = accountDao.findCountForUser(userProvider.getCurrentUser(), budget, search = query.filter.orElse(""))
        val payeeNumberWithName = payeeDao.findCountForUser(userProvider.getCurrentUser(), budget, search = query.filter.orElse(""))
        val count = accountsNumberWithName + payeeNumberWithName - query.offset
        if (count > query.limit) {
            return query.limit
        } else {
            return count
        }
    }

}
