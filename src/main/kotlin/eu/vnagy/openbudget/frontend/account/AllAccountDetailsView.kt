package eu.vnagy.openbudget.frontend.account

import com.github.mvysny.karibudsl.v10.*
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.checkbox.Checkbox
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.*
import com.vaadin.flow.shared.Registration
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.frontend.UserProvider
import eu.vnagy.openbudget.frontend.account.AllAccountDetailsView.Companion.ROUTE_NAME
import eu.vnagy.openbudget.frontend.base.HasBudget
import eu.vnagy.openbudget.frontend.base.KtMainLayout
import eu.vnagy.openbudget.model.dao.*
import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Budget
import org.springframework.beans.factory.annotation.Autowired
import java.util.*
import javax.annotation.security.PermitAll

@PermitAll
@Route(ROUTE_NAME, layout = KtMainLayout::class)
class AllAccountDetailsView @Autowired constructor(
    private val budgetDao: BudgetDao,
    private val accountDao: AccountDao,
    private val transactionDao: TransactionDao,
    private val categoryDao: CategoryDao,
    private val payeeDao: PayeeDao,
    private val userProvider: UserProvider,
    private val numberFormatServices: NumberFormatServices
) : VerticalLayout(), HasBudget, RouterLayout, BeforeEnterObserver {

    private var dataGrid: TransactionsOnAccountGrid? = null

    private var showReconciledTransactionsCheckbox: Checkbox? = null
    private var showTrackingAccountsCheckbox: Checkbox? = null

    private var resizeListener: Registration? = null

    override lateinit var budget: Budget
        private set

    companion object {
        const val PARAMETER_BUDGET_ID = "budgetId"
        const val ROUTE_NAME = "all-accounts/:$PARAMETER_BUDGET_ID"

        fun navigateHere(budget: Budget) {
            UI.getCurrent().navigate(
                AllAccountDetailsView::class.java,
                RouteParameters(
                    RouteParam(PARAMETER_BUDGET_ID, budget.id.toString())
                )
            )
        }
    }

    init {
        setSizeFull()
    }

    override fun beforeEnter(event: BeforeEnterEvent) {
        val urlParameters: RouteParameters = event.routeParameters
        budget = urlParameters
            .get(PARAMETER_BUDGET_ID)
            .orElse(null)
            ?.let { UUID.fromString(it) }
            ?.let { budgetDao.findByIdAndUser(it, userProvider.getCurrentUser()) }
            ?: throw NotFoundException()
        removeAll()
        add(createSecondaryView())

        if (resizeListener == null) {
            resizeListener = event.ui.page.addBrowserWindowResizeListener { resizeEvent ->
                updateMobileLayout(resizeEvent.width)
            }
        }

        event.ui.page.retrieveExtendedClientDetails { receiver ->
            updateMobileLayout(receiver.screenWidth)
        }
    }

    private fun updateMobileLayout(width: Int) {
        dataGrid?.updateMobileLayout(width)
    }

    private fun createSecondaryView() = VerticalLayout().apply {
        setSizeFull()
        horizontalLayout {
            setWidthFull()
            val accountName = h2("All accounts")
            val balanceLayout = horizontalLayout {
                setWidthFull()
            }
            verticalLayout {
                width = "200px"
                isPadding = false
                showReconciledTransactionsCheckbox = checkBox("Show reconciled transactions") {
                    addClickListener { dataGrid?.refresh() }
                }
                showTrackingAccountsCheckbox = checkBox("Show tracking accounts") {
                    addClickListener { dataGrid?.refresh() }
                }
            }
            setFlexGrow(1.0, balanceLayout)
        }
        dataGrid = init(TransactionsOnAccountGrid(
            showAccountsColumn = true,
            transactionDao = transactionDao,
            categoryDao = categoryDao,
            payeeDao = payeeDao,
            numberFormatServices = numberFormatServices,
            accountsToShow = { listAccountsToShow() },
            budget = budget,
            refreshDataInCurrentPage = { refreshDataInCurrentPage() }
        ) { showReconciledTransactionsCheckbox?.value ?: false })
    }

    private fun listAccountsToShow(): Collection<Account> {
        val showTrackingAccountsValue = showTrackingAccountsCheckbox?.value ?: false
        return accountDao.findForUser(
            userProvider.getCurrentUser(),
            budget
        ).filter { showTrackingAccountsValue || it.onBudget }
    }

    private fun refreshDataInCurrentPage() {
        dataGrid?.refresh()
        KtMainLayout.get()?.refreshAccountsMenu()
    }

}
