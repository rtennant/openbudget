package eu.vnagy.openbudget.frontend.account.search

import eu.vnagy.openbudget.model.pojo.*
import java.util.*

class LeafSearchDescriptor<LEAF_FILTERABLE_FIELD: SearchDescriptor.FilterableField>(
    private val id: UUID = UUID.randomUUID(),
    var field: LEAF_FILTERABLE_FIELD,
    var operator: SearchDescriptor.SearchOperator = SearchDescriptor.SearchOperator.EQUALS,
    var searchValue: Any? = null
): SearchDescriptor<LEAF_FILTERABLE_FIELD> {
    override fun copy(): LeafSearchDescriptor<LEAF_FILTERABLE_FIELD> {
        return LeafSearchDescriptor(
            field = this.field,
            operator = this.operator,
            searchValue = this.searchValue
        )
    }

    override fun mapToFilters(): FilterExpression {
        return when(operator) {
            SearchDescriptor.SearchOperator.EQUALS -> EqualsFilterExpression(field.queryPath, searchValue)
            SearchDescriptor.SearchOperator.LESS_THAN -> LessThanFilterExpression(field.queryPath, searchValue)
            SearchDescriptor.SearchOperator.LESS_THAN_OR_EQUALS -> LessThanOrEqualsFilterExpression(field.queryPath, searchValue)
            SearchDescriptor.SearchOperator.GREATER_THAN -> GreaterThanFilterExpression(field.queryPath, searchValue)
            SearchDescriptor.SearchOperator.GREATER_THAN_OR_EQUALS -> GreaterThanOrEqualsFilterExpression(field.queryPath, searchValue)
        }
    }

    override fun hashCode() = id.hashCode()
    override fun equals(other: Any?) = other is LeafSearchDescriptor<*> && other.id == id
}
