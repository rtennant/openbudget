package eu.vnagy.openbudget.frontend.account.newtransaction

import com.github.mvysny.karibudsl.v10.*
import com.github.mvysny.karibudsl.v23.footer
import com.vaadin.flow.component.ItemLabelGenerator
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.formlayout.FormLayout
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep.LabelsPosition
import eu.vnagy.openbudget.frontend.account.newtransaction.models.PayeeModel
import eu.vnagy.openbudget.frontend.account.newtransaction.models.SplitCategoryModel
import eu.vnagy.openbudget.frontend.vaadin.appendCurrency
import eu.vnagy.openbudget.model.dtos.CategorySumForNewTransaction
import eu.vnagy.openbudget.model.entites.Account

class NewSplitCategoryTransactionDialog(
    private val newTransactionServices: NewTransactionServices,
    private val account: Account,
    private val onSaveCallback: (SplitCategoryModel) -> Unit
) : Dialog() {
    
    private val budget = account.budget

    init {
        headerTitle = "New split"
        width = "650px"
        height = "650px"
        val binder = beanValidationBinder<SplitCategoryModel>()
        var categoryFormItem: FormLayout.FormItem? = null
        var payeeFormItem: FormLayout.FormItem? = null
        var payeeComboBox: ComboBox<PayeeModel>? = null
        var categoryComboBox: ComboBox<CategorySumForNewTransaction>? = null
        formLayout {
            setSizeFull()
            setResponsiveSteps(
                ResponsiveStep("0", 1, LabelsPosition.TOP),
                ResponsiveStep("600px", 1, LabelsPosition.ASIDE)
            )
            payeeFormItem = formItem("Payee") {
                payeeComboBox = comboBox {
                    width = "80%"
                    setItems(newTransactionServices.createPayeeModelDataProvider(budget))
                    isAllowCustomValue = true
                    itemLabelGenerator = ItemLabelGenerator { payeeModel -> payeeModel.toString() }
                    bind(binder).bind(SplitCategoryModel::payeeModel)
                    addValueChangeListener { event ->
                        val transferAccount = event.value.transferAccount
                        val isCategoryEnabled = !(transferAccount != null && transferAccount.onBudget)
                        categoryFormItem?.isEnabled = isCategoryEnabled
                        if (isCategoryEnabled) {
                            categoryComboBox?.setItems(newTransactionServices.createCategoryModelDataProvider(budget, payeeComboBox?.value?.payee))
                        }
                    }
                }
            }
            categoryFormItem = formItem("Category") {
                categoryComboBox = comboBox {
                    width = "80%"
                    setItems(newTransactionServices.createCategoryModelDataProvider(budget, payeeComboBox?.value?.payee))
                    itemLabelGenerator = ItemLabelGenerator { category -> category.categoryName }
                    setRenderer(newTransactionServices.createCategoryRenderer(budget))
                    bind(binder).bind(SplitCategoryModel::category)
                }
            }
            formItem("Inflow") {
                textField {
                    width = "80%"
                    bind(binder).withConverter(newTransactionServices.createBigDecimalConverter(budget)).bind(SplitCategoryModel::inflow)
                    appendCurrency(account)
                }
            }
            formItem("Outflow") {
                textField {
                    width = "80%"
                    bind(binder).withConverter(newTransactionServices.createBigDecimalConverter(budget)).bind(SplitCategoryModel::outflow)
                    appendCurrency(account)
                }
            }
        }
        footer {
            button("Cancel") {
                onLeftClick { close() }
            }
            button("Add split") {
                addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                onLeftClick {
                    val splitTransactionModel = SplitCategoryModel()
                    binder.writeBean(splitTransactionModel)
                    onSaveCallback(splitTransactionModel)
                    close()
                }
            }
            button("Add split & Create new split") {
                addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                onLeftClick {
                    val splitTransactionModel = SplitCategoryModel()
                    binder.writeBean(splitTransactionModel)
                    onSaveCallback(splitTransactionModel)
                    close()
                    NewSplitCategoryTransactionDialog(newTransactionServices, account, onSaveCallback).open()
                }
            }
        }
    }
}
