package eu.vnagy.openbudget.frontend.account.newtransaction.models

import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Payee
import java.lang.IllegalStateException
import java.util.*

data class PayeeModel(
    private val id: UUID = UUID.randomUUID(),
    val payee: Payee? = null,
    val transferAccount: Account? = null
) {
    override fun toString(): String {
        if (payee != null) {
            return payee.name
        } else if (transferAccount != null) {
            return "Transfer to: ${transferAccount.name}"
        } else {
            throw IllegalStateException()
        }
    }
}
