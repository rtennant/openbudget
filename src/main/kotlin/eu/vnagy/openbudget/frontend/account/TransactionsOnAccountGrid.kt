package eu.vnagy.openbudget.frontend.account

import com.github.mvysny.karibudsl.v10.*
import com.github.mvysny.kaributools.refresh
import com.github.mvysny.kaributools.sortProperty
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.icon.Icon
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.component.treegrid.TreeGrid
import com.vaadin.flow.data.renderer.LitRenderer
import com.vaadin.flow.data.renderer.Renderer
import com.vaadin.starter.business.ui.components.ListItem
import com.vaadin.starter.business.ui.components.detailsdrawer.DetailsDrawer
import com.vaadin.starter.business.ui.components.detailsdrawer.DetailsDrawerHeader
import com.vaadin.starter.business.ui.util.UIUtils
import com.vaadin.starter.business.ui.util.css.WhiteSpace
import com.vaadin.starter.business.ui.views.SplitViewFrame
import eu.vnagy.openbudget.frontend.account.search.LeafSearchDescriptor
import eu.vnagy.openbudget.frontend.account.search.LogicalSearchDescriptor
import eu.vnagy.openbudget.frontend.account.search.SearchDescriptor
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.model.dao.CategoryDao
import eu.vnagy.openbudget.model.dao.PayeeDao
import eu.vnagy.openbudget.model.dao.TransactionDao
import eu.vnagy.openbudget.model.entites.*
import java.math.BigDecimal
import java.time.LocalDate

class TransactionsOnAccountGrid(
    private val showAccountsColumn: Boolean,
    private val transactionDao: TransactionDao,
    private val categoryDao: CategoryDao,
    private val payeeDao: PayeeDao,
    private val numberFormatServices: NumberFormatServices,
    private val budget: Budget,
    private val accountsToShow: () -> Collection<Account>,
    private val selectedTransaction: Transaction? = null,
    private val refreshDataInCurrentPage: () -> Unit,
    private val showReconciledTransactions: () -> Boolean,
) : SplitViewFrame() {

    private var screenWidth: Int = Int.MAX_VALUE

    private var dataProvider: TransactionTreeGridDataProvider? = null
    private var treeGrid: TreeGrid<Transaction>? = null
    private var searchRowsLayout: VerticalLayout? = null
    private var searchDescriptor: SearchDescriptor<TransactionFilterTypes> = LogicalSearchDescriptor()

    private var outFlowColumn: Grid.Column<Transaction>? = null
    private var inFlowColumn: Grid.Column<Transaction>? = null
    private var amountColumn: Grid.Column<Transaction>? = null
    private var statusButtonColumn: Grid.Column<Transaction>? = null
    private var categoryColumn: Grid.Column<Transaction>? = null
    private var payeeColumn: Grid.Column<Transaction>? = null

    private var detailsDrawerContent: Div? = null
    private var detailsDrawer: DetailsDrawer? = null

    init {
        style["width"] = "100%"
        style["height"] = "100%"
        setViewContent(createViewContent())
        setViewDetails(createDetailsDrawer())
    }

    private fun createDetailsDrawer(): DetailsDrawer {
        val detailsDrawer = DetailsDrawer(DetailsDrawer.Position.RIGHT)
        val detailsDrawerContent = Div()
        this.detailsDrawer = detailsDrawer
        this.detailsDrawerContent = detailsDrawerContent
        val detailsDrawerHeader = DetailsDrawerHeader("Transaction Details", detailsDrawerContent)
        detailsDrawerHeader.addCloseListener { this.detailsDrawer?.hide() }
        detailsDrawer.setHeader(detailsDrawerHeader)
        return detailsDrawer
    }

    private fun createViewContent(): VerticalLayout {
        return VerticalLayout().apply {
            setSizeFull()
            horizontalLayout {
                width = "100%"
                searchRowsLayout = verticalLayout {}
                button(icon = Icon(VaadinIcon.SEARCH_MINUS)) {
                    onLeftClick {
                        updateSearch(LogicalSearchDescriptor())
                    }
                }
                button(icon = Icon(VaadinIcon.SEARCH_PLUS)) {
                    onLeftClick {
                        EditSearchOnTransactionListDialog(
                            budget = accountsToShow().first().budget,
                            categoryDao = categoryDao,
                            payeeDao = payeeDao,
                            searchDescriptor = searchDescriptor.copy(),
                            onClose = ::updateSearch,
                        ).open()
                    }
                }
                setFlexGrow(1.0, searchRowsLayout)
            }
            dataProvider = TransactionTreeGridDataProvider(transactionDao, accountsToShow, showReconciledTransactions)
            treeGrid = treeGrid(dataProvider) {
                setSizeFull()
                addColumn(Transaction::date).apply {
                    setHeader("Date")
                    sortProperty = Transaction::date
                    width = "100px"
                }
                if (showAccountsColumn) {
                    addColumn { it.account.name }.apply {
                        setHeader("Account")
                        setSortProperty(Transaction::account.name)
                        width = "200px"
                    }
                }
                payeeColumn = addColumn(getPayeeColumnValueForRow()).apply {
                    setHeader("Payee")
                    setSortProperty(Transaction::payee.name, "${Transaction::transferTransaction.name}.${Transaction::account.name}.${Account::name.name}")
                    width = "200px"
                    isVisible = isPayeeColumnVisible()
                }
                categoryColumn = addHierarchyColumn(::getTransactionCategoryValue).apply {
                    setHeader("Category")
                    width = "200px"
                }
                outFlowColumn = addColumn(::getOutflowColumnValueForRow).apply {
                    setHeader("Outflow")
                    isVisible = isInflowOutFlowSeparateColumn()
                    width = "100px"
                }
                inFlowColumn = addColumn(::getInflowColumnValueForRow).apply {
                    setHeader("Inflow")
                    isVisible = isInflowOutFlowSeparateColumn()
                    width = "100px"
                }
                amountColumn = addColumn { t -> numberFormatServices.formatBigDecimalAsUnparseableText(t.amount, budget) }.apply {
                    setHeader("Amount")
                    isVisible = !isInflowOutFlowSeparateColumn()
                    width = "100px"
                }
                if (!showAccountsColumn) {
                    statusButtonColumn = addColumn(statusButtonRenderer()).apply {
                        isVisible = isStatusButtonVisible()
                        width = "100px"
                    }
                }

                addSelectionListener {
                    configureDetails()
                }

                gridContextMenu {
                    item("Delete transaction", clickListener = { transaction ->
                        if (transaction != null) {
                            transactionDao.delete(transaction)
                            refreshDataInCurrentPage()
                        }
                    })
                }

                isMultiSort = true
            }
            if (selectedTransaction != null) {
                treeGrid?.select(selectedTransaction)
            }
        }
    }

    private fun configureDetails() {
        val selectedTransaction = treeGrid?.selectionModel?.firstSelectedItem?.orElse(null)
        if (selectedTransaction != null) {
            detailsDrawerContent?.removeAll()
            detailsDrawerContent?.drawTransactionDetailsIntoDetailDrawer(selectedTransaction)
            detailsDrawer?.show()
        } else {
            detailsDrawer?.hide()
        }
    }

    private fun Div.drawTransactionDetailsIntoDetailDrawer(transaction: Transaction) {
        val listItems = listOf(
            ListItem(
                UIUtils.createSecondaryIcon(VaadinIcon.TAG),
                transaction.account.name,
                "Account"
            ),
            ListItem(
                UIUtils.createSecondaryIcon(VaadinIcon.GROUP),
                getTransactionCategoryValue(transaction),
                "Category"
            ),
            ListItem(
                UIUtils.createSecondaryIcon(VaadinIcon.SHOP),
                getTransactionPayeeValue(transaction),
                "Payee"
            ),
            ListItem(
                UIUtils.createSecondaryIcon(VaadinIcon.NOTEBOOK),
                transaction.note,
                "Note"
            ),
            ListItem(
                UIUtils.createSecondaryIcon(VaadinIcon.MONEY_DEPOSIT),
                numberFormatServices.formatBigDecimalAsUnparseableText(transaction.amount, budget),
                "Amount"
            )
        )
        listItems.forEach {
            it.setReverse(true)
            it.setWhiteSpace(WhiteSpace.PRE_LINE)
            add(it)
        }
    }

    private fun updateSearch(searchDescriptor: SearchDescriptor<TransactionFilterTypes>?) {
        if (searchDescriptor != null) {
            this.searchDescriptor = searchDescriptor
            this.dataProvider?.transactionFilter = searchDescriptor
            refreshDataInCurrentPage()
        }
    }

    private fun getInflowColumnValueForRow(t: Transaction): String {
        if (t.amount > BigDecimal.ZERO) {
            return numberFormatServices.formatBigDecimalAsUnparseableText(t.amount, budget)
        }
        return ""
    }

    private fun getOutflowColumnValueForRow(t: Transaction): String {
        if (t.amount < BigDecimal.ZERO) {
            return numberFormatServices.formatBigDecimalAsUnparseableText(t.amount.negate(), budget)
        }
        return ""
    }

    private fun getTransactionCategoryValue(t: Transaction): String {
        val categoryName = t.category?.name
        if (categoryName != null) {
            return categoryName
        }
        val transferTransactionTo = t.transferTransaction?.account
        if (transferTransactionTo != null) {
            return "Transfer to: ${transferTransactionTo.name}"
        }
        val childTransactions = t.childTransactions
        if (childTransactions.isNotEmpty()) {
            return "Split transaction"
        } else {
            return ""
        }
    }

    private fun getPayeeColumnValueForRow(): Renderer<Transaction> {
        return LitRenderer
            .of<Transaction>(
                """
                <vaadin-horizontal-layout style="align-items: center">
                    <div style="flex-grow: 1.0; height: max-content; overflow: hidden;">
                        <div style="width: inherit; overflow: hidden; text-overflow: ellipsis;">
                            ${'$'}{item.payeeValue}
                        </div>
                    </div>
                    <vaadin-button 
                        style="display: ${'$'}{item.transactionButtonDisplay}"
                        theme="small"
                        @click=${'$'}{handleClick}>
                        <vaadin-icon icon="vaadin:arrow-right" slot="suffix"></vaadin-icon>
                    </vaadin-button>
                </vaadin-horizontal-layout>
            """.trimIndent()
            )
            .withProperty("payeeValue") { transaction ->
                getTransactionPayeeValue(transaction)
            }
            .withProperty("transactionButtonDisplay") { transaction ->
                if (transaction.transferTransaction != null) {
                    "block"
                } else {
                    "none"
                }
            }
            .withFunction("handleClick") { transaction ->
                val transferTransaction = transaction.transferTransaction
                if (transferTransaction != null) {
                    AccountDetailsView.navigateHere(transferTransaction.account, transferTransaction)
                }
            }
    }

    private fun getTransactionPayeeValue(transaction: Transaction): String {
        val payeeName = transaction.payee?.name
        return if (payeeName != null) {
            payeeName
        } else {
            val transferAccount = transaction.transferTransaction?.account
            if (transferAccount != null) {
                "Transfer: ${transferAccount.name}"
            } else
                ""
        }
    }

    private fun statusButtonRenderer(): Renderer<Transaction> {
        return LitRenderer
            .of<Transaction>("<vaadin-button @click=\${handleClick} style='display: \${item.display}'>\${item.status}</button>")
            .withProperty("status", Transaction::status)
            .withProperty("display") { transaction ->
                if (transaction.parentTransaction == null) {
                    "block"
                } else {
                    "none"
                }
            }
            .withFunction("handleClick") { transaction ->
                val toggleable = TransactionStatus.getToggleableStatuses()
                val currentIndex = toggleable.indexOf(transaction.status)
                val nextIndex = (currentIndex + 1).mod(toggleable.size)
                transaction.status = toggleable[nextIndex]
                transaction.childTransactions.forEach { it.status = toggleable[nextIndex] }
                transactionDao.save(transaction)
                refreshDataInCurrentPage()
            }
    }

    fun refresh() {
        this.searchRowsLayout?.apply {
            this@apply.removeAll()
            text(createSearchTextRepresentation(searchDescriptor))
        }
        this.treeGrid?.refresh()
        if (selectedTransaction != null) {
            treeGrid?.select(selectedTransaction)
        }
    }

    private fun createSearchTextRepresentation(searchDescriptor: SearchDescriptor<TransactionFilterTypes>): String {
        @Suppress("REDUNDANT_ELSE_IN_WHEN")
        return when (searchDescriptor) {
            is LeafSearchDescriptor -> "${searchDescriptor.field.displayValue} ${searchDescriptor.operator.displayValue} ${
                mapToHumanReadableString(
                    searchDescriptor.searchValue
                )
            }"

            is LogicalSearchDescriptor -> if (searchDescriptor.children.isNotEmpty()) {
                searchDescriptor
                    .children
                    .map { createSearchTextRepresentation(it) }
                    .joinToString(
                        separator = " ${searchDescriptor.type.displayValue} ",
                        prefix = "(",
                        postfix = ")"
                    )
            } else {
                ""
            }

            else -> TODO() // don't ask, the Kotlin compiler needs this :'(
        }
    }

    private fun mapToHumanReadableString(searchValue: Any?): String {
        return when (searchValue) {
            is LocalDate -> searchValue.toString()
            is Category -> searchValue.name
            is Payee -> searchValue.name
            else -> TODO()
        }
    }

    fun updateMobileLayout(width: Int) {
        this.screenWidth = width
        outFlowColumn?.isVisible = isInflowOutFlowSeparateColumn()
        inFlowColumn?.isVisible = isInflowOutFlowSeparateColumn()
        amountColumn?.isVisible = !(isInflowOutFlowSeparateColumn())
        statusButtonColumn?.isVisible = isStatusButtonVisible()
        payeeColumn?.isVisible = isPayeeColumnVisible()
    }

    private fun isInflowOutFlowSeparateColumn(): Boolean {
        return screenWidth > 1250
    }

    private fun isStatusButtonVisible(): Boolean {
        return screenWidth > 1450
    }

    private fun isPayeeColumnVisible(): Boolean {
        return screenWidth > 825
    }
}
