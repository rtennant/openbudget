package eu.vnagy.openbudget.frontend.account.newtransaction.models

import eu.vnagy.openbudget.model.dtos.CategorySumForNewTransaction
import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.TransactionStatus
import java.math.BigDecimal
import java.time.LocalDate

data class NewTransactionModel(
    var date: LocalDate = LocalDate.now(),
    val account: Account,
    var payeeModel: PayeeModel? = null,
    var category: CategorySumForNewTransaction? = null,
    var note: String? = null,
    var inflow: BigDecimal? = null,
    var outflow: BigDecimal? = null,
    var status: TransactionStatus = TransactionStatus.UNCLEARED,
    var splitCategories: List<SplitCategoryModel> = mutableListOf(),
)
