package eu.vnagy.openbudget.frontend.account.search

import eu.vnagy.openbudget.model.pojo.AndExpression
import eu.vnagy.openbudget.model.pojo.FilterExpression
import eu.vnagy.openbudget.model.pojo.OrExpression
import java.util.*

class LogicalSearchDescriptor<LEAF_FILTERABLE_FIELD: SearchDescriptor.FilterableField>(
    private val id: UUID = UUID.randomUUID(),
    var type: SearchDescriptor.LogicalOperator = SearchDescriptor.LogicalOperator.AND,
    val children: MutableList<SearchDescriptor<LEAF_FILTERABLE_FIELD>> = mutableListOf()
): SearchDescriptor<LEAF_FILTERABLE_FIELD> {
    override fun copy(): LogicalSearchDescriptor<LEAF_FILTERABLE_FIELD> {
        return LogicalSearchDescriptor(
            type = this.type,
            children = children.map { it.copy() }.toMutableList()
        )
    }

    override fun mapToFilters(): FilterExpression {
        return when (type) {
            SearchDescriptor.LogicalOperator.OR -> OrExpression(children.map { it.mapToFilters() })
            SearchDescriptor.LogicalOperator.AND -> AndExpression(children.map { it.mapToFilters() })
        }
    }

    override fun hashCode() = id.hashCode()
    override fun equals(other: Any?) = other is LogicalSearchDescriptor<*> && other.id == id
}
