package eu.vnagy.openbudget.frontend.account.search

import eu.vnagy.openbudget.model.pojo.FilterExpression

sealed interface SearchDescriptor<LEAF_FILTERABLE_FIELD: SearchDescriptor.FilterableField> {

    interface FilterableField {
        val queryPath: String
    }

    enum class LogicalOperator(
        val displayValue: String
    ) {
        AND("and"),
        OR("or"),
    }

    enum class SearchOperator(
        val displayValue: String
    ) {
        EQUALS("="),
        LESS_THAN("<"),
        LESS_THAN_OR_EQUALS("<="),
        GREATER_THAN(">"),
        GREATER_THAN_OR_EQUALS(">="),
    }

    fun copy(): SearchDescriptor<LEAF_FILTERABLE_FIELD>
    fun mapToFilters(): FilterExpression

}
