package eu.vnagy.openbudget.frontend.account.search

import com.vaadin.flow.data.provider.hierarchy.AbstractHierarchicalDataProvider
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery
import java.util.stream.Stream

class HierarchicalSearchDescriptorDataProvider<LEAF_FILTERABLE_FIELD: SearchDescriptor.FilterableField>(
    private val searchDescriptor: SearchDescriptor<LEAF_FILTERABLE_FIELD>
) : AbstractHierarchicalDataProvider<SearchDescriptor<LEAF_FILTERABLE_FIELD>, Unit>() {

    override fun isInMemory() = true

    override fun hasChildren(item: SearchDescriptor<LEAF_FILTERABLE_FIELD>): Boolean {
        return item is LogicalSearchDescriptor && item.children.isNotEmpty()
    }

    override fun fetchChildren(query: HierarchicalQuery<SearchDescriptor<LEAF_FILTERABLE_FIELD>, Unit>): Stream<SearchDescriptor<LEAF_FILTERABLE_FIELD>> {
        val parent = query.parent
        if (parent == null) {
            return Stream.of(searchDescriptor)
        }
        return (parent as? LogicalSearchDescriptor)?.children?.stream() ?: Stream.empty()
    }

    override fun getChildCount(query: HierarchicalQuery<SearchDescriptor<LEAF_FILTERABLE_FIELD>, Unit>): Int {
        val parent = query.parent
        if (parent == null) {
            return 1
        }
        return (parent as? LogicalSearchDescriptor)?.children?.size ?: 0
    }

}
