package eu.vnagy.openbudget.frontend.account

import com.github.mvysny.karibudsl.v10.*
import com.github.mvysny.kaributools.get
import com.github.mvysny.kaributools.textAlign
import com.vaadin.flow.component.Key
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.checkbox.Checkbox
import com.vaadin.flow.component.contextmenu.ContextMenu
import com.vaadin.flow.component.html.H2
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.FlexLayout
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.*
import com.vaadin.flow.shared.Registration
import eu.vnagy.openbudget.frontend.account.AccountDetailsView.Companion.ROUTE_NAME
import eu.vnagy.openbudget.frontend.account.newtransaction.NewTransactionDialog
import eu.vnagy.openbudget.frontend.account.newtransaction.NewTransactionServices
import eu.vnagy.openbudget.frontend.account.newtransaction.models.NewTransactionModel
import eu.vnagy.openbudget.frontend.base.HasBudget
import eu.vnagy.openbudget.frontend.base.KtMainLayout
import eu.vnagy.openbudget.frontend.services.LocalizationServices
import eu.vnagy.openbudget.frontend.vaadin.bigdecimal.NumberFormatServices
import eu.vnagy.openbudget.frontend.vaadin.toUUID
import eu.vnagy.openbudget.model.dao.*
import eu.vnagy.openbudget.model.entites.Account
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Transaction
import eu.vnagy.openbudget.model.entites.TransactionStatus
import org.springframework.beans.factory.annotation.Autowired
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.annotation.security.PermitAll


@PermitAll
@Route(ROUTE_NAME, layout = KtMainLayout::class)
class AccountDetailsView @Autowired constructor(
    private val accountDao: AccountDao,
    private val reconciliationDao: ReconciliationDao,
    private val transactionDao: TransactionDao,
    private val categoryDao: CategoryDao,
    private val payeeDao: PayeeDao,
    private val numberFormatServices: NumberFormatServices,
    private val localizationServices: LocalizationServices,
    private val newTransactionServices: NewTransactionServices,
) : VerticalLayout(), HasBudget, RouterLayout, BeforeEnterObserver {

    companion object {
        const val PARAMETER_ACCOUNT_ID = "accountId"
        const val QUERY_PARAMETER_SELECTED_TRANSACTION = "selectedTransaction"
        const val ROUTE_NAME = "accounts/:$PARAMETER_ACCOUNT_ID"

        fun navigateHere(account: Account, selectedTransaction: Transaction? = null) {
            val url = RouteConfiguration.forSessionScope().getUrl(
                AccountDetailsView::class.java,
                RouteParameters(
                    RouteParam(PARAMETER_ACCOUNT_ID, account.id.toString())
                )
            )
            if (selectedTransaction == null) {
                UI.getCurrent()?.navigate(url)
            } else {
                UI.getCurrent()?.navigate(
                    url,
                    QueryParameters(
                        mapOf(
                            QUERY_PARAMETER_SELECTED_TRANSACTION to listOf(selectedTransaction.id.toString())
                        )
                    )
                )
            }
        }
    }

    override lateinit var budget: Budget
        private set

    private var account: Account? = null
    private var selectedTransaction: Transaction? = null

    private var showReconciledTransactionsCheckbox: Checkbox? = null
    private var dataGrid: TransactionsOnAccountGrid? = null

    private var resizeListener: Registration? = null

    private var duringReconciliation: Boolean = false
    private var balanceLayout: HorizontalLayout? = null
    private var reconciliationDiffView: VerticalLayout? = null
    private var reconciliationContextMenu: ContextMenu? = null
    private var headerLayout: FlexLayout? = null
    private var accountNameHeader: H2? = null
    private var reconcileButtonLayout: VerticalLayout? = null
    private var accountNameHeaderClickListener: Registration? = null

    private var reconciliationTransactionStatuses: List<TransactionStatus> = emptyList()
    private var reconciliationBalanceByUser: BigDecimal = BigDecimal.ZERO

    private var screenWidth: Int = Int.MAX_VALUE

    init {
        setSizeFull()
    }

    override fun beforeEnter(event: BeforeEnterEvent) {
        val urlParameters: RouteParameters = event.routeParameters
        val accountId = urlParameters.get(PARAMETER_ACCOUNT_ID).orElse(null)?.toUUID() ?: throw NotFoundException()
        val oldAccount = account
        val newAccount = accountDao.findById(accountId) ?: throw NotFoundException() 
        this.account = newAccount
        this.budget = newAccount.budget
        
        if (oldAccount != newAccount) {
            duringReconciliation = false
        }

        val selectedTransactionId = event.location.queryParameters[QUERY_PARAMETER_SELECTED_TRANSACTION]?.toUUID()
        if (selectedTransactionId != null) {
            selectedTransaction = transactionDao.findByIdAndAccount(selectedTransactionId, newAccount)
        } else {
            selectedTransaction = null
        }
        removeAll()
        add(createSecondaryView())

        if (resizeListener == null) {
            resizeListener = event.ui.page.addBrowserWindowResizeListener { resizeEvent ->
                updateMobileLayout(resizeEvent.width)
            }
        }

        event.ui.page.retrieveExtendedClientDetails { receiver ->
            updateMobileLayout(receiver.screenWidth)
        }
        refreshReconciliationDiffViewContent()
    }

    private fun updateMobileLayout(width: Int) {
        this.screenWidth = width
        headerLayout?.headerLayoutMobileUpdate()
        if (screenWidth >= 800) {
            accountNameHeader?.text = account?.name ?: ""
            accountNameHeaderClickListener?.remove()
            accountNameHeaderClickListener = null
            reconcileButtonLayout?.isVisible = true
            balanceLayout?.isVisible = true
        } else {
            if (accountNameHeaderClickListener == null) {
                accountNameHeader?.text = "${account?.name ?: ""} ⬇️"
                accountNameHeaderClickListener = accountNameHeader?.addClickListener {
                    reconcileButtonLayout?.isVisible = !(reconcileButtonLayout?.isVisible ?: false)
                    balanceLayout?.isVisible = !(balanceLayout?.isVisible ?: false)
                }
                reconcileButtonLayout?.isVisible = false
                balanceLayout?.isVisible = false
            }
        }
        dataGrid?.updateMobileLayout(width)
    }

    private fun createSecondaryView() = VerticalLayout().apply {
        setSizeFull()
        headerLayout = flexLayout {
            setWidthFull()
            style["gap"] = "var(--lumo-space-m)"
            headerLayoutMobileUpdate()
            accountNameHeader = h2(account?.name ?: "")
            balanceLayout = horizontalLayout {}
            refreshAccountSummaryAtTop()
            reconcileButtonLayout = verticalLayout {
                setSizeUndefined()
                isPadding = false
                style["gap"] = "0"
                horizontalLayout {
                    setWidthFull()
                    button("Reconcile") {
                        setWidthFull()
                        reconciliationContextMenu = this@button.contextMenu {
                            isOpenOnClick = true
                            val contextMenuLayout = verticalLayout {
                                setSizeUndefined()
                                width = "300px"
                                textAlign = "center"
                            }
                            addOpenedChangeListener { event ->
                                if (event.isOpened) {
                                    contextMenuLayout.removeAll()
                                    val lastReconciliation = reconciliationDao.findLatestReconciliationForAccount(account!!)
                                    contextMenuLayout.apply {
                                        span("Are you reconciling cleared or uncleared balance?")
                                        if (lastReconciliation != null) {
                                            val durationAsText = localizationServices.prettyPrintDurationBetween(lastReconciliation.dateTime, LocalDateTime.now())
                                            span("Last reconciliation was $durationAsText ago.") {
                                                style["font-size"] = "0.7em"
                                            }
                                        } else {
                                            span("Never reconciled.") {
                                                style["font-size"] = "0.7em"
                                            }
                                        }
                                        horizontalLayout {
                                            width = "100%"
                                            justifyContentMode = FlexComponent.JustifyContentMode.CENTER
                                            button("Cleared") {
                                                onLeftClick {
                                                    contextMenuLayout.renderReconcileAccountBalanceTypeChooser(cleared = true)
                                                }
                                            }
                                            button("Uncleared") {
                                                onLeftClick {
                                                    contextMenuLayout.renderReconcileAccountBalanceTypeChooser(cleared = false)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    button("Add new") {
                        setWidthFull()
                        onLeftClick {
                            NewTransactionDialog(
                                NewTransactionModel(account = account!!),
                                newTransactionServices,
                                numberFormatServices,
                                account!!,
                            ) {
                                refreshDataInCurrentPage()
                            }.open()
                        }
                    }
                }
                verticalLayout {
                    isPadding = false
                    showReconciledTransactionsCheckbox = checkBox("Show reconciled transactions") {
                        addClickListener { dataGrid?.refresh() }
                        value = (selectedTransaction?.status == TransactionStatus.RECONCILED)
                    }
                }
            }
            setFlexGrow(1.0, balanceLayout)
        }
        dataGrid = init(TransactionsOnAccountGrid(
            showAccountsColumn = false,
            transactionDao = transactionDao,
            categoryDao = categoryDao,
            payeeDao = payeeDao,
            numberFormatServices = numberFormatServices,
            accountsToShow = { listOfNotNull(account) },
            selectedTransaction = selectedTransaction,
            budget = budget,
            refreshDataInCurrentPage = { refreshDataInCurrentPage() }
        ) { showReconciledTransactionsCheckbox?.value ?: false })
    }

    private fun refreshDataInCurrentPage() {
        dataGrid?.refresh()
        refreshReconciliationDiffViewContent()
        refreshAccountSummaryAtTop()
        KtMainLayout.get()?.refreshAccountsMenu()
    }

    private fun FlexLayout.headerLayoutMobileUpdate() {
        setFlexDirection(if (screenWidth >= 800) FlexLayout.FlexDirection.ROW else FlexLayout.FlexDirection.COLUMN)
        alignItems = if (screenWidth >= 800) FlexComponent.Alignment.END else FlexComponent.Alignment.STRETCH
    }

    private fun refreshAccountSummaryAtTop() {
        val account = this.account ?: TODO()
        balanceLayout?.removeAll()
        balanceLayout?.apply {
            val totalValue = accountDao.getBalanceForAccount(account)
            val clearedValue = accountDao.getBalanceForAccount(
                account, transactionStatuses = listOf(TransactionStatus.RECONCILED, TransactionStatus.CLEARED)
            )
            val unclearedValue = accountDao.getBalanceForAccount(account, transactionStatuses = listOf(TransactionStatus.UNCLEARED))
            verticalLayout {
                setSizeUndefined()
                isPadding = false
                isMargin = false
                h4(numberFormatServices.formatBigDecimalAsUnparseableText(totalValue, budget)) {
                    style.set("margin", "0")
                }
                label("Total value")
                alignItems = FlexComponent.Alignment.CENTER
                style.set("gap", "0")
            }
            label("=")
            verticalLayout {
                setSizeUndefined()
                isPadding = false
                isMargin = false
                h4(numberFormatServices.formatBigDecimalAsUnparseableText(clearedValue, budget)) {
                    style.set("margin", "0")
                }
                label("Cleared value")
                alignItems = FlexComponent.Alignment.CENTER
                style.set("gap", "0")
            }
            label("+")
            verticalLayout {
                setSizeUndefined()
                isPadding = false
                isMargin = false
                h4(numberFormatServices.formatBigDecimalAsUnparseableText(unclearedValue, budget)) {
                    style.set("margin", "0")
                }
                label("Uncleared value")
                alignItems = FlexComponent.Alignment.CENTER
                style.set("gap", "0")
            }
            if (reconciliationDiffView == null) {
                reconciliationDiffView = verticalLayout {
                    isPadding = false
                    isMargin = false
                    alignItems = FlexComponent.Alignment.END
                }
            } else {
                add(reconciliationDiffView)
            }
            setFlexGrow(1.0, reconciliationDiffView)
        }
    }

    private fun VerticalLayout.renderReconcileAccountBalanceTypeChooser(cleared: Boolean) {
        removeAll()
        textAlign = "center"
        width = "100%"
        justifyContentMode = FlexComponent.JustifyContentMode.CENTER
        if (cleared) {
            reconciliationTransactionStatuses = listOf(TransactionStatus.CLEARED, TransactionStatus.RECONCILED)
            text("Is your cleared balance equals to:")
        } else {
            reconciliationTransactionStatuses = TransactionStatus.values().toList()
            text("Is your uncleared balance equals to:")
        }
        label(
            numberFormatServices.formatBigDecimalAsUnparseableText(
                accountDao.getBalanceForAccount(account!!, reconciliationTransactionStatuses),
                budget
            )
        )
        horizontalLayout {
            button("No") {
                onLeftClick {
                    renderReconcileAccountBalanceForm()
                }
            }
            button("Yes") {
                onLeftClick {
                    reconciliationContextMenu?.close()
                    finishReconciliation()
                }
            }
        }
    }

    private fun VerticalLayout.renderReconcileAccountBalanceForm() {
        removeAll()
        textAlign = "center"
        width = "100%"
        justifyContentMode = FlexComponent.JustifyContentMode.CENTER
        text("Please, enter your account's balance:")
        val accountBalanceField = textField {
            addKeyPressListener { e ->
                if (e.key == Key.ENTER) {
                    reconcileWithAccountBalance(this@textField.value)
                }
            }
        }
        accountBalanceField.focus()
        button("Reconcile") {
            onLeftClick {
                reconcileWithAccountBalance(accountBalanceField.value)
            }
        }
    }

    private fun reconcileWithAccountBalance(value: String?) {
        reconciliationContextMenu?.close()
        duringReconciliation = true
        reconciliationBalanceByUser = numberFormatServices.parseBigDecimalFromInput(value, budget)
        refreshReconciliationDiffViewContent()
    }

    private fun refreshReconciliationDiffViewContent() {
        reconciliationDiffView?.apply {
            removeAll()
            if (duringReconciliation) {
                val accountBalanceByDatabase = accountDao.getBalanceForAccount(account!!, reconciliationTransactionStatuses)
                val reconciliationDiff = reconciliationBalanceByUser - accountBalanceByDatabase
                verticalLayout {
                    isPadding = false
                    isMargin = false
                    width = "300px"
                    text("Diff: ${numberFormatServices.formatBigDecimalAsUnparseableText(reconciliationDiff, budget)}")
                    horizontalLayout {
                        button("Cancel") {
                            onLeftClick {
                                duringReconciliation = false
                                refreshDataInCurrentPage()
                            }
                        }
                        button("Finish") {
                            onLeftClick {
                                finishReconciliation(amount = reconciliationDiff)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun finishReconciliation(amount: BigDecimal = BigDecimal.ZERO) {
        accountDao.reconcileAccount(account!!, amount)
        duringReconciliation = false
        refreshDataInCurrentPage()
    }
}
