package eu.vnagy.openbudget.frontend.account

import com.github.mvysny.karibudsl.v10.button
import com.github.mvysny.karibudsl.v10.onLeftClick
import com.github.mvysny.karibudsl.v10.treeGrid
import com.github.mvysny.karibudsl.v23.footer
import com.github.mvysny.kaributools.ComboBoxVariant
import com.github.mvysny.kaributools.addThemeVariants
import com.github.mvysny.kaributools.expandAll
import com.github.mvysny.kaributools.refresh
import com.vaadin.flow.component.Component
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.datepicker.DatePicker
import com.vaadin.flow.component.datepicker.DatePickerVariant
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.treegrid.TreeGrid
import com.vaadin.flow.data.renderer.LitRenderer
import com.vaadin.flow.data.renderer.Renderer
import eu.vnagy.openbudget.frontend.account.search.HierarchicalSearchDescriptorDataProvider
import eu.vnagy.openbudget.frontend.account.search.LeafSearchDescriptor
import eu.vnagy.openbudget.frontend.account.search.LogicalSearchDescriptor
import eu.vnagy.openbudget.frontend.account.search.SearchDescriptor
import eu.vnagy.openbudget.frontend.account.search.SearchDescriptor.*
import eu.vnagy.openbudget.model.dao.CategoryDao
import eu.vnagy.openbudget.model.dao.PayeeDao
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Category
import eu.vnagy.openbudget.model.entites.Payee
import eu.vnagy.openbudget.model.entites.Transaction
import java.time.LocalDate
import java.util.*

enum class TransactionFilterTypes(
    val displayValue: String,
    override val queryPath: String
): FilterableField {
    DATE("Date", Transaction::date.name),
    CATEGORY("Category", Transaction::category.name),
    PAYEE("Payee", Transaction::payee.name),
}

class EditSearchOnTransactionListDialog(
    private val budget: Budget,
    private val categoryDao: CategoryDao,
    private val payeeDao: PayeeDao,
    private val searchDescriptor: SearchDescriptor<TransactionFilterTypes>,
    private val onClose: (SearchDescriptor<TransactionFilterTypes>?) -> Unit
) : Dialog() {

    private val grid: TreeGrid<SearchDescriptor<TransactionFilterTypes>>

    init {
        headerTitle = "Add search"
        width = "950px"
        height = "550px"
        grid = treeGrid(dataProvider = HierarchicalSearchDescriptorDataProvider(searchDescriptor)) {
            addComponentHierarchyColumn { rowDescriptor ->
                if (rowDescriptor is LogicalSearchDescriptor) {
                    ComboBox<LogicalOperator>().apply {
                        this@apply.setId(UUID.randomUUID().toString())
                        setItems(LogicalOperator.values().toList())
                        this@apply.addThemeVariants(ComboBoxVariant.Small)
                        width = "100px"
                        value = rowDescriptor.type
                        setItemLabelGenerator { it.displayValue }
                        addValueChangeListener { event ->
                            rowDescriptor.type = event.value
                        }
                    }
                } else {
                    Div()
                }
            }.apply {
                this@apply.setHeader("Type")
                this@apply.isResizable = true
                this@apply.width = "150px"
            }
            addComponentColumn { rowDescriptor -> renderPossibleFilteredColumns(rowDescriptor) }.apply {
                this@apply.setHeader("Property")
                this@apply.isResizable = true
                this@apply.width = "150px"
            }
            addComponentColumn { rowDescriptor -> renderPossibleFilterOperationsFor(rowDescriptor) }.apply {
                this@apply.setHeader("Operation")
                this@apply.isResizable = true
                this@apply.width = "140px"
            }
            addComponentColumn { rowDescriptor -> renderValueInputFieldFor(rowDescriptor) }.apply {
                this@apply.setHeader("Value")
                this@apply.isResizable = true
                this@apply.width = "150px"
            }
            addColumn(addChildButtonRenderer()).apply {
                this@apply.setHeader("Add sub-filter")
                this@apply.isResizable = true
                this@apply.width = "200px"
            }
        }
        footer {
            button("Cancel") {
                onLeftClick {
                    onClose(null)
                    close()
                }
            }
            button("Save") {
                tabIndex = 10
                addThemeVariants(ButtonVariant.LUMO_PRIMARY)
                onLeftClick {
                    onClose(searchDescriptor)
                    close()
                }
            }
        }
    }

    private fun renderValueInputFieldFor(rowDescriptor: SearchDescriptor<TransactionFilterTypes>): Component {
        if (rowDescriptor is LeafSearchDescriptor) {
            val component = when(rowDescriptor.field) {
                TransactionFilterTypes.DATE -> renderDateFilterInputForRow(rowDescriptor)
                TransactionFilterTypes.CATEGORY -> renderCategoryInputForRow(rowDescriptor)
                TransactionFilterTypes.PAYEE -> renderPayeeInputForRow(rowDescriptor)
            }
            component.width = "140px"
            component.setId(UUID.randomUUID().toString())
            rowDescriptor.searchValue = component.value // use it for the default value bindign
            component.addValueChangeListener { event ->
                rowDescriptor.searchValue = event.value
                grid.refresh()
                grid.expandAll()
            }
            return component
        }
        return Div()
    }

    private fun renderPayeeInputForRow(rowDescriptor: LeafSearchDescriptor<TransactionFilterTypes>): ComboBox<Payee> {
        val payees = payeeDao.findForUser(user = budget.user, budget = budget)
        return ComboBox<Payee>().apply {
            setItems(payees)
            value = rowDescriptor.searchValue as? Payee ?: payees.firstOrNull()
            setItemLabelGenerator { it.name }
            this@apply.addThemeVariants(ComboBoxVariant.Small)
        }
    }

    private fun renderCategoryInputForRow(rowDescriptor: LeafSearchDescriptor<TransactionFilterTypes>): ComboBox<Category> {
        val categories = categoryDao.findByBudget(budget = budget)
        return ComboBox<Category>().apply {
            setItems(categories)
            value = rowDescriptor.searchValue as? Category ?: categories.firstOrNull()
            setItemLabelGenerator { it.name }
            this@apply.addThemeVariants(ComboBoxVariant.Small)
        }
    }

    private fun renderDateFilterInputForRow(rowDescriptor: LeafSearchDescriptor<TransactionFilterTypes>): DatePicker {
        return DatePicker().apply {
            value = rowDescriptor.searchValue as? LocalDate ?: LocalDate.now()
            addThemeVariants(DatePickerVariant.LUMO_SMALL)
        }
    }

    private fun renderPossibleFilteredColumns(rowDescriptor: SearchDescriptor<TransactionFilterTypes>): Component {
        return if (rowDescriptor is LeafSearchDescriptor) {
            ComboBox<TransactionFilterTypes>().apply {
                this@apply.setId(UUID.randomUUID().toString())
                width = "125px"
                setItems(TransactionFilterTypes.values().toList())
                value = rowDescriptor.field
                setItemLabelGenerator { it.displayValue }
                this@apply.addThemeVariants(ComboBoxVariant.Small)
                addValueChangeListener { event ->
                    rowDescriptor.field = event.value
                    grid.refresh()
                    grid.expandAll()
                }
            }
        } else {
            Div()
        }
    }

    private fun renderPossibleFilterOperationsFor(rowDescriptor: SearchDescriptor<TransactionFilterTypes>): Component {
        if (rowDescriptor is LeafSearchDescriptor) {
            val possibleFilterTypes = when(rowDescriptor.field) {
                TransactionFilterTypes.DATE -> SearchOperator.values().toList()
                TransactionFilterTypes.CATEGORY -> listOf(SearchOperator.EQUALS)
                TransactionFilterTypes.PAYEE -> listOf(SearchOperator.EQUALS)
            }
            return ComboBox<SearchOperator>().apply {
                this@apply.setId(UUID.randomUUID().toString())
                setItems(possibleFilterTypes)
                value = rowDescriptor.operator
                width = "75px"
                setItemLabelGenerator { it.displayValue }
                this@apply.addThemeVariants(ComboBoxVariant.Small)
                addValueChangeListener { event ->
                    rowDescriptor.operator = event.value
                }
            }
        }
        return Div()
    }

    private fun addChildButtonRenderer(): Renderer<SearchDescriptor<TransactionFilterTypes>> {
        return LitRenderer
            .of<SearchDescriptor<TransactionFilterTypes>>("""
                <vaadin-vertical-layout style='display: ${'$'}{item.display}'>
                    <!-- <vaadin-button @click=${'$'}{addSubQuery} theme='small'>Add subquery</vaadin-button> -->
                    <vaadin-button @click=${'$'}{addFilter} theme='small'>Add filter</vaadin-button>
                </<vaadin-vertical-layout>
                """)
            .withProperty("display") { descriptor ->
                if (descriptor is LogicalSearchDescriptor) {
                    "block"
                } else {
                    "none"
                }
            }
            .withFunction("addSubQuery") { descriptor ->
                (descriptor as? LogicalSearchDescriptor)?.children?.add(LogicalSearchDescriptor())
                grid.refresh()
                grid.expandAll()
            }
            .withFunction("addFilter") { descriptor ->
                (descriptor as? LogicalSearchDescriptor)?.children?.add(LeafSearchDescriptor(field = TransactionFilterTypes.DATE))
                grid.refresh()
                grid.expandAll()
            }
    }

}
