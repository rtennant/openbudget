package eu.vnagy.openbudget.frontend.account.newtransaction.models

import eu.vnagy.openbudget.model.dtos.CategorySumForNewTransaction
import java.math.BigDecimal
import java.util.*

data class SplitCategoryModel(
    private val id: UUID = UUID.randomUUID(),
    var payeeModel: PayeeModel? = null,
    var category: CategorySumForNewTransaction? = null,
    var inflow: BigDecimal? = null,
    var outflow: BigDecimal? = null,
)
