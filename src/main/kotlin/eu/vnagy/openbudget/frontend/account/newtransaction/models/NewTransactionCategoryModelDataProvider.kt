package eu.vnagy.openbudget.frontend.account.newtransaction.models

import com.vaadin.flow.data.provider.AbstractBackEndDataProvider
import com.vaadin.flow.data.provider.Query
import eu.vnagy.openbudget.model.dao.CategoryDao
import eu.vnagy.openbudget.model.dtos.CategorySumForNewTransaction
import eu.vnagy.openbudget.model.entites.Budget
import eu.vnagy.openbudget.model.entites.Payee
import java.util.stream.Stream

class NewTransactionCategoryModelDataProvider(
    private val categoryDao: CategoryDao,
    private val budget: Budget,
    private val payee: Payee?,
) : AbstractBackEndDataProvider<CategorySumForNewTransaction, String>() {

    override fun fetchFromBackEnd(query: Query<CategorySumForNewTransaction, String>): Stream<CategorySumForNewTransaction> {
        val list = categoryDao.findLeafCategoriesForNewTransaction(
            budget,
            payee,
            query.filter.orElse(""),
            query.offset,
            query.limit
        )
        return list.stream()
    }

    override fun sizeInBackEnd(query: Query<CategorySumForNewTransaction, String>): Int {
        return categoryDao.countLeafCategoriesForNewTransaction(
            budget,
            query.filter.orElse(""),
            query.offset,
            query.limit
        )
    }

}
