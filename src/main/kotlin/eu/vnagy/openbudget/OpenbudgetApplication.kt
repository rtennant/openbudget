package eu.vnagy.openbudget

import com.vaadin.flow.component.page.AppShellConfigurator
import com.vaadin.flow.component.page.Push
import com.vaadin.flow.component.page.Viewport
import com.vaadin.flow.server.AppShellSettings
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.transaction.annotation.EnableTransactionManagement

@Push
@SpringBootApplication
@EnableTransactionManagement
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
class OpenbudgetApplication: AppShellConfigurator {
	override fun configurePage(settings: AppShellSettings) {
		settings.addMetaTag("apple-mobile-web-app-capable", "yes")
		settings.addMetaTag("apple-mobile-web-app-status-bar-style", "black")
	}
}

fun main(args: Array<String>) {
    runApplication<OpenbudgetApplication>(*args)
}
