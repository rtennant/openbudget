import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.nio.charset.StandardCharsets

plugins {
	id("org.springframework.boot") version "2.7.3"
	id("io.spring.dependency-management") version "1.0.13.RELEASE"
	id("com.vaadin") version "23.2.6"
	kotlin("jvm") version "1.6.21"
	kotlin("plugin.spring") version "1.6.21"
	kotlin("kapt") version "1.7.0"
	id("io.ebean") version "13.9.0"
}

group = "eu.vnagy"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
	mavenCentral()
	maven("https://maven.vaadin.com/vaadin-prereleases")
	maven("https://maven.vaadin.com/vaadin-addons")
}

extra["vaadinVersion"] = "23.2.6"

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-jdbc")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.vaadin:vaadin-spring-boot-starter")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.liquibase:liquibase-core")
	implementation("org.springframework:spring-tx")
	implementation("com.github.mvysny.karibudsl:karibu-dsl-v23:1.1.3")
	implementation("io.ebean:ebean:13.9.0")
	implementation("io.ebean:ebean-spring-txn:13.0.0")
	implementation("com.google.code.gson:gson:2.9.1")
	implementation("org.springframework.boot:spring-boot-starter-oauth2-client")
	implementation("com.github.appreciated:apexcharts:23.0.0")
	implementation("org.apache.commons:commons-math3:3.6.1")
	implementation("com.github.gbenroscience:parser-ng:0.1.8")
	
	developmentOnly("org.springframework.boot:spring-boot-devtools")
	runtimeOnly("org.postgresql:postgresql")
	testImplementation("org.junit.jupiter:junit-jupiter-engine")
	testImplementation("org.junit.jupiter:junit-jupiter-api")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("net.datafaker:datafaker:1.6.0")
	testImplementation("org.testcontainers:testcontainers:1.17.6")
	testImplementation("org.testcontainers:postgresql:1.17.6")
	testImplementation("org.testcontainers:junit-jupiter:1.17.6")
	testImplementation("org.mockito.kotlin:mockito-kotlin:4.1.0")
}

dependencyManagement {
	imports {
		mavenBom("com.vaadin:vaadin-bom:${property("vaadinVersion")}")
	}
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict", "-Xallow-kotlin-package", "-opt-in=kotlin.contracts.ExperimentalContracts")
		jvmTarget = "11"
		
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
	if (environment["CI"] != "true") {
		val process: Process = Runtime.getRuntime().exec(arrayOf("id", "-u"))
		val uid = process.inputStream.readAllBytes().toString(StandardCharsets.UTF_8).trim()
		val dockerSocket = "unix:///run/user/$uid/podman/podman.sock"
		println("docker socket is: $dockerSocket")
		environment("DOCKER_HOST", dockerSocket)
		environment("TESTCONTAINERS_RYUK_DISABLED", "true")
	}
}

ebean {
	debugLevel = 1
}

springBoot {
	mainClass.set("eu.vnagy.openbudget.OpenbudgetApplicationKt")
}
